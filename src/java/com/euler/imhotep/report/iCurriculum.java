/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.report;

import com.euler.imhotep.persistence.*;
import com.euler.info.Person;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class iCurriculum {

    private Document document;
    private PdfWriter writer;
    private static final String BASEPATH="/usr/share/tomcat6/webapps_files/";
    public iCurriculum() {
        document = new Document(PageSize.A4);
    }

    public File createResume(UserAccount user) {
        try {
            StringBuffer path = new StringBuffer(BASEPATH);
            path.append("CV_");
            path.append(constructName(user.getPersonalInfo()).replace(" ", "."));
            path.append(".pdf");
            File file = new File(path.toString());
            FileOutputStream fileout =  new FileOutputStream(file);
            writer = PdfWriter.getInstance(document,fileout);
            document.addTitle("Curriculum Vitae de " + constructName(user.getPersonalInfo()));
            document.addAuthor("Imhotep Servicios S.L.");
            document.open();

            if (user.getPersonalInfo() != null) {
                Phrase title = new Phrase(new Chunk("Datos Personales: \n", new Font(Font.HELVETICA, 22, Font.BOLD)));
                document.add(title);
                document.add(getPersonalInfo(user));
            }
            if (user.getStudies() != null) {
                Paragraph p = new Paragraph(new Phrase(new Chunk("Estudios: \n", new Font(Font.HELVETICA, 22, Font.BOLD))));
                p.setSpacingAfter(10);
                p.setSpacingBefore(10);
                document.add(p);
                document.add(getStudy(user.getStudies()));
            }
            if (user.getExperiences() != null) {
                Paragraph p = new Paragraph(new Phrase(new Chunk("Experiencia: \n", new Font(Font.HELVETICA, 22, Font.BOLD))));
                p.setSpacingAfter(10);
                p.setSpacingBefore(10);
                document.add(p);
                document.add(getExperience(user.getExperiences()));
            }
            if (user.getKnowledges() != null) {
                Paragraph p = new Paragraph(new Phrase(new Chunk("Conocimientos: \n", new Font(Font.HELVETICA, 22, Font.BOLD))));
                p.setSpacingAfter(10);
                p.setSpacingBefore(10);
                document.add(p);
                document.add(getKnowledge(user.getKnowledges()));
            }
            document.close();
            return file;
        } catch (IOException ex) {
            Logger.getLogger(iCurriculum.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }  catch (DocumentException ex) {
            Logger.getLogger(iCurriculum.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Paragraph getPersonalInfo(UserAccount user) {
        Person personalInfo = user.getPersonalInfo();
        Paragraph p = new Paragraph();
        try {
            p.setSpacingBefore(10);
            p.setSpacingAfter(10);
            Font names = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
            Font values = new Font(Font.HELVETICA, 12, Font.NORMAL);

            Chunk nameValue = new Chunk(personalInfo.getName() + "  ", values);

            Phrase name = new Phrase(255);
            name.add(nameValue);

            p.add(name);
            StringBuffer buffer = new StringBuffer(personalInfo.getSurname1());
            buffer.append(" ");
            if (personalInfo.getSurname2() != null) {
                buffer.append(personalInfo.getSurname2());
            }
            buffer.append(" ");
            Phrase surname = new Phrase(255);
            Chunk surnameValue = new Chunk(buffer.toString(), values);
            surname.add(surnameValue);
            p.add(surname);                       

            buffer = new StringBuffer();
            Phrase birthdate = new Phrase(255);
            SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
            buffer.append("("+df.format(user.getBirthdate())+")");
            buffer.append("\n");
            Chunk birthdateValue =  new Chunk(buffer.toString(), values);
            birthdate.add(birthdateValue);
            p.add(birthdate);
            
            buffer = new StringBuffer();
            Phrase email = new Phrase(255);
            buffer.append(user.getEmailAccount());
            buffer.append("\n");
            Chunk emailValue =  new Chunk(buffer.toString(), values);
            email.add(emailValue);
            p.add(email);

            buffer = new StringBuffer();
            Phrase telf = new Phrase(255);
            buffer.append(personalInfo.getMobilePhone());
            buffer.append("\n");
            Chunk telfValue =  new Chunk(buffer.toString(), values);
            telf.add(telfValue);
            p.add(telf);


            buffer = new StringBuffer();
            Phrase address = new Phrase(255);
            if (personalInfo.getAddress() != null) {
                if (personalInfo.getAddress().getAddress() != null) {
                    buffer.append(personalInfo.getAddress().getAddress());
                }
            }
            buffer.append("\n");
            Chunk addressValue = new Chunk(buffer.toString(), values);
            address.add(addressValue);
            p.add(address);


            Phrase town = new Phrase(255);
            buffer = new StringBuffer();
            if (personalInfo.getAddress() != null) {
                if (personalInfo.getAddress().getTown() != null) {
                    buffer.append(personalInfo.getAddress().getTown());
                    buffer.append(" (");
                }
                if (personalInfo.getAddress().getProvince() != null) {
                    buffer.append(personalInfo.getAddress().getProvince());
                    buffer.append(" )");
                }
                buffer.append("\n");
                Chunk townValue = new Chunk(buffer.toString(), values);
                town.add(townValue);
                p.add(town);

                Phrase zip = new Phrase(255);
                buffer = new StringBuffer();
                buffer.append("C.P. ");
                if (personalInfo.getAddress().getZip() != null) {
                    buffer.append(personalInfo.getAddress().getZip());
                }
                buffer.append("\n");
                Chunk zipValue = new Chunk(buffer.toString(), values);
                zip.add(zipValue);
                p.add(zip);
            }
            return p;
        } catch (Exception ex) {
            Logger.getLogger(iCurriculum.class.getName()).log(Level.SEVERE, null, ex);
            return p;
        }
    }

    private Element getStudy(java.util.List<Study> studies) {
        try {
            float width = document.getPageSize().width();
            float[] columnDefinitionSize = {13.33F, 23.33F};
            PdfPTable table = new PdfPTable(columnDefinitionSize);
            Font font = new Font(Font.TIMES_ROMAN, 12, Font.ITALIC);
            Font uni = new Font(Font.TIMES_ROMAN, 12, Font.BOLDITALIC);

            table.setTotalWidth(400);
            table.setHorizontalAlignment(0);
            table.setLockedWidth(true);

            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            for (Study s : studies) {
                StringBuffer study = new StringBuffer();
                study.append(s.getCustomStartDate());
                study.append(" - ");
                study.append(s.getCustomFinishDate());
                PdfPCell c = new PdfPCell(new Phrase(new Chunk(study.toString(), font)));
                c.rectangle(12, 40);
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);

                c = new PdfPCell(new Phrase(new Chunk(s.getUniversityName(), uni)));
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);

                table.addCell("");

                c = new PdfPCell(new Phrase(new Chunk(s.getDegreeName(), font)));
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);
            }

            return table;
        } catch (Exception ex) {
            Logger.getLogger(iCurriculum.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private Element getExperience(java.util.List<Experience> experiences) {
        try {
            float width = document.getPageSize().width();
            float[] columnDefinitionSize = {13.33F, 23.33F};
            PdfPTable table = new PdfPTable(columnDefinitionSize);
            Font font = new Font(Font.TIMES_ROMAN, 12, Font.ITALIC);
            Font uni = new Font(Font.TIMES_ROMAN, 12, Font.BOLDITALIC);

            table.setTotalWidth(400);
            table.setHorizontalAlignment(0);
            table.setLockedWidth(true);

            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            for (Experience e : experiences) {

                StringBuffer exp = new StringBuffer();
                exp.append(e.getCustomStartDate());
                exp.append(" - ");
                exp.append(e.getCustomFinishDate());
                PdfPCell c = new PdfPCell(new Phrase(new Chunk(exp.toString(), font)));
                c.rectangle(12, 40);
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);


                exp = new StringBuffer();
                if(e.getCompany() != null){
                    exp.append(e.getCompany());
                }
                exp.append("\n");
                if(e.getPosition() != null){
                    exp.append(e.getPosition());
                }
                c = new PdfPCell(new Phrase(new Chunk(exp.toString(), uni)));
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);
                table.addCell("");

                exp = new StringBuffer();
                if(e.getDescription() != null){
                    exp.append(e.getDescription());
                }
                c = new PdfPCell(new Phrase(new Chunk(exp.toString(), font)));
                c.setBorder(PdfPCell.NO_BORDER);
                table.addCell(c);
            }

            return table;
        } catch (Exception ex) {
            Logger.getLogger(iCurriculum.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private Element getKnowledge(java.util.List<Knowledge> knowledges) {
        float width = document.getPageSize().width();
        float[] columnDefinitionSize = {13.33F, 23.33F};
        PdfPTable table = new PdfPTable(columnDefinitionSize);
        Font font = new Font(Font.TIMES_ROMAN, 12, Font.ITALIC);
        Font uni = new Font(Font.TIMES_ROMAN, 12, Font.BOLDITALIC);

        table.setTotalWidth(400);
        table.setHorizontalAlignment(0);
        table.setLockedWidth(true);

        table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        for (Knowledge k : knowledges) {

            StringBuffer know = new StringBuffer(k.getKnowledge());
            PdfPCell c = new PdfPCell(new Phrase(new Chunk(know.toString(), font)));
            c.rectangle(12, 40);
            c.setBorder(PdfPCell.NO_BORDER);
            table.addCell(c);
            know = new StringBuffer(k.getLevel().getKnowLevel());
            know.append("\n");
            know.append(k.getExperience().getName());
            c = new PdfPCell(new Phrase(new Chunk(know.toString(), uni)));
            c.setBorder(PdfPCell.NO_BORDER);
            table.addCell(c);
            table.addCell("");

            know = new StringBuffer();
            if(k.getDescription() != null){
                know.append(k.getDescription());
            }
            know.append("\n");
            c = new PdfPCell(new Phrase(new Chunk(know.toString(), font)));
            c.setBorder(PdfPCell.NO_BORDER);
            table.addCell(c);
        }
        return table;
    }

    private String constructName(Person person) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(person.getName());
        buffer.append(" ");
        buffer.append(person.getSurname1());
        return buffer.toString();
    }
}
