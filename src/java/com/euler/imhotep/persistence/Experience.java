/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="EXPERIENCE")
public class Experience implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableGenerator(name = "experienceGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_EXPERIENCE_ID", allocationSize = 1, initialValue = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "experienceGen")
    private Long id;
    
    @Column(name="STARTDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    @Column(name="FINISHDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    @Column(name="POSITION")
    private String position;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="COMPANY")
    private String company;

    @Column(name="CURRENTLY")
    private boolean currently;
    
    @ManyToOne
    @JoinColumn(name="FK_AREAID")
    private Area area;

    @ManyToOne
    @JoinColumn(name="FK_USER_ID")
    private UserAccount user;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public boolean isCurrently() {
        return currently;
    }

    public void setCurrently(boolean currently) {
        this.currently = currently;
    }

    public String getCustomExperiencePeriod() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(startDate)+" - "+df.format(finishDate);
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getStartDate() {
        return startDate;
    }
    public String getCustomStartDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(startDate);
    }
    public String getCustomFinishDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(finishDate);
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Experience)) {
            return false;
        }
        Experience other = (Experience) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Experience[id=" + id + "]";
    }

}
