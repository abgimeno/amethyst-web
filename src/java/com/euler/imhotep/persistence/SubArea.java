/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="SUBAREA")
@NamedQueries({
    @NamedQuery(name="SubAreas.getSubAreas", query="Select sb FROM SubArea sb ")
})
public class SubArea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="NAME")
    private String areaName;

    @ManyToOne
    @JoinColumn(name="FK_PARENT_AREAID")
    private Area parent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Area getParent() {
        return parent;
    }

    public void setParent(Area parent) {
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }



    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubArea)) {
            return false;
        }
        SubArea other = (SubArea) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.SubArea[id=" + id + "]";
    }

}
