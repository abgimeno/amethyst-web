/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.info;

import java.io.Serializable;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author abrahamgimeno
 */
@Entity
@Table(name="NOTICIA")
public class Noticia implements Serializable {

    public Noticia() {
        this.enabled = true;
        texto="";
        title="";
        date = new Date();
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="TITLE")
    private String title;

    @Column(name="NOTICIA", length=20000)
    private String texto;

    @Column(name="PUBLISHDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    @Column(name = "ENABLED_ENTRY")
    private boolean enabled;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrimedTitle() {
        if (this.title.length() > 60) {
            return this.title.substring(0, 60)+"...";
        } else {
            return this.title;
        }
    }


    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTrimedText() {
        try {
            StringReader sr = new StringReader(texto);
            final StringBuffer buf = new StringBuffer(1000);
            HTMLDocument doc = new HTMLDocument() {
                @Override
                public HTMLEditorKit.ParserCallback getReader(int pos) {
                    return new HTMLEditorKit.ParserCallback() {                        
                        @Override
                        public void handleText(char[] data, int pos) {
                            buf.append(data);
                            buf.append('\n');
                        }
                    };
                }
            };

            EditorKit kit = new HTMLEditorKit();
            kit.read(sr, doc, 0);
            if(buf.toString().length() > 200){
                return buf.toString().substring(0,200)+" ...";
                        }
            else{
                return buf.toString()+" ...";
            }
        } catch (Exception ex) {
            Logger.getLogger(Noticia.class.getName()).log(Level.SEVERE, null, ex);
            return texto;
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public String getCustomDate() {
        Locale l = Locale.getDefault();
        SimpleDateFormat df =  new SimpleDateFormat("dd-MM-yy",Locale.getDefault());
        return  df.format(date);
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noticia)) {
            return false;
        }
        Noticia other = (Noticia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.info.Noticia[id=" + id + "]";
    }

}
