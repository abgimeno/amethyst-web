/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="USERHASHES")
@NamedQueries({
    @NamedQuery(name="UserHash.findHash", query="SELECT uh FROM UserHash uh WHERE uh.hashed LIKE :hash"),
    @NamedQuery(name="UserHash.findHashByUserId", query="SELECT uh FROM UserHash uh WHERE uh.user.id = :userId")
})
public class UserHash implements Serializable {
    
   public UserHash () {
       
   }
   @Id
   @TableGenerator(name = "userHashGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_HASH_ID", allocationSize = 9, initialValue = 3)
   @GeneratedValue(strategy = GenerationType.TABLE, generator = "userHashGen")
   @Column(name="HASH_ID")
   private Long hashId;

   @ManyToOne
   @JoinColumn(name="FK_USER_ID")
   private UserAccount user;
   
   @Column(name="HASHED")
   private String hashed;

   @Column(name="HASH_DATE")
   @Temporal(TemporalType.DATE)
   private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getHashId() {
        return hashId;
    }

    public void setHashId(Long hashId) {
        this.hashId = hashId;
    }

    public String getHashed() {
        return hashed;
    }

    public void setHashed(String hashed) {
        this.hashed = hashed;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }
   

    /** returns the hash, encoded to pass it in an URL */
    public String getHashUrlEncoded(){
        try {
            return URLEncoder.encode(hashed, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UserHash.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }


}
