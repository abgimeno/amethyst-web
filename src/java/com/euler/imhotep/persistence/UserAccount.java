/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.persistence;

import com.euler.imhotep.persistence.state.OfferStatus;
import com.euler.info.Address;
import com.euler.info.Person;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author abraham
 */

@Table(name = "USERACCOUNT")
@NamedQueries({
    @NamedQuery(name="UserAccount.findByEMail", query="SELECT u FROM UserAccount u WHERE u.emailAccount LIKE :email"),
    @NamedQuery(name="UserAccount.getByTag", query="SELECT u FROM UserAccount u LEFT JOIN u.tags ut WHERE ut.id = :tagId"),
    @NamedQuery(name="UserAccount.getByTagsAnd", query="SELECT DISTINCT Object(u) FROM UserAccount u WHERE :tag1 MEMBER OF  u.tags AND :tag2 MEMBER OF  u.tags"),
    @NamedQuery(name="UserAccount.getByTagsOr", query="SELECT DISTINCT Object(u) FROM UserAccount u WHERE :tag1 MEMBER OF  u.tags OR :tag2 MEMBER OF  u.tags")
})
@Entity
public class UserAccount implements Serializable {

    public static final String DEF_PWD = "JACK99";
    public static final String DEF_ROOT = "admin@euler.com";
    
    private static final long serialVersionUID = 1L;
    
    @TableGenerator(name = "userGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_USER_ID", allocationSize = 1, initialValue = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "userGen")
    private Long id;
    
    @Column(name = "ACCOUNT_EMAIL")
    private String emailAccount;

    @Column(name = "DNI")
    private String dni;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLED_ACCOUNT")
    private boolean enabled;

    @Embedded
    private Person personalInfo;

    @ManyToMany(mappedBy = "users")
    private List<Offer> offers = new ArrayList<Offer>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Study> studies = new ArrayList<Study>();

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "USER_2_AREA", joinColumns = @JoinColumn(name = "FK_USER_ID"), inverseJoinColumns = @JoinColumn(name = "FK_AREA_ID"))
    private List<Area> areas = new ArrayList<Area>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Experience> experiences = new ArrayList<Experience>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Knowledge> knowledges = new ArrayList<Knowledge>();

    @OneToMany(mappedBy="user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name="OFFERSTATUS")
    private List<OfferStatus> status = new ArrayList<OfferStatus>();

    @ManyToMany
    @JoinTable(name="USER2TAG",joinColumns = @JoinColumn(name = "FK_USER_ID"), inverseJoinColumns = @JoinColumn(name = "FK_TAG_ID"))
    private List<Tag> tags = new ArrayList<Tag>();

    @Column(name="CURRICULUM_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(Person personalInfo) {
        this.personalInfo = personalInfo;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public List<Study> getStudies() {
        return studies;
    }

    public void setStudies(List<Study> studies) {
        this.studies = studies;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public List<Knowledge> getKnowledges() {
        return knowledges;
    }

    public void setKnowledges(List<Knowledge> knowledges) {
        this.knowledges = knowledges;
    }
    public List<OfferStatus> getStatus() {
        return status;
    }

    public void setStatus(List<OfferStatus> status) {
        this.status = status;
    }
    public String getName() {
        return personalInfo.getName();
    }

    public void setName(String name) {
        this.personalInfo.setName(name);
    }

    public String getSurname1() {
        return personalInfo.getSurname1();
    }

    public void setSurname1(String surname1) {
        this.personalInfo.setSurname1(surname1);
    }

    public String getSurname2() {
        return personalInfo.getSurname2();
    }

    public void setSurname2(String surname2) {
        this.personalInfo.setSurname2(surname2);
    }

    public Date getBirthdate() {
        return personalInfo.getBirthdate();
    }

    public void setBirthdate(Date birthdate) {
        this.personalInfo.setBirthdate(birthdate);
    }

    public Address getAddress() {
        return personalInfo.getAddress();
    }

    public void setAddress(Address address) {
        this.personalInfo.setAddress(address);
    }

    public String getEmail() {
        return personalInfo.getEmail();
    }

    public void setEmail(String email) {
         personalInfo.setEmail(email);
    }

    public String getPublicPhone() {
        return personalInfo.getPublicPhone();
    }

    public void setPublicPhone(String publicPhone) {
        personalInfo.setPublicPhone(publicPhone);
    }

    public String getMobilePhone() {
        return personalInfo.getMobilePhone();
    }

    public void setMobilePhone(String mobilePhone) {
        personalInfo.setMobilePhone(mobilePhone);
    }
    public Date getCurriculumDate() {
        return date;
    }

    public void setCurriculumDate(Date date) {
        this.date = date;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getCustomDate() {
        Locale l = Locale.getDefault();
        SimpleDateFormat df =  new SimpleDateFormat("dd-MM-yy",Locale.getDefault());
        return  df.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccount)) {
            return false;
        }
        UserAccount other = (UserAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.UserAccount[id=" + id + "]";
    }
}
