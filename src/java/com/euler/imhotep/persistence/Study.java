/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="STUDY")
public class Study implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableGenerator(name = "studyGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_STUDY_ID", allocationSize = 1, initialValue = 2)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "studyGen")
    private Long id;

    @Column(name="STARTDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    @Column(name="FINISHDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    @Column(name="DEGREE_NAME")
    private String degreeName;

    @Column(name="UNIVERSTITY_NAME")
    private String universityName;

    @Column(name="CURSANDO")
    private boolean cursando;

    @ManyToOne
    @JoinColumn(name="FK_DEGREE_ID")
    private DegreeLevel degreeLevel;

    @ManyToOne
    @JoinColumn(name="FK_USERACCOUNT_ID")
    private UserAccount user;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DegreeLevel getDegreeLevel() {
        return degreeLevel;
    }

    public void setDegreeLevel(DegreeLevel degreeLevel) {
        this.degreeLevel = degreeLevel;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String univesityName) {
        this.universityName = univesityName;
    }

    public boolean isCursando() {
        return cursando;
    }

    public void setCursando(boolean cursando) {
        this.cursando = cursando;
    }
    public String getCustomStartDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(startDate);
    }
    public String getCustomFinishDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(finishDate);
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Study)) {
            return false;
        }
        Study other = (Study) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Study[id=" + id + "]";
    }

}
