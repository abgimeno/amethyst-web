/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.DegreeLevel;
import com.euler.imhotep.persistence.SubArea;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.state.OfferStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author abrahamgimeno
 */
public class OfferJpaController {

    public OfferJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Offer offer) {
        if (offer.getUsers() == null) {
            offer.setUsers(new ArrayList<UserAccount>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Concern company = offer.getCompany();
            if (company != null) {
                company = em.getReference(company.getClass(), company.getId());
                offer.setCompany(company);
            }
            List<UserAccount> attachedUsers = new ArrayList<UserAccount>();
            for (UserAccount usersUserAccountToAttach : offer.getUsers()) {
                usersUserAccountToAttach = em.getReference(usersUserAccountToAttach.getClass(), usersUserAccountToAttach.getId());
                attachedUsers.add(usersUserAccountToAttach);
            }
            offer.setUsers(attachedUsers);
            em.persist(offer);
            if (company != null) {
                company.getOffers().add(offer);
                company = em.merge(company);
            }
            for (UserAccount usersUserAccount : offer.getUsers()) {
                usersUserAccount.getOffers().add(offer);
                usersUserAccount = em.merge(usersUserAccount);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Offer offer) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(offer);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = offer.getId();
                if (findOffer(id) == null) {
                    throw new NonexistentEntityException("The offer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Offer offer;
            try {
                offer = em.getReference(Offer.class, id);
                offer.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The offer with id " + id + " no longer exists.", enfe);
            }
            Concern company = offer.getCompany();
            if (company != null) {
                company.getOffers().remove(offer);
                company = em.merge(company);
            }
            List<UserAccount> users = offer.getUsers();
            for (UserAccount usersUserAccount : users) {
                usersUserAccount.getOffers().remove(offer);
                usersUserAccount = em.merge(usersUserAccount);
            }

            List<OfferStatus> results = em.createNamedQuery("OfferStatus.getOfferStatusByCompany")
                                          .setParameter("companyid", offer.getId())
                                          .getResultList();
            for(OfferStatus os : results){
                em.remove(os);
            }
            em.remove(offer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> findOfferEntities() {
        return findOfferEntities(true, -1, -1);
    }

    public List<Offer> findOfferEntities(int maxResults, int firstResult) {
        return findOfferEntities(false, maxResults, firstResult);
    }

    private List<Offer> findOfferEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Offer as o WHERE o.enabled = TRUE ORDER BY o.offerDate DESC");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
        return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Offer findOffer(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Offer.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfferCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Offer as o WHERE o.enabled = TRUE").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
/********************* MINE FUNCTIONS **************************/
    public void setSubAreas( Vector<Integer> areasId,Offer offer){
        EntityManager em = null;
        List<SubArea> areas = new ArrayList<SubArea>();
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            for (Integer i : areasId) {
                SubArea a = em.getReference(SubArea.class, i);
                areas.add(a);
            }
            offer.setAreas(areas);
            em.merge(offer);
        } finally {
            if (em != null) {
                em.close();
                
            }
        }
    }

    public void setDegrees (int desiredDegreeId, int requiredDegreeId, Offer offer){
        EntityManager em = null;
        DegreeLevel desired =null;
        DegreeLevel required = null;
        try{
            em = getEntityManager();
            em.getTransaction().begin();
            desired = em.getReference(DegreeLevel.class, desiredDegreeId);
            required =em.getReference(DegreeLevel.class, requiredDegreeId);
            offer.setRequiredDegree(required);
            offer.setDesiredDegree(desired);
            em.merge(offer);
        }finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
