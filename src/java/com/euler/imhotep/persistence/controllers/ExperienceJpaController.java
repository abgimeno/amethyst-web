/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Experience;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abraham
 */
public class ExperienceJpaController {

    public ExperienceJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Experience experience) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(experience);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Experience experience) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            experience = em.merge(experience);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = experience.getId();
                if (findExperience(id) == null) {
                    throw new NonexistentEntityException("The experience with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Experience experience;
            try {
                experience = em.getReference(Experience.class, id);
                experience.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The experience with id " + id + " no longer exists.", enfe);
            }
            em.remove(experience);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Experience> findExperienceEntities() {
        return findExperienceEntities(true, -1, -1);
    }

    public List<Experience> findExperienceEntities(int maxResults, int firstResult) {
        return findExperienceEntities(false, maxResults, firstResult);
    }

    private List<Experience> findExperienceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Experience as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Experience findExperience(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Experience.class, id);
        } finally {
            em.close();
        }
    }

    public int getExperienceCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Experience as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
