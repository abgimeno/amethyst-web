/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Area;
import com.euler.imhotep.persistence.SubArea;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abraham
 */
public class SubAreaJpaController {

    public SubAreaJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SubArea subArea) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(subArea);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SubArea subArea) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            subArea = em.merge(subArea);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = subArea.getId();
                if (findSubArea(id) == null) {
                    throw new NonexistentEntityException("The subArea with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SubArea subArea;
            try {
                subArea = em.getReference(SubArea.class, id);
                subArea.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The subArea with id " + id + " no longer exists.", enfe);
            }
            em.remove(subArea);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SubArea> findSubAreaEntities() {
        return findSubAreaEntities(true, -1, -1);
    }

    public List<SubArea> findSubAreaEntities(int maxResults, int firstResult) {
        return findSubAreaEntities(false, maxResults, firstResult);
    }

    private List<SubArea> findSubAreaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SubArea as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SubArea findSubArea(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SubArea.class, id);
        } finally {
            em.close();
        }
    }

    public int getSubAreaCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from SubArea as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
