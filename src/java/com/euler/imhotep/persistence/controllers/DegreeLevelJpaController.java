/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.DegreeLevel;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abrahamgimeno
 */
public class DegreeLevelJpaController {

    public DegreeLevelJpaController() {

        emf = Persistence.createEntityManagerFactory("amethystPU");
        
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DegreeLevel degreeLevel) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(degreeLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DegreeLevel degreeLevel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            degreeLevel = em.merge(degreeLevel);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = degreeLevel.getId();
                if (findDegreeLevel(id) == null) {
                    throw new NonexistentEntityException("The degreeLevel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DegreeLevel degreeLevel;
            try {
                degreeLevel = em.getReference(DegreeLevel.class, id);
                degreeLevel.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The degreeLevel with id " + id + " no longer exists.", enfe);
            }
            em.remove(degreeLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DegreeLevel> findDegreeLevelEntities() {
        return findDegreeLevelEntities(true, -1, -1);
    }

    public List<DegreeLevel> findDegreeLevelEntities(int maxResults, int firstResult) {
        return findDegreeLevelEntities(false, maxResults, firstResult);
    }

    private List<DegreeLevel> findDegreeLevelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DegreeLevel as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DegreeLevel findDegreeLevel(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DegreeLevel.class, id);
        } finally {
            em.close();
        }
    }

    public int getDegreeLevelCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from DegreeLevel as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
