/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.persistence.state.OfferStatus;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.euler.imhotep.persistence.UserAccount;

/**
 *
 * @author abrahamgimeno
 */
public class OfferStatusJpaController {

    public OfferStatusJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OfferStatus offerStatus) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserAccount user = offerStatus.getUser();
            if (user != null) {
                user = em.getReference(user.getClass(), user.getId());
                offerStatus.setUser(user);
            }
            em.persist(offerStatus);
            if (user != null) {
                user.getStatus().add(offerStatus);
                user = em.merge(user);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OfferStatus offerStatus) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfferStatus persistentOfferStatus = em.find(OfferStatus.class, offerStatus.getId());
            UserAccount userOld = persistentOfferStatus.getUser();
            UserAccount userNew = offerStatus.getUser();
            if (userNew != null) {
                userNew = em.getReference(userNew.getClass(), userNew.getId());
                offerStatus.setUser(userNew);
            }
            offerStatus = em.merge(offerStatus);
            if (userOld != null && !userOld.equals(userNew)) {
                userOld.getStatus().remove(offerStatus);
                userOld = em.merge(userOld);
            }
            if (userNew != null && !userNew.equals(userOld)) {
                userNew.getStatus().add(offerStatus);
                userNew = em.merge(userNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = offerStatus.getId();
                if (findOfferStatus(id) == null) {
                    throw new NonexistentEntityException("The offerStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfferStatus offerStatus;
            try {
                offerStatus = em.getReference(OfferStatus.class, id);
                offerStatus.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The offerStatus with id " + id + " no longer exists.", enfe);
            }
            UserAccount user = offerStatus.getUser();
            if (user != null) {
                user.getStatus().remove(offerStatus);
                user = em.merge(user);
            }
            em.remove(offerStatus);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OfferStatus> findOfferStatusEntities() {
        return findOfferStatusEntities(true, -1, -1);
    }

    public List<OfferStatus> findOfferStatusEntities(int maxResults, int firstResult) {
        return findOfferStatusEntities(false, maxResults, firstResult);
    }

    private List<OfferStatus> findOfferStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from OfferStatus as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OfferStatus findOfferStatus(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OfferStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfferStatusCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from OfferStatus as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
