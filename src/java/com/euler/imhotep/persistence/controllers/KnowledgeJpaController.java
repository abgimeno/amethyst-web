/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Knowledge;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abrahamgimeno
 */
public class KnowledgeJpaController {

    public KnowledgeJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Knowledge knowledge) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(knowledge);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Knowledge knowledge) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            knowledge = em.merge(knowledge);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = knowledge.getId();
                if (findKnowledge(id) == null) {
                    throw new NonexistentEntityException("The knowledge with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Knowledge knowledge;
            try {
                knowledge = em.getReference(Knowledge.class, id);
                knowledge.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The knowledge with id " + id + " no longer exists.", enfe);
            }
            em.remove(knowledge);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Knowledge> findKnowledgeEntities() {
        return findKnowledgeEntities(true, -1, -1);
    }

    public List<Knowledge> findKnowledgeEntities(int maxResults, int firstResult) {
        return findKnowledgeEntities(false, maxResults, firstResult);
    }

    private List<Knowledge> findKnowledgeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Knowledge as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Knowledge findKnowledge(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Knowledge.class, id);
        } finally {
            em.close();
        }
    }

    public int getKnowledgeCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Knowledge as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
