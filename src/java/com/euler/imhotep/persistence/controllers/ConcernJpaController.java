/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.euler.imhotep.persistence.Offer;
import com.euler.security.Cryptography;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.NoResultException;

/**
 *
 * @author abrahamgimeno
 */
public class ConcernJpaController {


    public ConcernJpaController() {

        emf = Persistence.createEntityManagerFactory("amethystPU");
        
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Concern concern) {
        if (concern.getOffers() == null) {
            concern.setOffers(new ArrayList<Offer>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Offer> attachedOffers = new ArrayList<Offer>();
            for (Offer offersOfferToAttach : concern.getOffers()) {
                offersOfferToAttach = em.getReference(offersOfferToAttach.getClass(), offersOfferToAttach.getId());
                attachedOffers.add(offersOfferToAttach);
            }
            concern.setOffers(attachedOffers);
            em.persist(concern);
            for (Offer offersOffer : concern.getOffers()) {
                Concern oldCompanyOfOffersOffer = offersOffer.getCompany();
                offersOffer.setCompany(concern);
                offersOffer = em.merge(offersOffer);
                if (oldCompanyOfOffersOffer != null) {
                    oldCompanyOfOffersOffer.getOffers().remove(offersOffer);
                    oldCompanyOfOffersOffer = em.merge(oldCompanyOfOffersOffer);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Concern concern) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Concern persistentConcern = em.find(Concern.class, concern.getId());
            List<Offer> offersOld = persistentConcern.getOffers();
            List<Offer> offersNew = concern.getOffers();
            List<Offer> attachedOffersNew = new ArrayList<Offer>();
            for (Offer offersNewOfferToAttach : offersNew) {
                offersNewOfferToAttach = em.getReference(offersNewOfferToAttach.getClass(), offersNewOfferToAttach.getId());
                attachedOffersNew.add(offersNewOfferToAttach);
            }
            offersNew = attachedOffersNew;
            concern.setOffers(offersNew);
            concern = em.merge(concern);
            for (Offer offersOldOffer : offersOld) {
                if (!offersNew.contains(offersOldOffer)) {
                    offersOldOffer.setCompany(null);
                    offersOldOffer = em.merge(offersOldOffer);
                }
            }
            for (Offer offersNewOffer : offersNew) {
                if (!offersOld.contains(offersNewOffer)) {
                    Concern oldCompanyOfOffersNewOffer = offersNewOffer.getCompany();
                    offersNewOffer.setCompany(concern);
                    offersNewOffer = em.merge(offersNewOffer);
                    if (oldCompanyOfOffersNewOffer != null && !oldCompanyOfOffersNewOffer.equals(concern)) {
                        oldCompanyOfOffersNewOffer.getOffers().remove(offersNewOffer);
                        oldCompanyOfOffersNewOffer = em.merge(oldCompanyOfOffersNewOffer);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = concern.getId();
                if (findConcern(id) == null) {
                    throw new NonexistentEntityException("The concern with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Concern concern;
            try {
                concern = em.getReference(Concern.class, id);
                concern.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The concern with id " + id + " no longer exists.", enfe);
            }
            List<Offer> offers = concern.getOffers();
            for (Offer offersOffer : offers) {
                offersOffer.setCompany(null);
                offersOffer = em.merge(offersOffer);
            }
            em.remove(concern);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Concern> findConcernEntities() {
        return findConcernEntities(true, -1, -1);
    }

    public List<Concern> findConcernEntities(int maxResults, int firstResult) {
        return findConcernEntities(false, maxResults, firstResult);
    }

    private List<Concern> findConcernEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Concern as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Concern findConcern(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Concern.class, id);
        } finally {
            em.close();
        }
    }

    public int getConcernCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Concern as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
/************* MINE FUNCTIONS ******************************/
    
    public Concern login(String email, String password) {
        EntityManager em = getEntityManager();
        try {

            Concern emailAccount = (Concern) em.createNamedQuery("Concern.findByEMail").setParameter("email", email).getSingleResult();
            if (emailAccount == null) {
                throw new NoResultException();
            }

            if (Cryptography.cryptoPassword(email, password).equals(emailAccount.getPassword())) {
                return emailAccount;
            } else {
                throw new NoResultException();
            }
        } finally {
            em.close();
        }
    }

    public boolean existsCompany(String email) {
        EntityManager em = getEntityManager();
        try {
            Concern emailAccount = (Concern) em.createNamedQuery("Concern.findByEMail").setParameter("email", email).getSingleResult();
            if (emailAccount == null) {
                return false;
            }
            else
                return true;
        }catch(NoResultException noResExc){
            Logger.getLogger(ConcernJpaController.class.getName()).info("User " +email+" does not exist.");
            return false;
        }
        finally {
            em.close();
        }
    }
}
