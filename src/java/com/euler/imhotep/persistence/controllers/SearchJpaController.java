/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Offer;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author abraham
 */
public class SearchJpaController {

    public SearchJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;



    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public int countOffersByArea(Integer areaId) {
      EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Long> subareaIds = em.createNamedQuery("Area.getSubAreasId")
                                         .setParameter("areaId", areaId)
                                         .getResultList();
            String areas="";
            for(Long i: subareaIds){
               areas=i+","+areas;
            }
            areas=areas.substring(0, areas.length()-1);
            Logger.getLogger(SearchJpaController.class.getName()).info("AREAS : ("+areas+")");
            Query q = em.createQuery("SELECT COUNT(o) FROM Offer o JOIN o.areas oa WHERE o.enabled = TRUE oa.id IN ("+areas+")");
            Long l = (Long) q.getSingleResult();
            em.getTransaction().commit();
            return l.intValue();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    public int countOffersByArea(String keyword, Integer areaId) {
      EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Long> subareaIds = em.createNamedQuery("Area.getSubAreasId")
                                         .setParameter("areaId", areaId)
                                         .getResultList();
            String areas="";
            for(Long i: subareaIds){
               areas=i+","+areas;
            }
            areas=areas.substring(0, areas.length()-1);
            Logger.getLogger(SearchJpaController.class.getName()).fine("AREAS : ("+areas+")");
            Query q = em.createQuery("SELECT COUNT(o) FROM Offer o JOIN o.areas oa WHERE o.enabled = TRUE AND oa.id IN (" + areas + ") AND (o.description LIKE :keyword OR o.requirements LIKE :keyword OR o.desired LIKE :keyword " +
                    "OR o.rDegreeName LIKE :keyword OR o.dDegreeName LIKE :keyword  OR o.jobTitle LIKE :keyword)").setParameter("keyword", "%" + keyword + "%");

            Long count = (Long) q.getSingleResult();
            em.getTransaction().commit();
            return count.intValue();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> getOffersByArea(Integer areaId){
      return getOffersByArea(true,areaId,-1,-1);
    }

    public List<Offer> getOffersByArea(Integer areaId, Integer maxResults, Integer firstResult) {
        return getOffersByArea(false,areaId,maxResults,firstResult);
    }

    private List<Offer> getOffersByArea(boolean all,Integer areaId,Integer maxResults, Integer firstResult){
      EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Long> subareaIds = em.createNamedQuery("Area.getSubAreasId")
                                         .setParameter("areaId", areaId)
                                         .getResultList();
            String areas="";
            for(Long i: subareaIds){
               areas=i+","+areas;
            }
            areas=areas.substring(0, areas.length()-1);
            Logger.getLogger(SearchJpaController.class.getName()).fine("AREAS : ("+areas+")");
            Query q = em.createQuery("SELECT DISTINCT o FROM Offer o JOIN o.areas oa WHERE  o.enabled = TRUE AND oa.id IN ("+areas+")");
            if(!all){
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            List<Offer> offers = q.getResultList();
            em.getTransaction().commit();            
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    public List<Offer> getOffersByArea(String keyword,Integer areaId){
      return getOffersByArea(true,keyword,areaId,-1,-1);
    }

    public List<Offer> getOffersByArea(String keyword,Integer areaId, Integer nResults, Integer startResult) {
        return getOffersByArea(false,keyword,areaId,nResults,startResult);
    }
    private List<Offer> getOffersByArea(boolean all,String keyword,Integer areaId,Integer maxResults, Integer firstResult){
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Long> subareaIds = em.createNamedQuery("Area.getSubAreasId").setParameter("areaId", areaId).getResultList();
            String areas = "";
            for (Long i : subareaIds) {
                areas = i + "," + areas;
            }
            areas = areas.substring(0, areas.length() - 1);
            Logger.getLogger(SearchJpaController.class.getName()).fine("AREAS : (" + areas + ")");
            Query q = em.createQuery("SELECT DISTINCT o FROM Offer o JOIN o.areas oa WHERE  o.enabled = TRUE AND oa.id IN (" + areas + ") AND (o.description LIKE :keyword OR o.requirements LIKE :keyword OR o.desired LIKE :keyword " +
                    "OR o.rDegreeName LIKE :keyword OR o.dDegreeName LIKE :keyword  OR o.jobTitle LIKE :keyword)").setParameter("keyword", "%" + keyword + "%");
            if(!all){
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            List<Offer> offers = q.getResultList();
            em.getTransaction().commit();
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> getOffersByDegreeLevel(Integer degreeLevel){
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Offer> offers = em.createNamedQuery("Offer.getOffersByDegreeLevel")
                                    .setParameter("degreeId", (long) degreeLevel)
                                    .getResultList();
            em.getTransaction().commit();
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    public List<Offer> getOffersByKeyword(String keyword){
        return getOffersByKeyword(true,keyword, -1, -1);
    }
    public List<Offer> getOffersByKeyword(String keyword, Integer maxResults, Integer firstResult) {
        return getOffersByKeyword(false,keyword, maxResults, firstResult);
    }
    public Integer countOffersByKeword(String keyword) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            String k ="%"+keyword+"%";
            Query q = em.createNamedQuery("Offer.countOfferByKeyword")
                                    .setParameter("keyword", k);

            Long count = (Long) q.getSingleResult();
            em.getTransaction().commit();
            return count.intValue();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> getOffersByKeyword(boolean all,String keyword,Integer maxResults, Integer firstResult){
                      EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            String k ="%"+keyword+"%";
            Query q = em.createNamedQuery("Offer.getOfferByKeyword")
                                    .setParameter("keyword", k);
            if(!all){
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            List<Offer> offers = q.getResultList();
            em.getTransaction().commit();
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    
    }

    public List<Offer> getOffersBySubAreas(Integer subareaIds[]){
              EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            String areas="";
            for(Integer i: subareaIds){
               areas=i+","+areas;
            }
            areas=areas.substring(0, areas.length()-1);
            Logger.getLogger(SearchJpaController.class.getName()).fine("AREAS : ("+areas+")");
            Query q = em.createQuery("SELECT DISTINCT o FROM Offer o JOIN o.areas oa WHERE  o.enabled = TRUE AND oa.id IN ("+areas+")");
            List<Offer> offers = q.getResultList();
            em.getTransaction().commit();
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> getOffersByJobTitle(String keyword){
              EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
        List<Offer> offers = em.createNamedQuery("Offer.getOffersByTitle")
                                .setParameter("keyword", keyword)
                                .getResultList();
            return offers;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
