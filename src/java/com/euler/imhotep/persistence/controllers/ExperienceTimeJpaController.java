/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.*;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abrahamgimeno
 */
public class ExperienceTimeJpaController {

    public ExperienceTimeJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ExperienceTime experienceTime) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(experienceTime);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ExperienceTime experienceTime) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            experienceTime = em.merge(experienceTime);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = experienceTime.getId();
                if (findExperienceTime(id) == null) {
                    throw new NonexistentEntityException("The experienceTime with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ExperienceTime experienceTime;
            try {
                experienceTime = em.getReference(ExperienceTime.class, id);
                experienceTime.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The experienceTime with id " + id + " no longer exists.", enfe);
            }
            em.remove(experienceTime);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ExperienceTime> findExperienceTimeEntities() {
        return findExperienceTimeEntities(true, -1, -1);
    }

    public List<ExperienceTime> findExperienceTimeEntities(int maxResults, int firstResult) {
        return findExperienceTimeEntities(false, maxResults, firstResult);
    }

    private List<ExperienceTime> findExperienceTimeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ExperienceTime as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ExperienceTime findExperienceTime(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ExperienceTime.class, id);
        } finally {
            em.close();
        }
    }

    public int getExperienceTimeCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from ExperienceTime as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
