/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Experience;
import com.euler.imhotep.persistence.Knowledge;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.Study;
import com.euler.imhotep.persistence.Tag;
import com.euler.imhotep.persistence.UserHash;
import com.euler.imhotep.persistence.state.OfferStatus;
import com.euler.imhotep.persistence.state.Status;
import com.euler.security.Cryptography;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;

/**
 *
 * @author abraham
 */
public class UserAccountJpaController {

    private EntityManagerFactory emf = null;

    public UserAccountJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

  /*  public void create(UserAccount userAccount) {
        if (userAccount.getOffers() == null) {
            userAccount.setOffers(new ArrayList<Offer>());
        }
        if (userAccount.getStudies() == null) {
            userAccount.setStudies(new ArrayList<Study>());
        }
        if (userAccount.getExperiences() == null) {
            userAccount.setExperiences(new ArrayList<Experience>());
        }
        if (userAccount.getKnowledges() == null) {
            userAccount.setKnowledges(new ArrayList<Knowledge>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Offer> attachedOffers = new ArrayList<Offer>();
            for (Offer offersOfferToAttach : userAccount.getOffers()) {
                offersOfferToAttach = em.getReference(offersOfferToAttach.getClass(), offersOfferToAttach.getId());
                attachedOffers.add(offersOfferToAttach);
            }
            userAccount.setOffers(attachedOffers);
            List<Study> attachedStudies = new ArrayList<Study>();
            for (Study studiesStudyToAttach : userAccount.getStudies()) {
                studiesStudyToAttach = em.getReference(studiesStudyToAttach.getClass(), studiesStudyToAttach.getId());
                attachedStudies.add(studiesStudyToAttach);
            }
            userAccount.setStudies(attachedStudies);
            List<Experience> attachedExperiences = new ArrayList<Experience>();
            for (Experience experiencesExperienceToAttach : userAccount.getExperiences()) {
                experiencesExperienceToAttach = em.getReference(experiencesExperienceToAttach.getClass(), experiencesExperienceToAttach.getId());
                attachedExperiences.add(experiencesExperienceToAttach);
            }
            userAccount.setExperiences(attachedExperiences);
            List<Knowledge> attachedKnowledges = new ArrayList<Knowledge>();
            for (Knowledge knowledgesKnowledgeToAttach : userAccount.getKnowledges()) {
                knowledgesKnowledgeToAttach = em.getReference(knowledgesKnowledgeToAttach.getClass(), knowledgesKnowledgeToAttach.getId());
                attachedKnowledges.add(knowledgesKnowledgeToAttach);
            }
            userAccount.setKnowledges(attachedKnowledges);
            em.persist(userAccount);
            for (Offer offersOffer : userAccount.getOffers()) {
                offersOffer.getUsers().add(userAccount);
                offersOffer = em.merge(offersOffer);
            }
            for (Study studiesStudy : userAccount.getStudies()) {
                UserAccount oldUserOfStudiesStudy = studiesStudy.getUser();
                studiesStudy.setUser(userAccount);
                studiesStudy = em.merge(studiesStudy);
                if (oldUserOfStudiesStudy != null) {
                    oldUserOfStudiesStudy.getStudies().remove(studiesStudy);
                    oldUserOfStudiesStudy = em.merge(oldUserOfStudiesStudy);
                }
            }
            for (Experience experiencesExperience : userAccount.getExperiences()) {
                UserAccount oldUserOfExperiencesExperience = experiencesExperience.getUser();
                experiencesExperience.setUser(userAccount);
                experiencesExperience = em.merge(experiencesExperience);
                if (oldUserOfExperiencesExperience != null) {
                    oldUserOfExperiencesExperience.getExperiences().remove(experiencesExperience);
                    oldUserOfExperiencesExperience = em.merge(oldUserOfExperiencesExperience);
                }
            }
            for (Knowledge knowledgesKnowledge : userAccount.getKnowledges()) {
                UserAccount oldUserOfKnowledgesKnowledge = knowledgesKnowledge.getUser();
                knowledgesKnowledge.setUser(userAccount);
                knowledgesKnowledge = em.merge(knowledgesKnowledge);
                if (oldUserOfKnowledgesKnowledge != null) {
                    oldUserOfKnowledgesKnowledge.getKnowledges().remove(knowledgesKnowledge);
                    oldUserOfKnowledgesKnowledge = em.merge(oldUserOfKnowledgesKnowledge);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserAccount userAccount) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserAccount persistentUserAccount = em.find(UserAccount.class, userAccount.getId());
            List<Offer> offersOld = persistentUserAccount.getOffers();
            List<Offer> offersNew = userAccount.getOffers();
            List<Study> studiesOld = persistentUserAccount.getStudies();
            List<Study> studiesNew = userAccount.getStudies();
            List<Experience> experiencesOld = persistentUserAccount.getExperiences();
            List<Experience> experiencesNew = userAccount.getExperiences();
            List<Knowledge> knowledgesOld = persistentUserAccount.getKnowledges();
            List<Knowledge> knowledgesNew = userAccount.getKnowledges();
            List<Offer> attachedOffersNew = new ArrayList<Offer>();
            for (Offer offersNewOfferToAttach : offersNew) {
                offersNewOfferToAttach = em.getReference(offersNewOfferToAttach.getClass(), offersNewOfferToAttach.getId());
                attachedOffersNew.add(offersNewOfferToAttach);
            }
            offersNew = attachedOffersNew;
            userAccount.setOffers(offersNew);
            List<Study> attachedStudiesNew = new ArrayList<Study>();
            for (Study studiesNewStudyToAttach : studiesNew) {
                studiesNewStudyToAttach = em.getReference(studiesNewStudyToAttach.getClass(), studiesNewStudyToAttach.getId());
                attachedStudiesNew.add(studiesNewStudyToAttach);
            }
            studiesNew = attachedStudiesNew;
            userAccount.setStudies(studiesNew);
            List<Experience> attachedExperiencesNew = new ArrayList<Experience>();
            for (Experience experiencesNewExperienceToAttach : experiencesNew) {
                experiencesNewExperienceToAttach = em.getReference(experiencesNewExperienceToAttach.getClass(), experiencesNewExperienceToAttach.getId());
                attachedExperiencesNew.add(experiencesNewExperienceToAttach);
            }
            experiencesNew = attachedExperiencesNew;
            userAccount.setExperiences(experiencesNew);
            List<Knowledge> attachedKnowledgesNew = new ArrayList<Knowledge>();
            for (Knowledge knowledgesNewKnowledgeToAttach : knowledgesNew) {
                knowledgesNewKnowledgeToAttach = em.getReference(knowledgesNewKnowledgeToAttach.getClass(), knowledgesNewKnowledgeToAttach.getId());
                attachedKnowledgesNew.add(knowledgesNewKnowledgeToAttach);
            }
            knowledgesNew = attachedKnowledgesNew;
            userAccount.setKnowledges(knowledgesNew);
            userAccount = em.merge(userAccount);
            for (Offer offersOldOffer : offersOld) {
                if (!offersNew.contains(offersOldOffer)) {
                    offersOldOffer.getUsers().remove(userAccount);
                    offersOldOffer = em.merge(offersOldOffer);
                }
            }
            for (Offer offersNewOffer : offersNew) {
                if (!offersOld.contains(offersNewOffer)) {
                    offersNewOffer.getUsers().add(userAccount);
                    offersNewOffer = em.merge(offersNewOffer);
                }
            }
            for (Study studiesOldStudy : studiesOld) {
                if (!studiesNew.contains(studiesOldStudy)) {
                    studiesOldStudy.setUser(null);
                    studiesOldStudy = em.merge(studiesOldStudy);
                }
            }
            for (Study studiesNewStudy : studiesNew) {
                if (!studiesOld.contains(studiesNewStudy)) {
                    UserAccount oldUserOfStudiesNewStudy = studiesNewStudy.getUser();
                    studiesNewStudy.setUser(userAccount);
                    studiesNewStudy = em.merge(studiesNewStudy);
                    if (oldUserOfStudiesNewStudy != null && !oldUserOfStudiesNewStudy.equals(userAccount)) {
                        oldUserOfStudiesNewStudy.getStudies().remove(studiesNewStudy);
                        oldUserOfStudiesNewStudy = em.merge(oldUserOfStudiesNewStudy);
                    }
                }
            }
            for (Experience experiencesOldExperience : experiencesOld) {
                if (!experiencesNew.contains(experiencesOldExperience)) {
                    experiencesOldExperience.setUser(null);
                    experiencesOldExperience = em.merge(experiencesOldExperience);
                }
            }
            for (Experience experiencesNewExperience : experiencesNew) {
                if (!experiencesOld.contains(experiencesNewExperience)) {
                    UserAccount oldUserOfExperiencesNewExperience = experiencesNewExperience.getUser();
                    experiencesNewExperience.setUser(userAccount);
                    experiencesNewExperience = em.merge(experiencesNewExperience);
                    if (oldUserOfExperiencesNewExperience != null && !oldUserOfExperiencesNewExperience.equals(userAccount)) {
                        oldUserOfExperiencesNewExperience.getExperiences().remove(experiencesNewExperience);
                        oldUserOfExperiencesNewExperience = em.merge(oldUserOfExperiencesNewExperience);
                    }
                }
            }
            for (Knowledge knowledgesOldKnowledge : knowledgesOld) {
                if (!knowledgesNew.contains(knowledgesOldKnowledge)) {
                    knowledgesOldKnowledge.setUser(null);
                    knowledgesOldKnowledge = em.merge(knowledgesOldKnowledge);
                }
            }
            for (Knowledge knowledgesNewKnowledge : knowledgesNew) {
                if (!knowledgesOld.contains(knowledgesNewKnowledge)) {
                    UserAccount oldUserOfKnowledgesNewKnowledge = knowledgesNewKnowledge.getUser();
                    knowledgesNewKnowledge.setUser(userAccount);
                    knowledgesNewKnowledge = em.merge(knowledgesNewKnowledge);
                    if (oldUserOfKnowledgesNewKnowledge != null && !oldUserOfKnowledgesNewKnowledge.equals(userAccount)) {
                        oldUserOfKnowledgesNewKnowledge.getKnowledges().remove(knowledgesNewKnowledge);
                        oldUserOfKnowledgesNewKnowledge = em.merge(oldUserOfKnowledgesNewKnowledge);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = userAccount.getId();
                if (findUserAccount(id) == null) {
                    throw new NonexistentEntityException("The userAccount with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserAccount userAccount;
            try {
                userAccount = em.getReference(UserAccount.class, id);
                userAccount.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userAccount with id " + id + " no longer exists.", enfe);
            }
            List<Offer> offers = userAccount.getOffers();
            for (Offer offersOffer : offers) {
                offersOffer.getUsers().remove(userAccount);
                offersOffer = em.merge(offersOffer);
            }
            List<Study> studies = userAccount.getStudies();
            for (Study studiesStudy : studies) {
                studiesStudy.setUser(null);
                studiesStudy = em.merge(studiesStudy);
            }
            List<Experience> experiences = userAccount.getExperiences();
            for (Experience experiencesExperience : experiences) {
                experiencesExperience.setUser(null);
                experiencesExperience = em.merge(experiencesExperience);
            }
            List<Knowledge> knowledges = userAccount.getKnowledges();
            for (Knowledge knowledgesKnowledge : knowledges) {
                knowledgesKnowledge.setUser(null);
                knowledgesKnowledge = em.merge(knowledgesKnowledge);
            }
            em.remove(userAccount);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserAccount> findUserAccountEntities() {
        return findUserAccountEntities(true, -1, -1);
    }

    public List<UserAccount> findUserAccountEntities(int maxResults, int firstResult) {
        return findUserAccountEntities(false, maxResults, firstResult);
    }

    private List<UserAccount> findUserAccountEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserAccount as o WHERE o.enabled = true ORDER BY o.id DESC");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserAccount findUserAccount(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserAccount.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserAccountCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from UserAccount as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }*/

    public void create(UserAccount userAccount) {
        if (userAccount.getOffers() == null) {
            userAccount.setOffers(new ArrayList<Offer>());
        }
        if (userAccount.getStudies() == null) {
            userAccount.setStudies(new ArrayList<Study>());
        }
        if (userAccount.getExperiences() == null) {
            userAccount.setExperiences(new ArrayList<Experience>());
        }
        if (userAccount.getKnowledges() == null) {
            userAccount.setKnowledges(new ArrayList<Knowledge>());
        }
        if (userAccount.getStatus() == null) {
            userAccount.setStatus(new ArrayList<OfferStatus>());
        }
        if (userAccount.getTags() == null) {
            userAccount.setTags(new ArrayList<Tag>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Offer> attachedOffers = new ArrayList<Offer>();
            for (Offer offersOfferToAttach : userAccount.getOffers()) {
                offersOfferToAttach = em.getReference(offersOfferToAttach.getClass(), offersOfferToAttach.getId());
                attachedOffers.add(offersOfferToAttach);
            }
            userAccount.setOffers(attachedOffers);
            List<Study> attachedStudies = new ArrayList<Study>();
            for (Study studiesStudyToAttach : userAccount.getStudies()) {
                studiesStudyToAttach = em.getReference(studiesStudyToAttach.getClass(), studiesStudyToAttach.getId());
                attachedStudies.add(studiesStudyToAttach);
            }
            userAccount.setStudies(attachedStudies);
            List<Experience> attachedExperiences = new ArrayList<Experience>();
            for (Experience experiencesExperienceToAttach : userAccount.getExperiences()) {
                experiencesExperienceToAttach = em.getReference(experiencesExperienceToAttach.getClass(), experiencesExperienceToAttach.getId());
                attachedExperiences.add(experiencesExperienceToAttach);
            }
            userAccount.setExperiences(attachedExperiences);
            List<Knowledge> attachedKnowledges = new ArrayList<Knowledge>();
            for (Knowledge knowledgesKnowledgeToAttach : userAccount.getKnowledges()) {
                knowledgesKnowledgeToAttach = em.getReference(knowledgesKnowledgeToAttach.getClass(), knowledgesKnowledgeToAttach.getId());
                attachedKnowledges.add(knowledgesKnowledgeToAttach);
            }
            userAccount.setKnowledges(attachedKnowledges);
            List<OfferStatus> attachedStatus = new ArrayList<OfferStatus>();
            for (OfferStatus statusOfferStatusToAttach : userAccount.getStatus()) {
                statusOfferStatusToAttach = em.getReference(statusOfferStatusToAttach.getClass(), statusOfferStatusToAttach.getId());
                attachedStatus.add(statusOfferStatusToAttach);
            }
            userAccount.setStatus(attachedStatus);
            List<Tag> attachedTags = new ArrayList<Tag>();
            for (Tag tagsTagToAttach : userAccount.getTags()) {
                tagsTagToAttach = em.getReference(tagsTagToAttach.getClass(), tagsTagToAttach.getId());
                attachedTags.add(tagsTagToAttach);
            }
            userAccount.setTags(attachedTags);
            em.persist(userAccount);
            for (Offer offersOffer : userAccount.getOffers()) {
                offersOffer.getUsers().add(userAccount);
                offersOffer = em.merge(offersOffer);
            }
            for (Study studiesStudy : userAccount.getStudies()) {
                UserAccount oldUserOfStudiesStudy = studiesStudy.getUser();
                studiesStudy.setUser(userAccount);
                studiesStudy = em.merge(studiesStudy);
                if (oldUserOfStudiesStudy != null) {
                    oldUserOfStudiesStudy.getStudies().remove(studiesStudy);
                    oldUserOfStudiesStudy = em.merge(oldUserOfStudiesStudy);
                }
            }
            for (Experience experiencesExperience : userAccount.getExperiences()) {
                UserAccount oldUserOfExperiencesExperience = experiencesExperience.getUser();
                experiencesExperience.setUser(userAccount);
                experiencesExperience = em.merge(experiencesExperience);
                if (oldUserOfExperiencesExperience != null) {
                    oldUserOfExperiencesExperience.getExperiences().remove(experiencesExperience);
                    oldUserOfExperiencesExperience = em.merge(oldUserOfExperiencesExperience);
                }
            }
            for (Knowledge knowledgesKnowledge : userAccount.getKnowledges()) {
                UserAccount oldUserOfKnowledgesKnowledge = knowledgesKnowledge.getUser();
                knowledgesKnowledge.setUser(userAccount);
                knowledgesKnowledge = em.merge(knowledgesKnowledge);
                if (oldUserOfKnowledgesKnowledge != null) {
                    oldUserOfKnowledgesKnowledge.getKnowledges().remove(knowledgesKnowledge);
                    oldUserOfKnowledgesKnowledge = em.merge(oldUserOfKnowledgesKnowledge);
                }
            }
            for (OfferStatus statusOfferStatus : userAccount.getStatus()) {
                UserAccount oldUserOfStatusOfferStatus = statusOfferStatus.getUser();
                statusOfferStatus.setUser(userAccount);
                statusOfferStatus = em.merge(statusOfferStatus);
                if (oldUserOfStatusOfferStatus != null) {
                    oldUserOfStatusOfferStatus.getStatus().remove(statusOfferStatus);
                    oldUserOfStatusOfferStatus = em.merge(oldUserOfStatusOfferStatus);
                }
            }
            for (Tag tagsTag : userAccount.getTags()) {
                tagsTag.getUsers().add(userAccount);
                tagsTag = em.merge(tagsTag);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserAccount userAccount) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserAccount persistentUserAccount = em.find(UserAccount.class, userAccount.getId());
            List<Offer> offersOld = persistentUserAccount.getOffers();
            List<Offer> offersNew = userAccount.getOffers();
            List<Study> studiesOld = persistentUserAccount.getStudies();
            List<Study> studiesNew = userAccount.getStudies();
            List<Experience> experiencesOld = persistentUserAccount.getExperiences();
            List<Experience> experiencesNew = userAccount.getExperiences();
            List<Knowledge> knowledgesOld = persistentUserAccount.getKnowledges();
            List<Knowledge> knowledgesNew = userAccount.getKnowledges();
            List<OfferStatus> statusOld = persistentUserAccount.getStatus();
            List<OfferStatus> statusNew = userAccount.getStatus();
            List<Tag> tagsOld = persistentUserAccount.getTags();
            List<Tag> tagsNew = userAccount.getTags();
            List<Offer> attachedOffersNew = new ArrayList<Offer>();
            for (Offer offersNewOfferToAttach : offersNew) {
                offersNewOfferToAttach = em.getReference(offersNewOfferToAttach.getClass(), offersNewOfferToAttach.getId());
                attachedOffersNew.add(offersNewOfferToAttach);
            }
            offersNew = attachedOffersNew;
            userAccount.setOffers(offersNew);
            List<Study> attachedStudiesNew = new ArrayList<Study>();
            for (Study studiesNewStudyToAttach : studiesNew) {
                studiesNewStudyToAttach = em.getReference(studiesNewStudyToAttach.getClass(), studiesNewStudyToAttach.getId());
                attachedStudiesNew.add(studiesNewStudyToAttach);
            }
            studiesNew = attachedStudiesNew;
            userAccount.setStudies(studiesNew);
            List<Experience> attachedExperiencesNew = new ArrayList<Experience>();
            for (Experience experiencesNewExperienceToAttach : experiencesNew) {
                experiencesNewExperienceToAttach = em.getReference(experiencesNewExperienceToAttach.getClass(), experiencesNewExperienceToAttach.getId());
                attachedExperiencesNew.add(experiencesNewExperienceToAttach);
            }
            experiencesNew = attachedExperiencesNew;
            userAccount.setExperiences(experiencesNew);
            List<Knowledge> attachedKnowledgesNew = new ArrayList<Knowledge>();
            for (Knowledge knowledgesNewKnowledgeToAttach : knowledgesNew) {
                knowledgesNewKnowledgeToAttach = em.getReference(knowledgesNewKnowledgeToAttach.getClass(), knowledgesNewKnowledgeToAttach.getId());
                attachedKnowledgesNew.add(knowledgesNewKnowledgeToAttach);
            }
            knowledgesNew = attachedKnowledgesNew;
            userAccount.setKnowledges(knowledgesNew);
            List<OfferStatus> attachedStatusNew = new ArrayList<OfferStatus>();
            for (OfferStatus statusNewOfferStatusToAttach : statusNew) {
                statusNewOfferStatusToAttach = em.getReference(statusNewOfferStatusToAttach.getClass(), statusNewOfferStatusToAttach.getId());
                attachedStatusNew.add(statusNewOfferStatusToAttach);
            }
            statusNew = attachedStatusNew;
            userAccount.setStatus(statusNew);
            List<Tag> attachedTagsNew = new ArrayList<Tag>();
            for (Tag tagsNewTagToAttach : tagsNew) {
                tagsNewTagToAttach = em.getReference(tagsNewTagToAttach.getClass(), tagsNewTagToAttach.getId());
                attachedTagsNew.add(tagsNewTagToAttach);
            }
            tagsNew = attachedTagsNew;
            userAccount.setTags(tagsNew);
            userAccount = em.merge(userAccount);
            for (Offer offersOldOffer : offersOld) {
                if (!offersNew.contains(offersOldOffer)) {
                    offersOldOffer.getUsers().remove(userAccount);
                    offersOldOffer = em.merge(offersOldOffer);
                }
            }
            for (Offer offersNewOffer : offersNew) {
                if (!offersOld.contains(offersNewOffer)) {
                    offersNewOffer.getUsers().add(userAccount);
                    offersNewOffer = em.merge(offersNewOffer);
                }
            }
            for (Study studiesOldStudy : studiesOld) {
                if (!studiesNew.contains(studiesOldStudy)) {
                    studiesOldStudy.setUser(null);
                    studiesOldStudy = em.merge(studiesOldStudy);
                }
            }
            for (Study studiesNewStudy : studiesNew) {
                if (!studiesOld.contains(studiesNewStudy)) {
                    UserAccount oldUserOfStudiesNewStudy = studiesNewStudy.getUser();
                    studiesNewStudy.setUser(userAccount);
                    studiesNewStudy = em.merge(studiesNewStudy);
                    if (oldUserOfStudiesNewStudy != null && !oldUserOfStudiesNewStudy.equals(userAccount)) {
                        oldUserOfStudiesNewStudy.getStudies().remove(studiesNewStudy);
                        oldUserOfStudiesNewStudy = em.merge(oldUserOfStudiesNewStudy);
                    }
                }
            }
            for (Experience experiencesOldExperience : experiencesOld) {
                if (!experiencesNew.contains(experiencesOldExperience)) {
                    experiencesOldExperience.setUser(null);
                    experiencesOldExperience = em.merge(experiencesOldExperience);
                }
            }
            for (Experience experiencesNewExperience : experiencesNew) {
                if (!experiencesOld.contains(experiencesNewExperience)) {
                    UserAccount oldUserOfExperiencesNewExperience = experiencesNewExperience.getUser();
                    experiencesNewExperience.setUser(userAccount);
                    experiencesNewExperience = em.merge(experiencesNewExperience);
                    if (oldUserOfExperiencesNewExperience != null && !oldUserOfExperiencesNewExperience.equals(userAccount)) {
                        oldUserOfExperiencesNewExperience.getExperiences().remove(experiencesNewExperience);
                        oldUserOfExperiencesNewExperience = em.merge(oldUserOfExperiencesNewExperience);
                    }
                }
            }
            for (Knowledge knowledgesOldKnowledge : knowledgesOld) {
                if (!knowledgesNew.contains(knowledgesOldKnowledge)) {
                    knowledgesOldKnowledge.setUser(null);
                    knowledgesOldKnowledge = em.merge(knowledgesOldKnowledge);
                }
            }
            for (Knowledge knowledgesNewKnowledge : knowledgesNew) {
                if (!knowledgesOld.contains(knowledgesNewKnowledge)) {
                    UserAccount oldUserOfKnowledgesNewKnowledge = knowledgesNewKnowledge.getUser();
                    knowledgesNewKnowledge.setUser(userAccount);
                    knowledgesNewKnowledge = em.merge(knowledgesNewKnowledge);
                    if (oldUserOfKnowledgesNewKnowledge != null && !oldUserOfKnowledgesNewKnowledge.equals(userAccount)) {
                        oldUserOfKnowledgesNewKnowledge.getKnowledges().remove(knowledgesNewKnowledge);
                        oldUserOfKnowledgesNewKnowledge = em.merge(oldUserOfKnowledgesNewKnowledge);
                    }
                }
            }
            for (OfferStatus statusOldOfferStatus : statusOld) {
                if (!statusNew.contains(statusOldOfferStatus)) {
                    statusOldOfferStatus.setUser(null);
                    statusOldOfferStatus = em.merge(statusOldOfferStatus);
                }
            }
            for (OfferStatus statusNewOfferStatus : statusNew) {
                if (!statusOld.contains(statusNewOfferStatus)) {
                    UserAccount oldUserOfStatusNewOfferStatus = statusNewOfferStatus.getUser();
                    statusNewOfferStatus.setUser(userAccount);
                    statusNewOfferStatus = em.merge(statusNewOfferStatus);
                    if (oldUserOfStatusNewOfferStatus != null && !oldUserOfStatusNewOfferStatus.equals(userAccount)) {
                        oldUserOfStatusNewOfferStatus.getStatus().remove(statusNewOfferStatus);
                        oldUserOfStatusNewOfferStatus = em.merge(oldUserOfStatusNewOfferStatus);
                    }
                }
            }
            for (Tag tagsOldTag : tagsOld) {
                if (!tagsNew.contains(tagsOldTag)) {
                    tagsOldTag.getUsers().remove(userAccount);
                    tagsOldTag = em.merge(tagsOldTag);
                }
            }
            for (Tag tagsNewTag : tagsNew) {
                if (!tagsOld.contains(tagsNewTag)) {
                    tagsNewTag.getUsers().add(userAccount);
                    tagsNewTag = em.merge(tagsNewTag);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = userAccount.getId();
                if (findUserAccount(id) == null) {
                    throw new NonexistentEntityException("The userAccount with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserAccount userAccount;
            try {
                userAccount = em.getReference(UserAccount.class, id);
                userAccount.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userAccount with id " + id + " no longer exists.", enfe);
            }
            List<Offer> offers = userAccount.getOffers();
            for (Offer offersOffer : offers) {
                offersOffer.getUsers().remove(userAccount);
                offersOffer = em.merge(offersOffer);
            }
            List<Study> studies = userAccount.getStudies();
            for (Study studiesStudy : studies) {
                studiesStudy.setUser(null);
                studiesStudy = em.merge(studiesStudy);
            }
            List<Experience> experiences = userAccount.getExperiences();
            for (Experience experiencesExperience : experiences) {
                experiencesExperience.setUser(null);
                experiencesExperience = em.merge(experiencesExperience);
            }
            List<Knowledge> knowledges = userAccount.getKnowledges();
            for (Knowledge knowledgesKnowledge : knowledges) {
                knowledgesKnowledge.setUser(null);
                knowledgesKnowledge = em.merge(knowledgesKnowledge);
            }
            List<OfferStatus> status = userAccount.getStatus();
            for (OfferStatus statusOfferStatus : status) {
                statusOfferStatus.setUser(null);
                statusOfferStatus = em.merge(statusOfferStatus);
            }
            List<Tag> tags = userAccount.getTags();
            for (Tag tagsTag : tags) {
                tagsTag.getUsers().remove(userAccount);
                tagsTag = em.merge(tagsTag);
            }
            em.remove(userAccount);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserAccount> findUserAccountEntities() {
        return findUserAccountEntities(true, -1, -1);
    }

    public List<UserAccount> findUserAccountEntities(int maxResults, int firstResult) {
        return findUserAccountEntities(false, maxResults, firstResult);
    }

    private List<UserAccount> findUserAccountEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserAccount as o  WHERE o.enabled = true ORDER BY o.id DESC");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserAccount findUserAccount(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserAccount.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserAccountCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from UserAccount as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    /***********MINE ************/
    public List<UserAccount> getByTags(int conjunction, long tagId1, long tagId2) {
        EntityManager em = getEntityManager();
        try {
            Tag tag1 = null;
            Tag tag2 = null;
                switch(conjunction){
                    case 1:
                        tag1 = em.find(Tag.class, tagId1);
                        tag2 = em.find(Tag.class, tagId2);
                        return em.createNamedQuery("UserAccount.getByTagsAnd")
                            .setParameter("tag1", tag1)
                            .setParameter("tag2", tag2)
                            .getResultList();
                    case 2:
                        tag1 = em.find(Tag.class, tagId1);
                        tag2 = em.find(Tag.class, tagId2);
                        return em.createNamedQuery("UserAccount.getByTagsOr")
                            .setParameter("tag1", tag1)
                            .setParameter("tag2", tag2)
                            .getResultList();
                    default:
                         return em.createNamedQuery("UserAccount.getByTag")
                            .setParameter("tagId", tagId1)
                            .getResultList();             
            }
        } finally {
            em.close();
        }
    }

    
    public int getUserAccountCountEnabled() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from UserAccount as o WHERE o.enabled = true").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }


    public UserAccount login(String email, String password) {
        EntityManager em = getEntityManager();
        try {
            String cryptedPass = null;
            UserAccount emailAccount = (UserAccount) em.createNamedQuery("UserAccount.findByEMail").setParameter("email", email).getSingleResult();
            if (emailAccount == null) {
                throw new NoResultException();
            }

            //ROOT Login with encrypted PWD
            if (email.equals(UserAccount.DEF_ROOT) && emailAccount.getPassword().equals(UserAccount.DEF_PWD) && password.equals(UserAccount.DEF_PWD)) {
                return emailAccount;
            }

            if (Cryptography.cryptoPassword(email, password).equals(emailAccount.getPassword())) {
                return emailAccount;
            } else {
                //logger.info("Password not valid!");
                throw new NoResultException();
            }
        } finally {
            em.close();
        }
    }
    public boolean existsUser(String email) {
        EntityManager em = getEntityManager();
        try {
            UserAccount emailAccount = (UserAccount) em.createNamedQuery("UserAccount.findByEMail").setParameter("email", email).getSingleResult();
            if (emailAccount == null) {
                return false;
            } else {
                return true;
            }
        } catch (NoResultException noResExc) {
            Logger.getLogger(UserAccountJpaController.class.getName()).info("User " + email + " does not exist.");
            return false;
        } finally {
            em.close();
        }
    }

    /** Find a user throught a hash we sent in an email.
     *
     * @param hashed The hash from the email.
     * @return
     */
    public UserAccount returnHashedUser(String hashed) {
        EntityManager em = getEntityManager();
        UserHash userHash = new UserHash();
        try {
            em.getTransaction().begin();
            userHash= (UserHash) em.createNamedQuery("UserHash.findHash")
                                    .setParameter("hash", hashed)
                                    .getSingleResult();
            if(userHash == null){
                return null;
            }
            UserAccount user = userHash.getUser();
            em.getTransaction().commit();
            return user;
        } catch (NoResultException excNores) {
            Logger.getLogger(UserAccountJpaController.class.getName()).log(Level.SEVERE,"UserAccountJpaController::returnHashedUser() User not found", excNores);
            return null;
        }
    }
    /** Creates a Hash (for activating account or reset password) for a user and stores it into db.
     *
     * @param user The Useraccount for which the hash should be created.
     * @return the created Hash.
     */
    public UserHash createHash(UserAccount user) {

        EntityManager em = getEntityManager();
        UserHash userHash = new UserHash();
        try {
            em.getTransaction().begin();
            userHash.setHashId(user.getId());
            userHash.setDate(new Date());
            userHash.setUser(user);

            Logger.getLogger(UserAccountJpaController.class.getName()).info("HASH: " + user.getEmail() + user.getId() + userHash.getHashId());
            String hashed = Cryptography.hashWithDate(user.getEmail() + user.getId() + userHash.getHashId());
            userHash.setHashed(hashed);
            em.persist(userHash);
            //em.merge(userHash);
            em.getTransaction().commit();
            return userHash;
        } finally {
            em.close();
        }
    }

    /** Removes a hash out of the hash-tables in the db.
     *
     * @param user The UserAccount which hash should be deleted.
     *
     */
    public void removeOlderHash(UserAccount user) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            UserHash userReset =
                    (UserHash) em.createNamedQuery("UserHash.findHashByUserId").setParameter("userId", user.getId()).getSingleResult();
            if (userReset != null) {
                em.remove(userReset);
                em.flush();
            }
            em.getTransaction().commit();
        } catch (NoResultException excNores) {
            Logger.getLogger(UserAccountJpaController.class.getName()).log(Level.SEVERE,"UserAccountJpaController::removeOlderHash() User not found", excNores);
        } finally {
            em.close();
        }
    }
    public boolean addUserToOffer(UserAccount user, Long offerId) {
        EntityManager em = getEntityManager();
        Offer offer = null;
        OfferStatus status=null;
        try {
            em.getTransaction().begin();
            offer = em.find(Offer.class, offerId);
            user = em.find(UserAccount.class,user.getId());
            //First I create the OfferStatus
            status = (OfferStatus) em.createNamedQuery("OfferStatus.isJoinedUser")
                                                .setParameter("userid", user.getId())
                                                .setParameter("offerid", offerId)
                                                .getSingleResult();

            em.getTransaction().commit();
            return false;
        }catch(NoResultException noRes){
                offer = em.find(Offer.class, offerId);
                status = new OfferStatus();
                Status state = em.find(Status.class, Status.RECEIVED);
                status.setStatus(state);
                status.setUser(user);
                status.setOffer(offer);
                em.persist(status);
                /*After I set the Offer and the Offer status in the user
                because the cascading, it will save all
                 * */

                user.getStatus().add(status);
                user.getOffers().add(offer);
                em.merge(user);
                em.getTransaction().commit();
                return true;
        }
        finally {
            em.close();
        }
    }

    public List<Offer> getOfferByUser(Long userId){
        EntityManager em = getEntityManager();
        List<Offer> offers = new ArrayList<Offer>();
        List<Integer> ids = new ArrayList<Integer>();
        try {
            em.getTransaction().begin();
            
            //First I create the OfferStatus
            ids =  em.createNamedQuery("OfferStatus.getOfferIdsByUser")
                                                .setParameter("userid", userId)
                                                .getResultList();
            String statusIds="";
            for(Integer i: ids){
               statusIds=i+","+statusIds;
            }
            statusIds=statusIds.substring(0, statusIds.length()-1);

            offers = em.createQuery("SELECT o FROM Offer WHERE o.id IN("+statusIds+")")
                        .getResultList();
            em.getTransaction().commit();
            return offers;
        }
        finally {
            em.close();
        }
    }
}
