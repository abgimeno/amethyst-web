/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.KnowledgeLevel;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abrahamgimeno
 */
public class KnowledgeLevelJpaController {

    public KnowledgeLevelJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(KnowledgeLevel knowledgeLevel) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(knowledgeLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(KnowledgeLevel knowledgeLevel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            knowledgeLevel = em.merge(knowledgeLevel);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = knowledgeLevel.getId();
                if (findKnowledgeLevel(id) == null) {
                    throw new NonexistentEntityException("The knowledgeLevel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            KnowledgeLevel knowledgeLevel;
            try {
                knowledgeLevel = em.getReference(KnowledgeLevel.class, id);
                knowledgeLevel.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The knowledgeLevel with id " + id + " no longer exists.", enfe);
            }
            em.remove(knowledgeLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<KnowledgeLevel> findKnowledgeLevelEntities() {
        return findKnowledgeLevelEntities(true, -1, -1);
    }

    public List<KnowledgeLevel> findKnowledgeLevelEntities(int maxResults, int firstResult) {
        return findKnowledgeLevelEntities(false, maxResults, firstResult);
    }

    private List<KnowledgeLevel> findKnowledgeLevelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from KnowledgeLevel as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public KnowledgeLevel findKnowledgeLevel(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(KnowledgeLevel.class, id);
        } finally {
            em.close();
        }
    }

    public int getKnowledgeLevelCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from KnowledgeLevel as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
