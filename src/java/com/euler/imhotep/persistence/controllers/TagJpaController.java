/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Tag;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.euler.imhotep.persistence.UserAccount;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abrahamgimeno
 */
public class TagJpaController {

    public TagJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tag tag) {
        if (tag.getUsers() == null) {
            tag.setUsers(new ArrayList<UserAccount>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<UserAccount> attachedUsers = new ArrayList<UserAccount>();
            for (UserAccount usersUserAccountToAttach : tag.getUsers()) {
                usersUserAccountToAttach = em.getReference(usersUserAccountToAttach.getClass(), usersUserAccountToAttach.getId());
                attachedUsers.add(usersUserAccountToAttach);
            }
            tag.setUsers(attachedUsers);
            em.persist(tag);
            for (UserAccount usersUserAccount : tag.getUsers()) {
                usersUserAccount.getTags().add(tag);
                usersUserAccount = em.merge(usersUserAccount);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tag tag) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tag persistentTag = em.find(Tag.class, tag.getId());
            List<UserAccount> usersOld = persistentTag.getUsers();
            List<UserAccount> usersNew = tag.getUsers();
            List<UserAccount> attachedUsersNew = new ArrayList<UserAccount>();
            for (UserAccount usersNewUserAccountToAttach : usersNew) {
                usersNewUserAccountToAttach = em.getReference(usersNewUserAccountToAttach.getClass(), usersNewUserAccountToAttach.getId());
                attachedUsersNew.add(usersNewUserAccountToAttach);
            }
            usersNew = attachedUsersNew;
            tag.setUsers(usersNew);
            tag = em.merge(tag);
            for (UserAccount usersOldUserAccount : usersOld) {
                if (!usersNew.contains(usersOldUserAccount)) {
                    usersOldUserAccount.getTags().remove(tag);
                    usersOldUserAccount = em.merge(usersOldUserAccount);
                }
            }
            for (UserAccount usersNewUserAccount : usersNew) {
                if (!usersOld.contains(usersNewUserAccount)) {
                    usersNewUserAccount.getTags().add(tag);
                    usersNewUserAccount = em.merge(usersNewUserAccount);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = tag.getId();
                if (findTag(id) == null) {
                    throw new NonexistentEntityException("The tag with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tag tag;
            try {
                tag = em.getReference(Tag.class, id);
                tag.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tag with id " + id + " no longer exists.", enfe);
            }
            List<UserAccount> users = tag.getUsers();
            for (UserAccount usersUserAccount : users) {
                usersUserAccount.getTags().remove(tag);
                usersUserAccount = em.merge(usersUserAccount);
            }
            em.remove(tag);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tag> findTagEntities() {
        return findTagEntities(true, -1, -1);
    }

    public List<Tag> findTagEntities(int maxResults, int firstResult) {
        return findTagEntities(false, maxResults, firstResult);
    }

    private List<Tag> findTagEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Tag as o ORDER BY o.name");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tag findTag(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tag.class, id);
        } finally {
            em.close();
        }
    }

    public int getTagCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Tag as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
