/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.Study;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abraham
 */
public class StudyJpaController {

    public StudyJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Study study) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Study study) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            study = em.merge(study);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = study.getId();
                if (findStudy(id) == null) {
                    throw new NonexistentEntityException("The study with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Study study;
            try {
                study = em.getReference(Study.class, id);
                study.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The study with id " + id + " no longer exists.", enfe);
            }
            em.remove(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Study> findStudyEntities() {
        return findStudyEntities(true, -1, -1);
    }

    public List<Study> findStudyEntities(int maxResults, int firstResult) {
        return findStudyEntities(false, maxResults, firstResult);
    }

    private List<Study> findStudyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Study findStudy(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Study.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudyCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Study as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
