/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.company.Dispatch;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abraham
 */
public class DispatchJpaController {

    public DispatchJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Dispatch dispatch) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dispatch);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dispatch dispatch) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            dispatch = em.merge(dispatch);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = dispatch.getId();
                if (findDispatch(id) == null) {
                    throw new NonexistentEntityException("The dispatch with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dispatch dispatch;
            try {
                dispatch = em.getReference(Dispatch.class, id);
                dispatch.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dispatch with id " + id + " no longer exists.", enfe);
            }
            em.remove(dispatch);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dispatch> findDispatchEntities() {
        return findDispatchEntities(true, -1, -1);
    }

    public List<Dispatch> findDispatchEntities(int maxResults, int firstResult) {
        return findDispatchEntities(false, maxResults, firstResult);
    }

    private List<Dispatch> findDispatchEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Dispatch as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dispatch findDispatch(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dispatch.class, id);
        } finally {
            em.close();
        }
    }

    public int getDispatchCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Dispatch as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
/**** MINE *****/
    private List<Dispatch> getPayed() {
        return getPayed(true, -1, -1);
    }

    private List<Dispatch> getPayed(int maxResults, int firstResult) {
        return getPayed(false, maxResults, firstResult);
    }

    private List<Dispatch> getPayed(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Dispatch.getPayed");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    private List<Dispatch> getNotPayed() {
        return getNotPayed(true, -1, -1);
    }

    private List<Dispatch> getNotPayed(int maxResults, int firstResult) {
        return getNotPayed(false, maxResults, firstResult);
    }

    private List<Dispatch> getNotPayed(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Dispatch.getNotPayed");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    private List<Dispatch> getByDate(Date startDate, Date finishDate) {
        return getByDate(true, -1, -1,startDate,finishDate);
    }

    private List<Dispatch> getByDate(int maxResults, int firstResult,Date startDate, Date finishDate) {
        return getByDate(false, maxResults, firstResult,startDate,finishDate);
    }

    private List<Dispatch> getByDate(boolean all, int maxResults, int firstResult,Date startDate, Date finishDate) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Dispatch.getPayed")
                        .setParameter("startDate", startDate)
                        .setParameter("finishDate", finishDate);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
}
