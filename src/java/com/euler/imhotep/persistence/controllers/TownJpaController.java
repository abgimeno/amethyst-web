/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.controllers;

import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.info.Town;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author abrahamgimeno
 */
public class TownJpaController {

    public TownJpaController() {
        emf = Persistence.createEntityManagerFactory("amethystPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Town town) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(town);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Town town) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            town = em.merge(town);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = town.getId();
                if (findTown(id) == null) {
                    throw new NonexistentEntityException("The town with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Town town;
            try {
                town = em.getReference(Town.class, id);
                town.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The town with id " + id + " no longer exists.", enfe);
            }
            em.remove(town);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Town> findTownEntities() {
        return findTownEntities(true, -1, -1);
    }

    public List<Town> findTownEntities(int maxResults, int firstResult) {
        return findTownEntities(false, maxResults, firstResult);
    }

    private List<Town> findTownEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Town as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Town findTown(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Town.class, id);
        } finally {
            em.close();
        }
    }

    public int getTownCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Town as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
/****************** mine functions *********************************/
    public List<Town> getTownsByProvince(Integer provinceId) {
        EntityManager em = getEntityManager();
        List<Town> towns = new ArrayList<Town>();
        try {
            em.getTransaction().begin();
            towns = em.createNamedQuery("Town.getTownByProvince")
                    .setParameter("provinceId", provinceId)
                    .getResultList();
            em.getTransaction().commit();
            return towns;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    public List<Town> getCastellonsTowns(){
        return getTownsByProvince(12);
    }
}
