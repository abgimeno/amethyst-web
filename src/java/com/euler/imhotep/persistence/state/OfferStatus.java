/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.state;

import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.UserAccount;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author abrahamgimeno
 */
@Entity
@Table(name="OFFERSTATUS")
@NamedQueries({
    @NamedQuery(name="OfferStatus.getOffers", query="SELECT os FROM OfferStatus os"),
    @NamedQuery(name="OfferStatus.isJoinedUser",query="SELECT os FROM OfferStatus os WHERE os.user.id =:userid AND os.offer.id =:offerid"),
    @NamedQuery(name="OfferStatus.getOfferIdsByUser",query="SELECT DISTINCT os.offer.id FROM OfferStatus os WHERE os.user.id =:userid "),
    @NamedQuery(name="OfferStatus.getOfferStatusByCompany",query="SELECT DISTINCT os FROM OfferStatus os WHERE os.offer.company.id =:companyid ")
})
public class OfferStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    
     @TableGenerator(name = "offer2UserGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_OFFER2USER_ID", allocationSize = 1, initialValue = 2)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "offer2UserGen")
    private Long id;

    @ManyToOne
    @JoinColumn(name="FK_USERACCOUNT_ID")
    private UserAccount user;

   
    @ManyToOne
    @JoinColumn(name="FK_OFFER_ID")
    private Offer offer;


    @ManyToOne
    @JoinColumn(name="FK_STATUS_ID")
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

}
