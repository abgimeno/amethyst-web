/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import com.euler.info.Town;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.*;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="OFFER")
@NamedQueries({
    @NamedQuery(name="Offer.getOffersByDegreeLevel", query="SELECT o FROM Offer o WHERE o.desiredDegree.id =:degreeId OR o.requiredDegree.id =:degreeId"),
    @NamedQuery(name="Offer.getOfferByKeyword",
                query="SELECT o FROM Offer o WHERE o.description LIKE :keyword OR o.requirements LIKE :keyword OR o.desired LIKE :keyword " +
                            "OR o.rDegreeName LIKE :keyword OR o.dDegreeName LIKE :keyword  OR o.jobTitle LIKE :keyword"),
    @NamedQuery(name="Offer.countOfferByKeyword",
                query="SELECT COUNT (o) FROM Offer o WHERE o.description LIKE :keyword OR o.requirements LIKE :keyword OR o.desired LIKE :keyword " +
                            "OR o.rDegreeName LIKE :keyword OR o.dDegreeName LIKE :keyword  OR o.jobTitle LIKE :keyword")
})
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableGenerator(name = "offerGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_OFFER_ID", allocationSize = 1, initialValue = 2)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "offerGen")
    private Long id;
    
    @Column(name="OFFER_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date offerDate;
    
    @Column(name="JOB_TITLE")
    private String jobTitle;
    
    @Column(name="DDEGREE_NAME")
    private String dDegreeName;

    @Column(name="RDEGREE_NAME")
    private String rDegreeName;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="REQUIREMENTS")
    private String requirements;

    @Column(name="DESIRED")
    private String desired;

    @Column(name="SALLARY")
    private String sallary;

    @Column(name="POSITIONS")
    private String positions;

    @Column(name = "ENABLED_OFFER")
    private boolean enabled;

    @ManyToOne
    @JoinColumn(name="FK_CONTRACT_ID")
    private Contract contract;

    @ManyToOne
    @JoinColumn(name="FK_TIMETABLE_ID")
    private Timetable timetable;

    @ManyToOne
    @JoinColumn(name="FK_EXPERIENCETIME_ID")
    private ExperienceTime experienceTime;

    @ManyToOne
    @JoinColumn(name="FK_TOWN_ID")
    private Town town;

    @ManyToOne
    @JoinColumn(name="FK_COMPANY_ID")
    private Concern company;

    @ManyToMany
    @JoinTable(name="OFFERSTATUS",joinColumns=@JoinColumn(name="FK_OFFER_ID"), inverseJoinColumns=@JoinColumn(name="FK_USERACCOUNT_ID"))
    private List<UserAccount> users = new ArrayList<UserAccount>();

    @ManyToMany
    @JoinTable(name="OFFER_2_AREA",joinColumns=@JoinColumn(name="FK_OFFER_ID"), inverseJoinColumns=@JoinColumn(name="FK_AREA_ID"))
    private List<SubArea>  areas = new ArrayList<SubArea>();

    @ManyToOne
    @JoinColumn(name="FK_DDEGREE_ID")
    private DegreeLevel desiredDegree;

    @ManyToOne
    @JoinColumn(name="FK_RDEGREE_ID")
    private DegreeLevel requiredDegree;


    public String getPositions() {

        return positions;

    }

    public void setPositions(String positions) {
        this.positions = positions;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Concern getCompany() {
        return company;
    }

    public void setCompany(Concern company) {
        this.company = company;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public List<SubArea> getAreas() {
        return areas;
    }

    public void setAreas(List<SubArea> areas) {
        this.areas = areas;
    }

    public String getSallary() {
        return sallary;
    }

    public void setSallary(String sallary) {
        this.sallary = sallary;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDesired() {
        return desired;
    }

    public void setDesired(String desired) {
        this.desired = desired;
    }

    public DegreeLevel getDesiredDegree() {
        return desiredDegree;
    }

    public void setDesiredDegree(DegreeLevel desiredDegree) {
        this.desiredDegree = desiredDegree;
    }

    public String getCustomOfferDate() {
        Locale l = Locale.getDefault();
        SimpleDateFormat df =  new SimpleDateFormat("dd-MM-yy",Locale.getDefault());
        return  df.format(offerDate);
    }
    public Date getOfferDate() {

        return offerDate;
    }

    public void setOfferDate(Date offerDate) {
        this.offerDate = offerDate;
    }

    public DegreeLevel getRequiredDegree() {
        return requiredDegree;
    }

    public void setRequiredDegree(DegreeLevel requiredDegree) {
        this.requiredDegree = requiredDegree;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public List<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(List<UserAccount> users) {
        this.users = users;
    }

    public ExperienceTime getExperienceTime() {
        return experienceTime;
    }

    public void setExperienceTime(ExperienceTime experienceTime) {
        this.experienceTime = experienceTime;
    }

    public String getDDegreeName() {
        return dDegreeName;
    }
    public String getDesiredDegreeName() {
        return dDegreeName;
    }
    public void setDDegreeName(String dDegreeName) {
        this.dDegreeName = dDegreeName;
    }

    public String getRDegreeName() {
        return rDegreeName;
    }
    public String getRequiredDegreeName() {
        return rDegreeName;
    }

    public void setRDegreeName(String rDegreeName) {
        this.rDegreeName = rDegreeName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offer)) {
            return false;
        }
        Offer other = (Offer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Offer[id=" + id + "]";
    }

}
