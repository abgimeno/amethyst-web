/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author abrahamgimeno
 */
@Entity
@Table(name="KNOWLEDGE")
public class Knowledge implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableGenerator(name = "knowledgeGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_KNOWLEDGE_ID", allocationSize = 1, initialValue = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "knowledgeGen")
    private Long id;

    @Column(name="KNOWLEDGE")
    private String knowledge;

    @Column(name="DESCRIPTION")
    private String description;
    
    @ManyToOne
    @JoinColumn(name="FK_KNOWLEDGEEXPERIENCE_ID")
    private ExperienceTime experience;

    @ManyToOne
    @JoinColumn(name="FK_KNOWLEDGELEVEL_ID")
    private KnowledgeLevel level;

    @ManyToOne
    @JoinColumn(name="FK_USER_ID")
    private UserAccount user;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public KnowledgeLevel getLevel() {
        return level;
    }

    public void setLevel(KnowledgeLevel level) {
        this.level = level;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public ExperienceTime getExperience() {
        return experience;
    }

    public void setExperience(ExperienceTime experience) {
        this.experience = experience;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Knowledge)) {
            return false;
        }
        Knowledge other = (Knowledge) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Knowledge[id=" + id + "]";
    }

}
