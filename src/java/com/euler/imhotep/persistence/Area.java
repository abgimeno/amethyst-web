/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Entity(name="Area")
@Table(name="AREA")
@NamedQueries({
    @NamedQuery(name="Area.getAllAreasFecthingSubAreas", query="SELECT a FROM Area a "),
    @NamedQuery(name="Area.getSubAreasId", query="SELECT DISTINCT sa.id FROM Area a JOIN a.subareas sa WHERE a.id =:areaId")

})
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="AREA")
    private String area;

    @OneToMany(mappedBy="parent")
    private List<SubArea> subareas= new ArrayList<SubArea>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<SubArea> getSubareas() {
        return subareas;
    }
    public Iterator<SubArea> getIterator() {
        return subareas.iterator();
    }
    public void setSubareas(List<SubArea> subareas) {
        this.subareas = subareas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Area[id=" + id + "]";
    }

}
