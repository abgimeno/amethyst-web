/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence.company;

import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.UserAccount;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author abraham
 */
@Entity
@Table(name="DISPATCH")
@NamedQueries({
    @NamedQuery(name="Dispatch.getPayed",query="SELECT d FROM Dispatch d WHERE d.payed = 1"),
    @NamedQuery(name="Dispatch.getNotPayed",query="SELECT d FROM Dispatch d WHERE d.payed = 0"),
    @NamedQuery(name="Dispatch.getByDate", query="SELECT d FROM Dispatch d WHERE d.dispatchDate > :startDate AND d.dispatchDate < :finishDate")
})
public class Dispatch implements Serializable {

    public Dispatch() {
    }

    public Dispatch(Concern company, boolean payed, Date dispatchDate) {
        this.company = company;
        this.payed = payed;
        this.dispatchDate = dispatchDate;
    }
    

    @Id
    @Column(name="DISPATCH_ID")
    // TODO - Generate i2d with seed from 1000
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="FK_COMPANY__ID")
    private Concern company;

    @ManyToMany
    @JoinTable(name="DISPATCH2USER",joinColumns=@JoinColumn(name="FK_DISPATCH_ID"), inverseJoinColumns=@JoinColumn(name="FK_USER_ID"))
    private List<UserAccount> users = new ArrayList<UserAccount>();

    @Column(name="PAYED")
    private boolean payed;

    @Column(name="DISPATCH_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dispatchDate;

    @Column(name="PAYMENT_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date paymentDate;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Concern getCompany() {
        return company;
    }

    public void setCompany(Concern company) {
        this.company = company;
    }

    public Date getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(Date dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public List<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(List<UserAccount> users) {
        this.users = users;
    }
    
    public String getCustomDispatchDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(dispatchDate);
    }
    public String getCustomPaymentDate() {
        SimpleDateFormat df =  new SimpleDateFormat("dd/MM/yy",Locale.getDefault());
        return  df.format(paymentDate);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Dispatch)) {
            return false;
        }
        Dispatch other = (Dispatch) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.company.Dispatched[id=" + id + "]";
    }

}
