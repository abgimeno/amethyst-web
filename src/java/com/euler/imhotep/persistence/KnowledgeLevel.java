/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abrahamgimeno
 */
@Entity
@Table(name="KNOWLEDGELEVEL")
public class KnowledgeLevel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKnowLevel() {
        return knowLevel;
    }

    public void setKnowLevel(String knowLevel) {
        this.knowLevel = knowLevel;
    }


    private String knowLevel;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KnowledgeLevel)) {
            return false;
        }
        KnowledgeLevel other = (KnowledgeLevel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.KnowledgeLevel[id=" + id + "]";
    }

}
