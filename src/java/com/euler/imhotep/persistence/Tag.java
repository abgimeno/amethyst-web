/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author abrahamgimeno
 */
@Entity
@Table(name="TAG")
@NamedQueries({
    @NamedQuery(name="Tag.findByUser",query="SELECT t FROM Tag t LEFT JOIN t.users tu WHERE tu.id =:id")
})

public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="NAME")
    private String name;

    @ManyToMany(mappedBy="tags")
    private List<UserAccount> users = new ArrayList<UserAccount>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(List<UserAccount> user) {
        this.users = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // PARAHACER:Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tag)) {
            return false;
        }
        Tag other = (Tag) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.imhotep.persistence.Tag[id=" + id + "]";
    }

}
