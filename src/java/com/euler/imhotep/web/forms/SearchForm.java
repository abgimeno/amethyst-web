/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.forms;

import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionNotNull;

/**
 *
 * @author nadiadaridou
 */
public class SearchForm extends Form {
    
    public static final String PARAM_SEARCH = "search";
    private Parameter search;
    
    
    public SearchForm() {
        search = new Parameter(PARAM_SEARCH, Parameter.ptype.STRING);
        search.addCondition(new ConditionNotNull());
        
    }
    
       public String getSearch() {
        return search.getStringValue();
    } 

}
