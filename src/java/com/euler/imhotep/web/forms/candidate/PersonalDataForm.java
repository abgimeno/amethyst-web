/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.candidate;

import com.euler.imhotep.persistence.UserAccount;
import com.euler.info.Address;
import com.euler.info.Person;
import com.euler.security.Cryptography;
import com.euler.web.handler.*;
import com.euler.web.handler.conditions.*;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nadia
 */
public class PersonalDataForm extends Form {

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "create_password";
    public static final String PARAM_PASSWORD2 = "create_password2";
    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME1 = "lastname1";
    public static final String PARAM_LASTNAME2 = "lastname2";
    public static final String PARAM_NICKNAME = "nickname";
    public static final String PARAM_BIRTHDAY = "birthday";
    public static final String PARAM_BIRTHMONTH = "birthmonth";
    public static final String PARAM_BIRTHYEAR = "birthyear";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_PROVINCE = "province";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_ZIP = "zip";
    private Parameter email;
    private Parameter firstname;
    private Parameter lastname1;
    private Parameter lastname2;
    private Parameter nickname;
    private Parameter birthday;
    private Parameter birthmonth;
    private Parameter birthyear;
    private Parameter address;
    private Parameter mobile;
    private Parameter city;
    private Parameter province;
    private Parameter country;
    private Parameter zip;
    private Parameter password;
    private Parameter password2;

    public PersonalDataForm(){
        init();
    }
    public PersonalDataForm(UserAccount user) {
        init();
        if(user != null){
            if (user.getPersonalInfo() != null) {
                firstname.setDefaultValue(user.getPersonalInfo().getName());
                lastname1.setDefaultValue(user.getPersonalInfo().getSurname1());
                lastname2.setDefaultValue(user.getPersonalInfo().getSurname2());

                GregorianCalendar grc = new GregorianCalendar();
                if (user.getPersonalInfo().getBirthdate() == null) {
                    grc.setTime(new Date());
                } else {
                    grc.setTime(user.getPersonalInfo().getBirthdate());
                }
                birthday.setDefaultValue("" + grc.get(GregorianCalendar.DAY_OF_MONTH));
                birthmonth.setDefaultValue("" + grc.get(GregorianCalendar.MONTH));
                birthyear.setDefaultValue("" + grc.get(GregorianCalendar.YEAR));
                if (user.getPersonalInfo().getAddress() != null) {
                    address.setDefaultValue(user.getPersonalInfo().getAddress().getAddress());
                    city.setDefaultValue(user.getPersonalInfo().getAddress().getTown());
                    province.setDefaultValue(user.getPersonalInfo().getAddress().getProvince());
                    country.setDefaultValue(user.getPersonalInfo().getAddress().getCountry());
                    zip.setDefaultValue(user.getPersonalInfo().getAddress().getZip());
                }
                mobile.setDefaultValue(user.getPersonalInfo().getMobilePhone());
            }
        }

    }
    private void init() {
        firstname = new Parameter(PARAM_FIRSTNAME, Parameter.ptype.STRING);
        firstname.addCondition(new ConditionNotNull());
        firstname.addCondition(new ConditionLength(0, 32));
        addParameter(firstname);

        lastname1 = new Parameter(PARAM_LASTNAME1, Parameter.ptype.STRING);
        lastname1.addCondition(new ConditionNotNull());
        lastname1.addCondition(new ConditionLength(0, 32));
        addParameter(lastname1);

        lastname2 = new Parameter(PARAM_LASTNAME2, Parameter.ptype.STRING);
        lastname2.addCondition(new ConditionLength(0, 32));
        addParameter(lastname2);

        nickname = new Parameter(PARAM_NICKNAME, Parameter.ptype.STRING);
        nickname.addCondition(new ConditionLength(0, 32));
        addParameter(nickname);

        birthday = new Parameter(PARAM_BIRTHDAY, Parameter.ptype.INT);
        birthday.addCondition(new ConditionLength(0, 32));
        // birthday.addCondition(new ConditionNotNull());
        addParameter(birthday);

        birthmonth = new Parameter(PARAM_BIRTHMONTH, Parameter.ptype.INT);
        birthmonth.addCondition(new ConditionLength(0, 32));
        // birthmonth.addCondition(new ConditionNotNull());
        addParameter(birthmonth);

        birthyear = new Parameter(PARAM_BIRTHYEAR, Parameter.ptype.INT);
        birthyear.addCondition(new ConditionLength(0, 32));
        // birthyear0.addCondition(new ConditionNotNull());
        addParameter(birthyear);

        address = new Parameter(PARAM_ADDRESS, Parameter.ptype.STRING);
        address.addCondition(new ConditionLength(0, 32));
        address.addCondition(new ConditionNotNull());
        addParameter(address);

        city = new Parameter(PARAM_CITY, Parameter.ptype.STRING);
        city.addCondition(new ConditionLength(0, 32));
        city.addCondition(new ConditionNotNull());
        addParameter(city);

        province = new Parameter(PARAM_PROVINCE, Parameter.ptype.STRING);
        province.addCondition(new ConditionLength(0, 32));
        addParameter(province);

        country = new Parameter(PARAM_COUNTRY, Parameter.ptype.STRING);
        country.addCondition(new ConditionLength(0, 32));
        addParameter(country);

        mobile = new Parameter(PARAM_MOBILE, Parameter.ptype.STRING);
        mobile.addCondition(new ConditionLength(0, 64));
        addParameter(mobile);

        zip = new Parameter(PARAM_ZIP, Parameter.ptype.STRING);
        zip.addCondition(new ConditionNotNull());
        zip.addCondition(new ConditionLength(0, 32));
        addParameter(zip);
    }
    public String getEmail() {
        return email.getStringValue();
    }

    public String getFirstName() {
        return firstname.getStringValue();
    }

    public String getLastName1() {
        return lastname1.getStringValue();
    }

    public String getLastName2() {
        return lastname2.getStringValue();
    }

    public String getNickName() {
        return nickname.getStringValue();
    }

    public Integer getBirthday() {
        return birthday.getIntegerValue();
    }

    public Integer getBirthmonth() {
        return birthmonth.getIntegerValue();
    }

    public Integer getBirthyear() {
        return birthyear.getIntegerValue();
    }

    public String getAddress() {
        return address.getStringValue();
    }

    public String getCity() {
        return city.getStringValue();
    }

    public String getProvince() {
        return province.getStringValue();
    }

    public String getCountry() {
        return country.getStringValue();
    }

    public String getMobile() {
        return mobile.getStringValue();
    }

    public String getZIP() {
        return zip.getStringValue();
    }

    public String getPassword() {
        return password.getStringValue();
    }

    public String getPassword2() {
        return password2.getStringValue();
    }

    @Override
    public void validate(Vector<String> errorlist) {
        super.validate(errorlist);

        if (password == null) {
            return;
        }

        if (getPassword() != null && getPassword2() != null) {
            if (!getPassword().equals(getPassword2())) {
                errorlist.add("password_notequals");
            }
        }
    }

    public void mapData(UserAccount user) {
        if (password != null) {
            user.setEmailAccount(getEmail());
            user.setPassword(Cryptography.cryptoPassword(getEmail(), getPassword()));
        }


        Person person = user.getPersonalInfo();
        person.setName(getFirstName());
        person.setSurname1(getLastName1());
        person.setSurname2(getLastName2());
        if(person.getAddress() == null){
            person.setAddress(new Address());
        }
        person.getAddress().setAddress(getAddress());
        person.getAddress().setTown(getCity());
        person.getAddress().setProvince(getProvince());
        person.getAddress().setCountry(getCountry());

        GregorianCalendar gc = new GregorianCalendar();
        gc.set(getBirthyear(), getBirthmonth(), getBirthday());
        person.setBirthdate(gc.getTime());
        person.setMobilePhone(getMobile());
        person.getAddress().setZip(getZIP());

    }
}


