/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.candidate;

import com.euler.imhotep.persistence.DegreeLevel;
import com.euler.imhotep.persistence.Study;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.DegreeLevelJpaController;
import com.euler.imhotep.web.servlets.FrontController;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author abrahamgimeno
 */
public class StudyForm extends Form {

    public static final String PARAM_DEGREENAME = "degree_name";
    public static final String PARAM_DEGREELEVEL = "degreelevel_id";
    public static final String PARAM_STARTDAY = "start_day";
    public static final String PARAM_STARTMONTH = "start_month";
    public static final String PARAM_STARTYEAR = "start_year";
    public static final String PARAM_FINISHDAY = "finish_day";
    public static final String PARAM_FINISHMONTH = "finish_month";
    public static final String PARAM_FINISHYEAR = "finish_year";
    public static final String PARAM_CURSANDO = "cursando";
    public static final String PARAM_UNIVERSITY = "university";
    
    private Parameter degreeName;
    private Parameter startDay;
    private Parameter startMonth;
    private Parameter startYear;
    private Parameter finishDay;
    private Parameter finishMonth;
    private Parameter finishYear;
    private Parameter cursando;
    private Parameter degreeLevelId;
    private Parameter university;

    //constructor for edit a study
    public StudyForm(Study study) {

        init();

        if (study != null) {
            
            GregorianCalendar grc = new GregorianCalendar();
            GregorianCalendar grec = new GregorianCalendar();
            grec.setTime(study.getStartDate());
            startDay.setDefaultValue("" + grec.get(GregorianCalendar.DAY_OF_MONTH));

            startMonth.setDefaultValue("" + grec.get(GregorianCalendar.MONTH));
            startYear.setDefaultValue("" + grec.get(GregorianCalendar.YEAR));

            degreeLevelId.setDefaultValue("" + study.getDegreeLevel().getId());

            university.setDefaultValue(study.getUniversityName());
            degreeName.setDefaultValue(study.getDegreeName());

            cursando.setBooleanValue(study.isCursando());
            if (getCursando() == true) {
                grc.set(0, 0, 0);
            } else {
                grc.setTime(study.getFinishDate());
                finishDay.setDefaultValue("" + grc.get(GregorianCalendar.DAY_OF_MONTH));
                finishMonth.setDefaultValue("" + grc.get(GregorianCalendar.MONTH));
                finishYear.setDefaultValue("" + grc.get(GregorianCalendar.YEAR));
            }
        }

    }

    //constructor for create a study
    public StudyForm() {

        init();

    }

    private void init() {


        startDay = new Parameter(PARAM_STARTDAY, Parameter.ptype.INT);
        startDay.addCondition(new ConditionLength(0, 32));
        addParameter(startDay);

        startMonth = new Parameter(PARAM_STARTMONTH, Parameter.ptype.INT);
        startMonth.addCondition(new ConditionLength(0, 32));
        addParameter(startMonth);

        startYear = new Parameter(PARAM_STARTYEAR, Parameter.ptype.INT);
        startYear.addCondition(new ConditionLength(0, 32));
        addParameter(startYear);

        finishDay = new Parameter(PARAM_FINISHDAY, Parameter.ptype.INT);
        finishDay.addCondition(new ConditionLength(0, 32));
        addParameter(finishDay);

        finishMonth = new Parameter(PARAM_FINISHMONTH, Parameter.ptype.INT);
        finishMonth.addCondition(new ConditionLength(0, 32));
        addParameter(finishMonth);

        finishYear = new Parameter(PARAM_FINISHYEAR, Parameter.ptype.INT);
        finishYear.addCondition(new ConditionLength(0, 32));
        addParameter(finishYear);

        cursando = new Parameter(PARAM_CURSANDO, Parameter.ptype.BOOLEAN);
        cursando.setBooleanValue(false);
        addParameter(cursando);

        degreeName = new Parameter(PARAM_DEGREENAME, Parameter.ptype.STRING);
        degreeName.addCondition(new ConditionNotNull());
        degreeName.addCondition(new ConditionLength(0, 255));
        addParameter(degreeName);

        university = new Parameter(PARAM_UNIVERSITY, Parameter.ptype.STRING);
        university.addCondition(new ConditionNotNull());
        university.addCondition(new ConditionLength(0, 255));
        addParameter(university);

        degreeLevelId = new Parameter(PARAM_DEGREELEVEL, Parameter.ptype.INT);
        degreeLevelId.addCondition(new ConditionNotNull());
        addParameter(degreeLevelId);

    }

    @Override
    public void validate(Vector<String> errorlist) {
        super.validate(errorlist);


    }

    public String getFinishDay() {
        return finishDay.getStringValue();
    }

    public String getFinishMonth() {
        return finishMonth.getStringValue();
    }

    public String getFinishYear() {
        return finishYear.getStringValue();
    }

    public int getStartDay() {
        return startDay.getIntegerValue();
    }

    public int getStartMonth() {
        return startMonth.getIntegerValue();
    }

    public int getStartYear() {
        return startYear.getIntegerValue();
    }

    public String getDegreeName() {
        return degreeName.getStringValue();
    }

    public String getUniversity() {
        return university.getStringValue();
    }

    public Boolean getCursando() {
       return cursando.getBooleanValue();
    }

    public int getDegreeLevelId() {
        return degreeLevelId.getIntegerValue();
    }

    public void mapData(UserAccount user) {

        Study study = new Study();
        List<Study> studies = user.getStudies();
        GregorianCalendar gc = new GregorianCalendar();

        gc.set(getStartYear(), getStartMonth(), getStartDay());
        study.setStartDate(gc.getTime());

        gc = new GregorianCalendar();
        
        if (getCursando() == true) {
            gc.set(0, 0, 0);
        }else{
            gc.set(Integer.parseInt(getFinishYear()), Integer.parseInt(getFinishMonth()), Integer.parseInt(getFinishDay()));
        }
        study.setFinishDate(gc.getTime());
        study.setDegreeName(getDegreeName());
        DegreeLevel dg = FrontController.degreeLevelCtl.findDegreeLevel(new Long(getDegreeLevelId()));
        study.setUniversityName(getUniversity());
        study.setDegreeLevel(dg);
        study.setUser(user);
        study.setCursando(getCursando());
        FrontController.studyCtl.create(study);
        studies.add(study);


    }
    //for edit
    public void mapData(Study study) {            

        GregorianCalendar gc = new GregorianCalendar();
        gc.set(getStartYear(), getStartMonth(), getStartDay());
        study.setStartDate(gc.getTime());
        gc = new GregorianCalendar();
        if (getCursando() == true) {
            gc.set(0, 0, 0);
        } else {
            gc.set(Integer.parseInt(getFinishYear()), Integer.parseInt(getFinishMonth()), Integer.parseInt(getFinishDay()));
        }
        study.setFinishDate(gc.getTime());
        study.setDegreeName(getDegreeName());
        study.setCursando(getCursando());
        DegreeLevel dg = FrontController.degreeLevelCtl.findDegreeLevel(new Long(getDegreeLevelId()));
        study.setUniversityName(getUniversity());
        study.setDegreeLevel(dg);

    }
}
