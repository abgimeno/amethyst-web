/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.candidate;

import com.euler.imhotep.persistence.UserAccount;
import com.euler.info.Person;
import com.euler.security.Cryptography;
import com.euler.web.handler.*;
import com.euler.web.handler.conditions.*;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class RegisterUserForm extends Form {

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "create_password";
    public static final String PARAM_PASSWORD2 = "create_password2";
    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME1 = "lastname1";
    public static final String PARAM_LASTNAME2 = "lastname2";
    public static final String PARAM_BIRTHDAY = "birthday";
    public static final String PARAM_BIRTHMONTH = "birthmonth";
    public static final String PARAM_BIRTHYEAR = "birthyear";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_ACCEPT = "accept";
    private Parameter email;
    private Parameter firstname;
    private Parameter lastname1;
    private Parameter lastname2;
    private Parameter birthday;
    private Parameter birthmonth;
    private Parameter birthyear;
    private Parameter mobile;
    private Parameter password;
    private Parameter password2;
    private Parameter accept;

    public RegisterUserForm() {

            init();

    }

    public RegisterUserForm(UserAccount user) {

        init();
        email.setDefaultValue(user.getEmailAccount());
        firstname.setDefaultValue(user.getPersonalInfo().getName());
        lastname1.setDefaultValue(user.getPersonalInfo().getSurname1());
        lastname2.setDefaultValue(user.getPersonalInfo().getSurname2());
        GregorianCalendar gc = new GregorianCalendar();
        if (user.getPersonalInfo().getBirthdate() == null) {
            gc.setTime(new Date());
        } else {
            gc.setTime(user.getPersonalInfo().getBirthdate());
        }
        birthday.setDefaultValue("" + gc.get(GregorianCalendar.DAY_OF_MONTH));
        birthmonth.setDefaultValue("" + gc.get(GregorianCalendar.MONTH));
        birthyear.setDefaultValue("" + gc.get(GregorianCalendar.YEAR));
        mobile.setDefaultValue(user.getPersonalInfo().getMobilePhone());


    }

    private void init() {
        try {
            email = new Parameter(PARAM_EMAIL, Parameter.ptype.STRING);
            email.addCondition(new ConditionNotNull());
            email.addCondition(new ConditionLength(0, 254));
            email.addCondition(new ConditionEmail());
            addParameter(email);
            password = new Parameter(PARAM_PASSWORD, Parameter.ptype.STRING);
            password.addCondition(new ConditionNotNull());
            password.addCondition(new ConditionLength(5, 20));
            addParameter(password);
            password2 = new Parameter(PARAM_PASSWORD2, Parameter.ptype.STRING);
            password2.addCondition(new ConditionNotNull());
            password2.addCondition(new ConditionLength(5, 20));
            addParameter(password2);
            firstname = new Parameter(PARAM_FIRSTNAME, Parameter.ptype.STRING);
            firstname.addCondition(new ConditionNotNull());
            firstname.addCondition(new ConditionLength(0, 32));
            addParameter(firstname);
            lastname1 = new Parameter(PARAM_LASTNAME1, Parameter.ptype.STRING);
            lastname1.addCondition(new ConditionNotNull());
            lastname1.addCondition(new ConditionLength(0, 32));
            addParameter(lastname1);
            lastname2 = new Parameter(PARAM_LASTNAME2, Parameter.ptype.STRING);
            lastname2.addCondition(new ConditionLength(0, 32));
            addParameter(lastname2);
            mobile = new Parameter(PARAM_MOBILE, Parameter.ptype.STRING);
            mobile.addCondition(new ConditionLength(0, 64));
            addParameter(mobile);
            birthday = new Parameter(PARAM_BIRTHDAY, Parameter.ptype.INT);
            birthday.addCondition(new ConditionLength(0, 32));
            // birthday.addCondition(new ConditionNotNull());
            addParameter(birthday);
            birthmonth = new Parameter(PARAM_BIRTHMONTH, Parameter.ptype.INT);
            birthmonth.addCondition(new ConditionLength(0, 32));
            // birthmonth.addCondition(new ConditionNotNull());
            addParameter(birthmonth);
            birthyear = new Parameter(PARAM_BIRTHYEAR, Parameter.ptype.INT);
            birthyear.addCondition(new ConditionLength(0, 32));
            // birthyear0.addCondition(new ConditionNotNull());
            addParameter(birthyear);
            GregorianCalendar gc = new GregorianCalendar();
            birthday.setDefaultValue("" + gc.get(GregorianCalendar.DAY_OF_MONTH));
            birthmonth.setDefaultValue("" + gc.get(GregorianCalendar.MONTH));
            birthyear.setDefaultValue("" + gc.get(GregorianCalendar.YEAR));
            accept = new Parameter(PARAM_ACCEPT, Parameter.ptype.BOOLEAN);
            addParameter(accept);
        } catch (RESyntaxException ex) {
            Logger.getLogger(RegisterUserForm.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public String getEmail() {
        return email.getStringValue();
    }

   public Boolean getAccept() {
       String s = accept.getStringValue();
        if (s == null) {
            return false;
        }
        return true;
    }

    public String getFirstName() {
        return firstname.getStringValue();
    }

    public String getLastName1() {
        return lastname1.getStringValue();
    }

    public String getLastName2() {
        return lastname2.getStringValue();
    }

    public String getMobile() {
        return mobile.getStringValue();
    }

    public Integer getBirthday() {
        return birthday.getIntegerValue();
    }

    public Integer getBirthmonth() {
        return birthmonth.getIntegerValue();
    }

    public Integer getBirthyear() {
        return birthyear.getIntegerValue();
    }

    public String getPassword() {
        return password.getStringValue();
    }

    public String getPassword2() {
        return password2.getStringValue();
    }

    @Override
    public void validate(Vector<String> errorlist) {
        super.validate(errorlist);

        if (password == null) {
            return;
        }

        if (getPassword() != null && getPassword2() != null) {
            if (!getPassword().equals(getPassword2())) {
                errorlist.add("password_notequals");
            }
        }

        if (getAccept() != true) {
                errorlist.add("accept_terms");
        }
    }

    public void mapData(UserAccount user) {
        if (password != null) {
            user.setEmailAccount(getEmail());
            user.setPassword(Cryptography.cryptoPassword(getEmail(), getPassword()));
        }

        Person person = user.getPersonalInfo();
        if (person == null) {
            person = new Person();
        }
        person.setName(getFirstName());
        person.setSurname1(getLastName1());
        person.setSurname2(getLastName2());
        person.setMobilePhone(getMobile());
        
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(getBirthyear(), getBirthmonth(), getBirthday());
        person.setBirthdate(gc.getTime());
        user.setPersonalInfo(person);
        user.setCurriculumDate(new Date());
    }
}


