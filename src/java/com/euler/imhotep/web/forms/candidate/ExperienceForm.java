/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.candidate;

import com.euler.imhotep.persistence.Area;
import com.euler.imhotep.persistence.Experience;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.AreaJpaController;
import com.euler.imhotep.persistence.controllers.ExperienceJpaController;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrahamgimeno
 */
public class ExperienceForm extends Form {

    public static final String PARAM_STARTDAY = "start_day";
    public static final String PARAM_STARTMONTH = "start_month";
    public static final String PARAM_STARTYEAR = "start_year";
    public static final String PARAM_FINISHDAY = "finish_day";
    public static final String PARAM_FINISHMONTH = "finish_month";
    public static final String PARAM_FINISHYEAR = "finish_year";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_POSITION = "position";
    public static final String PARAM_COMPANY = "company";
    public static final String PARAM_AREAID = "area_id";
    public static final String PARAM_CURSANDO = "currently";
    private Parameter startDay;
    private Parameter startMonth;
    private Parameter startYear;
    private Parameter finishDay;
    private Parameter finishMonth;
    private Parameter finishYear;
    private Parameter cursando;
    private Parameter description;
    private Parameter position;
    private Parameter company;
    private Parameter areaId;
    private AreaJpaController areaCtl;

    public ExperienceForm(Experience experience) {

        init();
        if (experience != null) {
            GregorianCalendar grc = new GregorianCalendar();
            GregorianCalendar grec = new GregorianCalendar();

            grec.setTime(experience.getStartDate());
            startDay.setDefaultValue("" + grec.get(GregorianCalendar.DAY_OF_MONTH));
            startMonth.setDefaultValue("" + grec.get(GregorianCalendar.MONTH));
            startYear.setDefaultValue("" + grec.get(GregorianCalendar.YEAR));


            grc.setTime(experience.getFinishDate());
            finishDay.setDefaultValue("" + grc.get(GregorianCalendar.DAY_OF_MONTH));
            finishMonth.setDefaultValue("" + grc.get(GregorianCalendar.MONTH));
            finishYear.setDefaultValue("" + grc.get(GregorianCalendar.YEAR));

            cursando.setBooleanValue(experience.isCurrently());
            if (getCursando() == true) {
                grc.set(0, 0, 0);
            } else {
                grc.setTime(experience.getFinishDate());
                finishDay.setDefaultValue("" + grc.get(GregorianCalendar.DAY_OF_MONTH));
                finishMonth.setDefaultValue("" + grc.get(GregorianCalendar.MONTH));
                finishYear.setDefaultValue("" + grc.get(GregorianCalendar.YEAR));
            }

            description.setDefaultValue(experience.getDescription());
            position.setDefaultValue(experience.getPosition());
            company.setDefaultValue(experience.getCompany());
            areaId.setDefaultValue(experience.getArea().getId() + "");

        }

    }
      public ExperienceForm() {
          init();
      }

    public ExperienceForm(UserAccount user) {

        areaCtl = new AreaJpaController();
        init();

    }

    private void init() {
        startDay = new Parameter(PARAM_STARTDAY, Parameter.ptype.INT);
        startDay.addCondition(new ConditionLength(0, 32));
        addParameter(startDay);

        startMonth = new Parameter(PARAM_STARTMONTH, Parameter.ptype.INT);
        startMonth.addCondition(new ConditionLength(0, 32));
        addParameter(startMonth);

        startYear = new Parameter(PARAM_STARTYEAR, Parameter.ptype.INT);
        startYear.addCondition(new ConditionLength(0, 32));
        addParameter(startYear);

        finishDay = new Parameter(PARAM_FINISHDAY, Parameter.ptype.INT);
        finishDay.addCondition(new ConditionLength(0, 32));
        addParameter(finishDay);

        finishMonth = new Parameter(PARAM_FINISHMONTH, Parameter.ptype.INT);
        finishMonth.addCondition(new ConditionLength(0, 32));
        addParameter(finishMonth);

        finishYear = new Parameter(PARAM_FINISHYEAR, Parameter.ptype.INT);
        finishYear.addCondition(new ConditionLength(0, 32));
        addParameter(finishYear);

        cursando = new Parameter(PARAM_CURSANDO, Parameter.ptype.BOOLEAN);
        cursando.setBooleanValue(false);
        addParameter(cursando);

        description = new Parameter(PARAM_DESCRIPTION, Parameter.ptype.STRING);
        description.addCondition(new ConditionLength(0, 1500));
        addParameter(description);

        position = new Parameter(PARAM_POSITION, Parameter.ptype.STRING);
        position.addCondition(new ConditionNotNull());
        position.addCondition(new ConditionLength(0, 255));
        addParameter(position);

        company = new Parameter(PARAM_COMPANY, Parameter.ptype.STRING);
        company.addCondition(new ConditionNotNull());
        company.addCondition(new ConditionLength(0, 64));
        addParameter(company);

        areaId = new Parameter(PARAM_AREAID, Parameter.ptype.INT);
        areaId.addCondition(new ConditionNotNull());
        addParameter(areaId);
    }

    @Override
    public void validate(Vector<String> errorlist) {
        //super.validate(errorlist);


        company.validate(errorlist);
        description.validate(errorlist);
        startDay.validate(errorlist);
        startMonth.validate(errorlist);
        startYear.validate(errorlist);
        position.validate(errorlist);
        areaId.validate(errorlist);
        startDay.validate(errorlist);

        if (!getCursando()) {
            finishDay.validate(errorlist);
            finishMonth.validate(errorlist);
            finishYear.validate(errorlist);
        }
    }

    public String getCompany() {
        return company.getStringValue();
    }

    public String getDescription() {
        return description.getStringValue();
    }

    public String getFinishDay() {
        return finishDay.getStringValue();
    }

    public String getFinishMonth() {
        return finishMonth.getStringValue();
    }

    public String getFinishYear() {
        return finishYear.getStringValue();
    }

    public Boolean getCursando() {
        return cursando.getBooleanValue();
    }

    public String getPosition() {
        return position.getStringValue();
    }

    public int getStartDay() {
        return startDay.getIntegerValue();
    }

    public int getStartMonth() {
        return startMonth.getIntegerValue();
    }

    public int getStartYear() {
        return startYear.getIntegerValue();
    }

    public int getAreaId() {
        return areaId.getIntegerValue();
    }
//for create

    public void mapData(UserAccount user) {

        Experience exp = new Experience();
        List<Experience> experiences = user.getExperiences();
        GregorianCalendar gc = new GregorianCalendar();
        ExperienceJpaController expCtl = new ExperienceJpaController();
        gc.set(getStartYear(), getStartMonth(), getStartDay());
        exp.setStartDate(gc.getTime());
        gc = new GregorianCalendar();
        if (getCursando() == true) {
            gc.set(0, 0, 0);
        } else {
            gc.set(Integer.parseInt(getFinishYear()), Integer.parseInt(getFinishMonth()), Integer.parseInt(getFinishDay()));
        }
        exp.setFinishDate(gc.getTime());
        exp.setDescription(getDescription());
        exp.setCompany(getCompany());
        exp.setPosition(getPosition());
        exp.setCurrently(getCursando());
        Area area = areaCtl.findArea(new Long(getAreaId()));
        exp.setArea(area);
        exp.setUser(user);
        expCtl.create(exp);
        experiences.add(exp);
        user.setExperiences(experiences);

    }
//for edit

    public void mapData(Experience experience) {

        GregorianCalendar gc = new GregorianCalendar();

        gc.set(getStartYear(), getStartMonth(), getStartDay());
        experience.setStartDate(gc.getTime());
        gc = new GregorianCalendar();
        if (getCursando() == true) {
            gc.set(0, 0, 0);
        } else {
            gc.set(Integer.parseInt(getFinishYear()), Integer.parseInt(getFinishMonth()), Integer.parseInt(getFinishDay()));

        }
        experience.setCurrently(getCursando());
        experience.setFinishDate(gc.getTime());
        experience.setDescription(getDescription());
        experience.setCompany(getCompany());
        experience.setPosition(getPosition());
        Area area = areaCtl.findArea(new Long(getAreaId()));
        experience.setArea(area);


    }
}
