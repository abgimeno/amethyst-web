/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.candidate;

import com.euler.imhotep.persistence.ExperienceTime;
import com.euler.imhotep.persistence.Knowledge;
import com.euler.imhotep.persistence.KnowledgeLevel;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.ExperienceTimeJpaController;
import com.euler.imhotep.persistence.controllers.KnowledgeLevelJpaController;
import com.euler.imhotep.web.servlets.FrontController;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abrahamgimeno
 */
public class KnowledgeForm extends Form {

    public static final String PARAM_KNOWLEDGE = "knowledge";
    public static final String PARAM_KNOWLEDGE_LEVEL = "knowledge_level";
    public static final String PARAM_KNOWLEDGE_EXPERIENCE = "knowledge_experience";
    public static final String PARAM_DESCRIPTION = "description";

    private Parameter knowledge;
    private Parameter knowledge_level;
    private Parameter knowledge_experience;
    private Parameter description;

    public KnowledgeForm() {
        init();
    }

    public KnowledgeForm(Knowledge conocimiento) {
        init();
        try {

            knowledge.setDefaultValue(conocimiento.getKnowledge());
            knowledge_level.setDefaultValue(conocimiento.getLevel().getId() + "");
            knowledge_experience.setDefaultValue(conocimiento.getExperience().getId() + "");
            description.setDefaultValue(conocimiento.getDescription());
            
        } catch (NullPointerException excNull) {
            Logger.getLogger(KnowledgeForm.class.getName()).log(Level.SEVERE, "KnowledgeForm::constructor(Knowledge conocimiento)", excNull);
        }


    }

    public void init() {
        knowledge = new Parameter(PARAM_KNOWLEDGE, Parameter.ptype.STRING);
        knowledge.addCondition(new ConditionNotNull());
        knowledge.addCondition(new ConditionLength(0, 76));
        addParameter(knowledge);

        knowledge_level = new Parameter(PARAM_KNOWLEDGE_LEVEL, Parameter.ptype.INT);
        //knowledge_level.addCondition(new ConditionLength(0, 32));
        addParameter(knowledge_level);

        knowledge_experience = new Parameter(PARAM_KNOWLEDGE_EXPERIENCE, Parameter.ptype.INT);
        knowledge_experience.addCondition(new ConditionLength(0, 32));
        addParameter(knowledge_experience);


        description = new Parameter(PARAM_DESCRIPTION, Parameter.ptype.STRING);
        addParameter(description);
    }

    public String getKnowledge() {
        return knowledge.getStringValue();
    }

    public String getDescription() {
        return description.getStringValue();
    }

    public int getKnowledgeLevel() {
        return knowledge_level.getIntegerValue();
    }

    public int getKnowledgeExperience() {
        return knowledge_experience.getIntegerValue();
    }

    public void mapData(UserAccount user) {
        try {
            user = FrontController.userCtl.findUserAccount(user.getId());
            Knowledge conocimiento = new Knowledge();
            ExperienceTimeJpaController expTCtl = new ExperienceTimeJpaController();
            ExperienceTime expTime = expTCtl.findExperienceTime((long) getKnowledgeExperience());
            KnowledgeLevelJpaController knowLCtl = new KnowledgeLevelJpaController();
            KnowledgeLevel knowledgeLevel = knowLCtl.findKnowledgeLevel((long) getKnowledgeLevel());

            conocimiento.setDescription(getDescription());
            conocimiento.setExperience(expTime);
            conocimiento.setLevel(knowledgeLevel);
            conocimiento.setKnowledge(getKnowledge());
            conocimiento.setUser(user);            
            FrontController.knowledgeCtl.create(conocimiento);
            user.getKnowledges().add(conocimiento);
            user.setCurriculumDate(new Date());
            FrontController.userCtl.edit(user);
            
        } catch (Exception exc) {
            Logger.getLogger(KnowledgeForm.class.getName()).log(Level.SEVERE, "KnowledgeForm::mapData(UserAccount user) ", exc);
        }

    }

    public void mapData(Knowledge conocimiento) {
        try {
            ExperienceTimeJpaController expTCtl = new ExperienceTimeJpaController();
            ExperienceTime expTime = expTCtl.findExperienceTime((long) getKnowledgeExperience());
            KnowledgeLevelJpaController knowLCtl = new KnowledgeLevelJpaController();
            KnowledgeLevel knowledgeLevel = knowLCtl.findKnowledgeLevel((long) getKnowledgeLevel());
            conocimiento.setDescription(getDescription());
            conocimiento.setExperience(expTime);
            conocimiento.setLevel(knowledgeLevel);
            conocimiento.setKnowledge(getKnowledge());

        } catch (Exception exc) {
            Logger.getLogger(KnowledgeForm.class.getName()).log(Level.SEVERE, "KnowledgeForm::mapData(Knowledge conocimiento) ", exc);
        }

    }
}
