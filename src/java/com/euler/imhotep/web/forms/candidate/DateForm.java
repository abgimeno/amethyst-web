/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.forms.candidate;

import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author abrahamgimeno
 */
public class DateForm extends Form{

    public static final String PARAM_BIRTHDAY = "birthday";
    public static final String PARAM_BIRTHMONTH = "birthmonth";
    public static final String PARAM_BIRTHYEAR = "birthyear";

    private Parameter birthday;
    private Parameter birthmonth;
    private Parameter birthyear;

    public DateForm() {
        init();
    }

    private void init(){
        birthday = new Parameter(PARAM_BIRTHDAY, Parameter.ptype.INT);
        birthday.addCondition(new ConditionLength(0, 32));
        // birthday.addCondition(new ConditionNotNull());
        addParameter(birthday);
        birthmonth = new Parameter(PARAM_BIRTHMONTH, Parameter.ptype.INT);
        birthmonth.addCondition(new ConditionLength(0, 32));
        // birthmonth.addCondition(new ConditionNotNull());
        addParameter(birthmonth);
        birthyear = new Parameter(PARAM_BIRTHYEAR, Parameter.ptype.INT);
        birthyear.addCondition(new ConditionLength(0, 32));
        // birthyear0.addCondition(new ConditionNotNull());
        addParameter(birthyear);
        GregorianCalendar gc = new GregorianCalendar();
        birthday.setDefaultValue("" + gc.get(GregorianCalendar.DAY_OF_MONTH));
        birthmonth.setDefaultValue("" + gc.get(GregorianCalendar.MONTH));
        birthyear.setDefaultValue("" + gc.get(GregorianCalendar.YEAR));
    }

    public Integer getBirthday() {
        return birthday.getIntegerValue();
    }

    public Integer getBirthmonth() {
        return birthmonth.getIntegerValue();
    }

    public Integer getBirthyear() {
        return birthyear.getIntegerValue();
    }
    public Date mapData(){
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(getBirthyear(), getBirthmonth(), getBirthday());
        return gc.getTime();

    }
}
