/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.forms;

import com.euler.imhotep.persistence.info.Noticia;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionNotNull;

/**
 *
 * @author abrahamgimeno
 */
public class NoticiaForm extends Form{

    public static final String PARAM_NEWID="id";
    public static final String PARAM_TEXT="text";
    public static final String PARAM_TITLE="title";

    protected Parameter id;
    protected Parameter text;
    protected Parameter title;

    public NoticiaForm(){
        init();
    }

    public NoticiaForm(Noticia not){
        init();
        id.setDefaultValue(not.getId()+"");
        text.setDefaultValue(not.getTexto());
        title.setDefaultValue(not.getTitle());        
    }
    private void init(){
        id = new Parameter(PARAM_NEWID, Parameter.ptype.STRING);
        addParameter(id);

        text = new Parameter(PARAM_TEXT, Parameter.ptype.STRING);
        text.addCondition(new ConditionNotNull());
        addParameter(text);

        title = new Parameter(PARAM_TITLE, Parameter.ptype.STRING);
        title.addCondition(new ConditionNotNull());
        addParameter(title);
    }
    public String getId(){
        return this.id.getStringValue();
    }
    public String getText(){
        return this.text.getStringValue();
    }

    public String getTitle(){
        return this.title.getStringValue();
    }
    public void mapData(Noticia noticia){
        noticia.setTitle(getTitle());
        noticia.setTexto(getText());
    }
}
