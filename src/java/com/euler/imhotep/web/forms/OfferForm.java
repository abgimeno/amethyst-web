/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms;

import com.euler.imhotep.persistence.Area;
import com.euler.imhotep.persistence.DegreeLevel;
import com.euler.imhotep.persistence.ExperienceTime;
import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.SubArea;
import com.euler.imhotep.persistence.controllers.DegreeLevelJpaController;
import com.euler.imhotep.persistence.controllers.ExperienceTimeJpaController;
import com.euler.imhotep.persistence.controllers.SubAreaJpaController;
import com.euler.imhotep.persistence.controllers.TownJpaController;
import com.euler.imhotep.web.servlets.FrontController;
import com.euler.info.Town;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abrahamgimeno
 */
public class OfferForm extends Form {

    public static final String PARAM_RDEGREE = "rdegree";
    public static final String PARAM_DDEGREE = "ddegree";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_YEARSEXP = "years_exp";
    public static final String PARAM_RREQUIREMENTS = "required";
    public static final String PARAM_DREQUIREMENTS = "desired";
    public static final String PARAM_AREA = "area_id";
    public static final String PARAM_RDEGREELEVEL = "rdegreeLevel";
    public static final String PARAM_DDEGREELEVEL = "ddegreeLevel";
    public static final String PARAM_SUBAREAS = "selected_subareas";
    public static final String PARAM_TITLE = "job_title";
    public static final String PARAM_SALLARY = "sallary";
    public static final String PARAM_TOWN = "town";
    public static final String PARAM_TIMETABLE="timetable";
    public static final String PARAM_CONTRACT="contract";
    public static final String PARAM_POSITIONS="positions";
    
    public static final int NSUBAREAS = 15;
    public static final int NAREAS = 15;
    private Parameter rDegree;
    private Parameter dDegree;
    private Parameter description;
    private Parameter yearsExp;
    private Parameter required;
    private Parameter desired;
    private Parameter areas;
    private Parameter subAreas;
    private Parameter ddegreeLevel;
    private Parameter rdegreeLevel;
    private Parameter jobTitle;
    private Parameter sallary;
    private Parameter town;
    private Parameter positions;
    private Parameter timetable;
    private Parameter contract;

    public OfferForm() {

        init();

    }

    public OfferForm(Offer offer) {

        init();

        jobTitle.setDefaultValue(offer.getJobTitle());
        sallary.setDefaultValue(offer.getSallary());
        rDegree.setDefaultValue(offer.getRDegreeName());
        dDegree.setDefaultValue(offer.getDDegreeName());
        description.setDefaultValue(offer.getDescription());
        required.setDefaultValue(offer.getRequirements());
        desired.setDefaultValue(offer.getDesired());
        if (offer.getDesiredDegree() != null){
            ddegreeLevel.setDefaultValue("" + offer.getDesiredDegree().getId());
        }
        if(offer.getRequiredDegree() != null){
            rdegreeLevel.setDefaultValue("" + offer.getRequiredDegree().getId());
        }
        if(offer.getContract() != null){
            contract.setDefaultValue(""+offer.getContract().getId());
        }
        positions.setDefaultValue(offer.getPositions());
        if(offer.getTimetable() != null){
            timetable.setDefaultValue(""+offer.getTimetable().getId());
        }
        if(offer.getExperienceTime() != null){
            yearsExp.setDefaultValue(""+offer.getExperienceTime().getId());
           }
        if(offer.getTown() != null){
            town.setDefaultValue(""+offer.getTown().getId());
        }
    }

    public void init() {

        jobTitle = new Parameter(PARAM_TITLE, Parameter.ptype.STRING);
        jobTitle.addCondition(new ConditionNotNull());
        jobTitle.addCondition(new ConditionLength(0, 64));
        jobTitle.setDefaultValue("Oferta de trabajo en ");
        addParameter(jobTitle);

        sallary = new Parameter(PARAM_SALLARY, Parameter.ptype.STRING);
        sallary.addCondition(new ConditionLength(0, 32));
        addParameter(sallary);

        rDegree = new Parameter(PARAM_RDEGREE, Parameter.ptype.STRING);
        rDegree.addCondition(new ConditionLength(0, 255));
        addParameter(rDegree);


        dDegree = new Parameter(PARAM_DDEGREE, Parameter.ptype.STRING);
        dDegree.addCondition(new ConditionLength(0, 255));
        addParameter(dDegree);

        description = new Parameter(PARAM_DESCRIPTION, Parameter.ptype.STRING);
        addParameter(description);

        yearsExp = new Parameter(PARAM_YEARSEXP, Parameter.ptype.STRING);
        addParameter(yearsExp);

        required = new Parameter(PARAM_RREQUIREMENTS, Parameter.ptype.STRING);
        required.addCondition(new ConditionNotNull());
        addParameter(required);

        desired = new Parameter(PARAM_DREQUIREMENTS, Parameter.ptype.STRING);
        addParameter(desired);

        ddegreeLevel = new Parameter(PARAM_DDEGREELEVEL, Parameter.ptype.STRING);
        ddegreeLevel.addCondition(new ConditionNotNull());
        addParameter(ddegreeLevel);

        rdegreeLevel = new Parameter(PARAM_RDEGREELEVEL, Parameter.ptype.STRING);
        addParameter(rdegreeLevel);

        subAreas = new Parameter(PARAM_SUBAREAS, Parameter.ptype.INT);
        //subAreas.addCondition(new ConditionNotNull());
        addParameter(subAreas);

        town = new Parameter(PARAM_TOWN, Parameter.ptype.STRING);
        addParameter(town);

        contract = new Parameter(PARAM_CONTRACT,Parameter.ptype.STRING);
        contract.addCondition(new ConditionLength(0, 255));
        addParameter(contract);

        timetable = new Parameter(PARAM_TIMETABLE,Parameter.ptype.STRING);
        timetable.addCondition(new ConditionLength(0, 255));
        addParameter(timetable);

        positions = new Parameter(PARAM_POSITIONS,Parameter.ptype.STRING);
        positions.addCondition(new ConditionLength(0, 255));
        addParameter(positions);
    }

    public String getDesiredDegree() {
        return dDegree.getStringValue();
    }

    public String getDescription() {
        return description.getStringValue();
    }

    public String getDesired() {
        return desired.getStringValue();
    }

    public String getRequiredDegree() {
        return rDegree.getStringValue();
    }

    public String getRequired() {
        return required.getStringValue();
    }

    public String getYearsExp() {
        return yearsExp.getStringValue();
    }

    public String getDesiredDegreeLevel() {
        return ddegreeLevel.getStringValue();
    }

    public String getRequiredDegreeLevel() {
        return rdegreeLevel.getStringValue();
    }

    public Integer[] getSubAreas() {
        return subAreas.getIntegerValues();
    }

    public String getJobTitle() {
        return jobTitle.getStringValue();
    }

    public String getSallary() {
        return sallary.getStringValue();
    }

    public String getTown() {
        return town.getStringValue();
    }

    public String getContract(){
        return contract.getStringValue();
    }

    public String getTimetable(){
        return timetable.getStringValue();
    }
    public String getPositions(){
        return positions.getStringValue();
    }

    public void mapData(Offer offer) {

        DegreeLevelJpaController degreeLevelCtl = new DegreeLevelJpaController();
        DegreeLevel desiredD, requiredD;
        desiredD = degreeLevelCtl.findDegreeLevel(Long.parseLong(getDesiredDegreeLevel()));
        offer.setDesiredDegree(desiredD);
        requiredD = degreeLevelCtl.findDegreeLevel(Long.parseLong(getRequiredDegreeLevel()));
        offer.setRequiredDegree(requiredD);

        List<SubArea> sareas = offer.getAreas();
        if (sareas.isEmpty()) {
            sareas = new ArrayList<SubArea>();
        }
        for (Integer i : subAreas.getIntegerValues()) {
            Area a = FrontController.areaCtl.findArea((long)i);
            for(SubArea sa:a.getSubareas()){
                if(!sareas.contains(sa)){
                    sareas.add(sa);
                }
            }
        }

        offer.setContract(FrontController.contractCtl.findContract(Long.parseLong(getContract())));
        offer.setTimetable(FrontController.timetableCtl.findTimetable(Long.parseLong(getTimetable())));
        offer.setPositions(getPositions());
        offer.setSallary(getSallary());
        offer.setJobTitle(getJobTitle());
        offer.setAreas(sareas);
        ExperienceTimeJpaController expCtl = new ExperienceTimeJpaController();
        ExperienceTime exp = expCtl.findExperienceTime(Long.parseLong(getYearsExp()));
        offer.setExperienceTime(exp);
        offer.setDescription(getDescription());
        offer.setDDegreeName(getDesiredDegree());
        offer.setRequirements(getRequired());
        offer.setDesired(getDesired());
        offer.setRDegreeName(getRequiredDegree());
        offer.setEnabled(true);
        offer.setTown( FrontController.townCtl.findTown(Long.parseLong(getTown())));
        offer.setOfferDate(new Date());
    }
}
