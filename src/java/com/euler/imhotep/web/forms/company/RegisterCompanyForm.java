/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.company;

import com.euler.imhotep.persistence.Concern;
import com.euler.info.Company;
import com.euler.security.Cryptography;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionEmail;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class RegisterCompanyForm extends Form {

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "create_password";
    public static final String PARAM_PASSWORD2 = "create_password2";
    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME1 = "lastname1";
    public static final String PARAM_LASTNAME2 = "lastname2";
    public static final String PARAM_COMPNAME = "company_name";
    public static final String PARAM_PHONENUMBER = "company_phone";
    public static final String PARAM_TIMETABLE = "timetable";
    public static final String PARAM_ACCEPT = "accept";
    
    private Parameter email;
    private Parameter firstname;
    private Parameter lastname1;
    private Parameter lastname2;
    private Parameter companyName;
    private Parameter companyPhone;
    private Parameter password;
    private Parameter password2;
    private Parameter timetable;
    private Parameter accept;

    public RegisterCompanyForm() {

        init();

        companyName = new Parameter(PARAM_COMPNAME, Parameter.ptype.STRING);
        companyName.addCondition(new ConditionNotNull());
        companyName.addCondition(new ConditionLength(0, 64));
        addParameter(companyName);

        companyPhone = new Parameter(PARAM_PHONENUMBER, Parameter.ptype.STRING);
        companyPhone.addCondition(new ConditionNotNull());
        companyPhone.addCondition(new ConditionLength(0, 64));
        addParameter(companyPhone);

    }

    private void init() {
        try {
            email = new Parameter(PARAM_EMAIL, Parameter.ptype.STRING);
            email.addCondition(new ConditionNotNull());
            email.addCondition(new ConditionLength(0, 254));
            email.addCondition(new ConditionEmail());
            addParameter(email);

            password = new Parameter(PARAM_PASSWORD, Parameter.ptype.STRING);
            password.addCondition(new ConditionNotNull());
            password.addCondition(new ConditionLength(5, 20));
            addParameter(password);

            password2 = new Parameter(PARAM_PASSWORD2, Parameter.ptype.STRING);
            password2.addCondition(new ConditionNotNull());
            password2.addCondition(new ConditionLength(5, 20));
            addParameter(password2);

            firstname = new Parameter(PARAM_FIRSTNAME, Parameter.ptype.STRING);
            firstname.addCondition(new ConditionNotNull());
            firstname.addCondition(new ConditionLength(0, 32));
            addParameter(firstname);

            lastname1 = new Parameter(PARAM_LASTNAME1, Parameter.ptype.STRING);
            lastname1.addCondition(new ConditionNotNull());
            lastname1.addCondition(new ConditionLength(0, 32));
            addParameter(lastname1);

            lastname2 = new Parameter(PARAM_LASTNAME2, Parameter.ptype.STRING);
            lastname2.addCondition(new ConditionLength(0, 32));
            addParameter(lastname2);

            timetable = new Parameter(PARAM_TIMETABLE, Parameter.ptype.STRING);
            addParameter(timetable);

            accept = new Parameter(PARAM_ACCEPT, Parameter.ptype.BOOLEAN);
            addParameter(accept);

        } catch (RESyntaxException ex) {
            Logger.getLogger(RegisterCompanyForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String getCompanyPhone() {
        return companyPhone.getStringValue();
    }

    private String getCompanyName() {
        return companyName.getStringValue();
    }

    public String getEmail() {
        return email.getStringValue();
    }

    public String getFirstName() {
        return firstname.getStringValue();
    }

    public String getLastName1() {
        return lastname1.getStringValue();
    }

    public String getPassword() {
        return password.getStringValue();
    }

    public String getPassword2() {
        return password2.getStringValue();
    }

    public String getTimetable() {
        return timetable.getStringValue();
    }

    public Boolean getAccept() {
         String s = accept.getStringValue();
        if (s == null) {
            return false;
        }
        return true;
    }

    @Override
    public void validate(Vector<String> errorlist) {
        super.validate(errorlist);

        if (password == null) {
            return;
        }

        if (getPassword() != null && getPassword2() != null) {
            if (!getPassword().equals(getPassword2())) {
                errorlist.add("password_notequals");
            }
        }

        if (getAccept() != true) {
            errorlist.add("accept_terms");
        }
    }

    public void mapData(Concern company) {
        if (password != null) {
            company.setEmailAccount(getEmail());
            company.setPassword(Cryptography.cryptoPassword(getEmail(), getPassword()));
        }

        Company comp = company.getCompany();
        if (comp == null) {
            comp = new Company();
        }
        comp.setEmail(getEmail());
        comp.setCompanyName(getCompanyName());
        comp.setPhoneNumber(getCompanyPhone());
        company.setCompany(comp);
        company.setFirstName(getFirstName());
        company.setSurname1(getLastName1());
        //company.setSurname2(getLastName2());
        company.setTimetable(getTimetable());

    }
}
