/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.company;

import com.euler.imhotep.persistence.Tag;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham gimeno
 */
public class TagForm extends Form {

    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";

    /*1*/
    private Parameter id;
    private Parameter name;

    /*2*/
    public TagForm() {
        init();
    }

    public TagForm(Tag entity) {
        init();
        id.setDefaultValue("" + entity.getId());
        
        name.setDefaultValue(entity.getName());


    /*3*/
    }

    public void init() {
        id = new Parameter(PARAM_ID, Parameter.ptype.STRING);
        addParameter(id);
        name = new Parameter(PARAM_NAME, Parameter.ptype.STRING);
        name.addCondition(new ConditionNotNull());
        name.addCondition(new ConditionLength(0, 255));
        addParameter(name);

    /*4*/
    }

    public String getId() {
        return id.getStringValue();
    }

    public String getName() {
        return name.getStringValue();
    }

    /*5*/
    public void mapData(Tag tag) {
        try {
            tag.setName(getName());
        } catch (Exception exc) {
            Logger.getLogger(TagForm.class.getName()).log(Level.SEVERE, "TagForm::mapData(Tag tag) ", exc);
        }
    }
}

