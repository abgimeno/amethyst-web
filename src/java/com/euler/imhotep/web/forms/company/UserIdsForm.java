/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.forms.company;
import com.euler.imhotep.persistence.Tag;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author abraham
 */
public class UserIdsForm extends Form{



    private static final String PARAM_TAGS= "user_id";

    /*1*/

    private Parameter userIds;

    /*2*/
    public UserIdsForm() {
        init();
    }

    public UserIdsForm(Tag entity) {
        init();


    /*3*/
    }

    public void init() {
        userIds = new Parameter(PARAM_TAGS, Parameter.ptype.INT);
        userIds.addCondition(new ConditionNotNull());
        addParameter(userIds);

    /*4*/
    }

    public Integer[] getName() {
        return userIds.getIntegerValues();
    }

    /*5*/
    public void mapData(List<Long> ids) {
        try {
            for(Integer i : userIds.getIntegerValues()){
                ids.add(Long.valueOf(i));
            }
        } catch (Exception exc) {
            Logger.getLogger(TagForm.class.getName()).log(Level.SEVERE, "UserIdsForm::mapData(Tag tag) ", exc);
        }
    }



}
