/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.forms.company;
import com.euler.imhotep.persistence.Tag;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.web.servlets.FrontController;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author abraham
 */
public class UserTagsForm extends Form{

    private static final String PARAM_ID = "userId";
    private static final String PARAM_TAGS= "selected_tags";

    /*1*/
    private Parameter userId;
    private Parameter selectedTags;

    /*2*/
    public UserTagsForm() {
        init();
    }

    public UserTagsForm(Tag entity) {
        init();


    /*3*/
    }

    public void init() {
        userId = new Parameter(PARAM_ID, Parameter.ptype.STRING);
        addParameter(userId);
        selectedTags = new Parameter(PARAM_TAGS, Parameter.ptype.INT);
        selectedTags.addCondition(new ConditionNotNull());
        addParameter(selectedTags);

    /*4*/
    }

    public String getId() {
        return userId.getStringValue();
    }

    public Integer[] getName() {
        return selectedTags.getIntegerValues();
    }

    /*5*/
    public void mapData(UserAccount user) {
        try {
            
            List<Tag> userTags =  new ArrayList<Tag>();            
            for (Integer i : selectedTags.getIntegerValues()) {
                Tag t = FrontController.tagCtl.findTag((long) i);
                if (!userTags.contains(t)) {
                    userTags.add(t);
                }
                if(!t.getUsers().contains(user)){
                   t.getUsers().add(user);
                   FrontController.tagCtl.edit(t);
                }
            }
            user.setTags(userTags);
        } catch (Exception exc) {
            Logger.getLogger(TagForm.class.getName()).log(Level.SEVERE, "TagForm::mapData(Tag tag) ", exc);
        }
    }


}
