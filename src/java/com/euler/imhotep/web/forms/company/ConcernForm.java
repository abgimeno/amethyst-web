/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.company;

import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.Offer;
import com.euler.info.Address;
import com.euler.info.Company;
import com.euler.security.Cryptography;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;
import com.euler.web.handler.conditions.ConditionEmail;
import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class ConcernForm extends Form {

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME1 = "lastname1";
    public static final String PARAM_COMPNAME = "company_name";
    public static final String PARAM_CIF = "company_cif";
    public static final String PARAM_PHONENUMBER = "company_phone";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_PROVINCE = "province";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_ZIP = "zip";
    public static final String PARAM_LEGALINFO = "legal_info";
    public static final String PARAM_LOCALIZATION = "localization";
    public static final String PARAM_CONTACT = "contact";
    public static final String PARAM_DESCRIPTION = "descripti5on";
    public static final String PARAM_FAXNUMBER = "company_fax";
    public static final String PARAM_TIMETABLE = "timetable";
    public static final String PARAM_OLDPASS="oldpass" ;
    public static final String PARAM_PASS1="pass1";
    public static final String PARAM_PASS2="pass2";

    private Parameter email;
    private Parameter firstname;
    private Parameter lastname1;
    private Parameter companyName;
    private Parameter companyPhone;
    private Parameter companyCif;
    private Parameter companyLegalInfo;
    private Parameter localization;
    private Parameter contact;
    private Parameter description;
    private Parameter faxNumber;
    private Parameter companyAddress;
    private Parameter city;
    private Parameter province;
    private Parameter country;
    private Parameter zip;
    private Parameter timetable;
    private Parameter oldpass;
    private Parameter password1;
    private Parameter password2;


    public ConcernForm(){
        init();
    }
    public ConcernForm(Concern company) {
        try {
            init();
            firstname.setDefaultValue(company.getFirstName());
            lastname1.setDefaultValue(company.getSurname1());
            timetable.setDefaultValue(company.getTimetable());
            email.setDefaultValue(company.getEmailAccount());
            if (company.getCompany() != null) {
                companyName.setDefaultValue(company.getCompany().getCompanyName());
                companyPhone.setDefaultValue(company.getCompany().getPhoneNumber());
                faxNumber.setDefaultValue(company.getCompany().getFaxNumber());
                companyCif.setDefaultValue(company.getCompany().getNif());
                if (company.getCompany().getAddress() != null) {
                    companyAddress.setDefaultValue(company.getCompany().getAddress().getAddress());
                    city.setDefaultValue(company.getCompany().getAddress().getTown());
                    province.setDefaultValue(company.getCompany().getAddress().getProvince());
                    zip.setDefaultValue(company.getCompany().getAddress().getZip());
                    country.setDefaultValue(company.getCompany().getAddress().getCountry());
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(RegisterCompanyForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getEmail() {
        return email.getStringValue();
    }

    public String getCompanyName() {
        return companyName.getStringValue();
    }

    public String getAddress() {
        return companyAddress.getStringValue();
    }

    public String getCompanyCif() {
        return companyCif.getStringValue();
    }

    public String getCity() {
        return city.getStringValue();
    }

    public String getContact() {
        return contact.getStringValue();
    }

    public String getCountry() {
        return country.getStringValue();
    }

    public String getDescription() {
        return description.getStringValue();
    }

    public String getCompanyFax() {
        return faxNumber.getStringValue();
    }

    public String getLegalInfo() {
        return companyLegalInfo.getStringValue();
    }

    public String getLocalization() {
        return localization.getStringValue();
    }

    public String getCompanyPhone() {
        return companyPhone.getStringValue();
    }

    public String getProvince() {
        return province.getStringValue();
    }

    public String getZip() {
        return zip.getStringValue();
    }

    public String getFirstName() {
        return firstname.getStringValue();
    }

    public String getTimetable() {
        return timetable.getStringValue();
    }

    public String getLastName1() {
        return this.lastname1.getStringValue();
    }

    public String  getOldpass() {
        return oldpass.getStringValue();
    }

    public String getPassword1() {
        return password1.getStringValue();
    }

    public String getPassword2() {
        return password2.getStringValue();
    }

    @Override
    public void validate(Vector<String> errorlist) {
        super.validate(errorlist);

        if (oldpass == null) {
            return;
        }
  
        if (getPassword1() != null && getPassword2() != null) {
            if (!getPassword1().equals(getPassword2())) {
                errorlist.add("password_notequals");
            }
        }

    }
    public void mapData(Concern concern) {

        Company company = concern.getCompany();
        if (password1 != null) {
            concern.setEmailAccount(getEmail());
            concern.setPassword(Cryptography.cryptoPassword(getEmail(), getPassword1()));
        }
        if (company == null) {
            company = new Company();
        }
        Address add = company.getAddress();
        if (add == null) {
            add = new Address();
        }
        add.setAddress(getAddress());
        add.setTown(getCity());
        add.setProvince(getProvince());
        add.setCountry(getCountry());
        add.setZip(getZip());
        company.setAddress(add);
        company.setContact(getContact());
        company.setDescription(getDescription());
        company.setFaxNumber(getCompanyFax());
        company.setPhoneNumber(getCompanyPhone());
        company.setLegalInfo(getLegalInfo());
        company.setLocalization(getLocalization());
        company.setNif(getCompanyCif());
        company.setCompanyName(getFirstName());
        concern.setCompany(company);
        concern.setTimetable(getTimetable());

    }

    private void init() {
        try {
            email = new Parameter(PARAM_EMAIL, Parameter.ptype.STRING);
            email.addCondition(new ConditionNotNull());
            email.addCondition(new ConditionLength(0, 254));
            email.addCondition(new ConditionEmail());
            addParameter(email);
            firstname = new Parameter(PARAM_FIRSTNAME, Parameter.ptype.STRING);
            firstname.addCondition(new ConditionNotNull());
            firstname.addCondition(new ConditionLength(0, 32));
            addParameter(firstname);
            lastname1 = new Parameter(PARAM_LASTNAME1, Parameter.ptype.STRING);
            lastname1.addCondition(new ConditionNotNull());
            lastname1.addCondition(new ConditionLength(0, 32));
            addParameter(lastname1);
            oldpass = new Parameter(PARAM_OLDPASS, Parameter.ptype.STRING);
            oldpass.addCondition(new ConditionNotNull());
            oldpass.addCondition(new ConditionLength(5, 20));
            addParameter(oldpass);
            password1 = new Parameter(PARAM_PASS1, Parameter.ptype.STRING);
            password1.addCondition(new ConditionNotNull());
            password1.addCondition(new ConditionLength(5, 20));
            addParameter(password1);
            password2 = new Parameter(PARAM_PASS2, Parameter.ptype.STRING);
            password2.addCondition(new ConditionNotNull());
            password2.addCondition(new ConditionLength(5, 20));
            addParameter(password2);
            companyName = new Parameter(PARAM_COMPNAME, Parameter.ptype.STRING);
            companyName.addCondition(new ConditionNotNull());
            companyName.addCondition(new ConditionLength(0, 64));
            addParameter(companyName);
            companyPhone = new Parameter(PARAM_PHONENUMBER, Parameter.ptype.STRING);
            companyPhone.addCondition(new ConditionNotNull());
            companyPhone.addCondition(new ConditionLength(0, 64));
            addParameter(companyPhone);
            companyCif = new Parameter(PARAM_CIF, Parameter.ptype.STRING);
            companyCif.addCondition(new ConditionNotNull());
            companyCif.addCondition(new ConditionLength(0, 64));
            addParameter(companyCif);
            faxNumber = new Parameter(PARAM_FAXNUMBER, Parameter.ptype.STRING);
            faxNumber.addCondition(new ConditionLength(0, 64));
            addParameter(faxNumber);
            companyAddress = new Parameter(PARAM_ADDRESS, Parameter.ptype.STRING);
            companyAddress.addCondition(new ConditionLength(0, 64));
            addParameter(companyAddress);
            city = new Parameter(PARAM_CITY, Parameter.ptype.STRING);
            city.addCondition(new ConditionLength(0, 64));
            addParameter(city);
            province = new Parameter(PARAM_PROVINCE, Parameter.ptype.STRING);
            province.addCondition(new ConditionLength(0, 64));
            addParameter(province);
            country = new Parameter(PARAM_COUNTRY, Parameter.ptype.STRING);
            country.addCondition(new ConditionLength(0, 64));
            addParameter(country);
            zip = new Parameter(PARAM_ZIP, Parameter.ptype.STRING);
            zip.addCondition(new ConditionLength(0, 64));
            addParameter(zip);
            companyLegalInfo = new Parameter(PARAM_LEGALINFO, Parameter.ptype.STRING);
            companyLegalInfo.addCondition(new ConditionLength(0, 64));
            addParameter(companyLegalInfo);
            localization = new Parameter(PARAM_LOCALIZATION, Parameter.ptype.STRING);
            localization.addCondition(new ConditionLength(0, 64));
            addParameter(localization);
            contact = new Parameter(PARAM_CONTACT, Parameter.ptype.STRING);
            contact.addCondition(new ConditionLength(0, 64));
            addParameter(contact);
            description = new Parameter(PARAM_DESCRIPTION, Parameter.ptype.STRING);
            description.addCondition(new ConditionLength(0, 64));
            addParameter(description);
            timetable = new Parameter(PARAM_TIMETABLE, Parameter.ptype.STRING);
            timetable.addCondition(new ConditionLength(0, 64));
            addParameter(timetable);
        } catch (RESyntaxException ex) {
            Logger.getLogger(ConcernForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
