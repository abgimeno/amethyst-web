/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.forms.company;
/*
 * DispatchForm.java
 *
 * Created on 7 de mayo de 2009, 16:23
 */

import com.euler.imhotep.persistence.company.Dispatch;
import com.euler.imhotep.web.servlets.FrontController;
import com.euler.web.handler.Form;
import com.euler.web.handler.Parameter;

import com.euler.web.handler.conditions.ConditionLength;
import com.euler.web.handler.conditions.ConditionNotNull;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 * @version
 */
public class DispatchForm extends Form {

    private static final String PARAM_ID = " id";
    private static final String PARAM_PAYED = " payed";
    
    public static final String PARAM_DISPATCHDAY = "dispatch_day";
    public static final String PARAM_DISPATCHMONTH = "dispatch_month";
    public static final String PARAM_DISPATCHYEAR = "dispatch_year";
    public static final String PARAM_PAYMENTDAY = "payment_day";
    public static final String PARAM_PAYMENTMONTH = "payment_month";
    public static final String PARAM_PAYMENTYEAR = "payment_year";
    public static final String PARAM_PAYEDIDS="payedIds";
    /*1*/
    private Parameter id;
    private Parameter payed;
    private Parameter dispatchDay;
    private Parameter dispatchMonth;
    private Parameter dispatchYear;
    private Parameter paymentDay;
    private Parameter paymentMonth;
    private Parameter paymentYear;
    private Parameter payedIds;


    /*2*/
    public DispatchForm() {
        init();
    }

    public DispatchForm(Dispatch entity) {
        init();

        id.setDefaultValue("" + entity.getId());
        payed.setDefaultValue("" + entity.isPayed());
        GregorianCalendar grc = new GregorianCalendar();
        GregorianCalendar grec = new GregorianCalendar();
        grec.setTime(entity.getDispatchDate());

        dispatchDay.setDefaultValue("" + grec.get(GregorianCalendar.DAY_OF_MONTH));
        dispatchMonth.setDefaultValue("" + grec.get(GregorianCalendar.MONTH));
        dispatchYear.setDefaultValue("" + grec.get(GregorianCalendar.YEAR));
        if(entity.getPaymentDate() != null && entity.isPayed()){
            grec.setTime(entity.getPaymentDate());
            paymentDay.setDefaultValue("" + grec.get(GregorianCalendar.DAY_OF_MONTH));
            paymentMonth.setDefaultValue("" + grec.get(GregorianCalendar.MONTH));
            paymentYear.setDefaultValue("" + grec.get(GregorianCalendar.YEAR));
        }
    /*3*/
    }

    public void init() {

        id = new Parameter(PARAM_ID, Parameter.ptype.STRING);
        payed = new Parameter(PARAM_PAYED, Parameter.ptype.BOOLEAN);

        payedIds = new Parameter(PARAM_PAYEDIDS, Parameter.ptype.INT);
        payedIds.addCondition(new ConditionNotNull());
        addParameter(payedIds);

        dispatchDay = new Parameter(PARAM_DISPATCHDAY, Parameter.ptype.INT);
        dispatchDay.addCondition(new ConditionLength(0, 32));
        addParameter(dispatchDay);

        dispatchMonth = new Parameter(PARAM_DISPATCHMONTH, Parameter.ptype.INT);
        dispatchMonth.addCondition(new ConditionLength(0, 32));
        addParameter(dispatchMonth);

        dispatchYear = new Parameter(PARAM_DISPATCHYEAR, Parameter.ptype.INT);
        dispatchYear.addCondition(new ConditionLength(0, 32));
        addParameter(dispatchYear);

        paymentDay = new Parameter(PARAM_PAYMENTDAY, Parameter.ptype.INT);
        paymentDay.addCondition(new ConditionLength(0, 32));
        addParameter(paymentDay);

        paymentMonth = new Parameter(PARAM_PAYMENTMONTH, Parameter.ptype.INT);
        paymentMonth.addCondition(new ConditionLength(0, 32));
        addParameter(paymentMonth);

        paymentYear = new Parameter(PARAM_PAYMENTYEAR, Parameter.ptype.INT);
        paymentYear.addCondition(new ConditionLength(0, 32));
        addParameter(paymentYear);
    /*4*/
    }

    public String getId() {
        return id.getStringValue();
    }

    public Boolean getPayed() {
        return payed.getBooleanValue();
    }

    public String getPaymentDay() {
        return paymentDay.getStringValue();
    }

    public String getPaymentMonth() {
        return paymentMonth.getStringValue();
    }

    public String getPaymentYear() {
        return paymentYear.getStringValue();
    }

    public int getDispatchDay() {
        return dispatchDay.getIntegerValue();
    }

    public int getDispatchMonth() {
        return dispatchMonth.getIntegerValue();
    }

    public int getDispatchYear() {
        return dispatchYear.getIntegerValue();
    }

    public Integer[] getPayedIds() {
        return payedIds.getIntegerValues();
    }
    /*5*/
    public void mapData(List<Long> idList) {
        try {
            for(Integer i : payedIds.getIntegerValues()){
                idList.add((long)i);
            }
        } catch (Exception exc) {
            Logger.getLogger(TagForm.class.getName()).log(Level.SEVERE, "DispatchForm::mapData(List<Long> ids) ", exc);
        }
    }
}