/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Experience;
import com.euler.imhotep.persistence.Knowledge;
import com.euler.imhotep.persistence.Study;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.web.forms.candidate.ExperienceForm;
import com.euler.imhotep.web.forms.candidate.KnowledgeForm;
import com.euler.imhotep.web.forms.candidate.PersonalDataForm;
import com.euler.imhotep.web.forms.candidate.StudyForm;
import com.euler.web.handler.HandlerException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public class CandidateHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "candidate";

    // Pages for this handler
    public static final String PAGE_CURICULUM = "/WEB-INF/docs/candidate/candidatecurriculum.jsp";
    public static final String PAGE_MENU = "/WEB-INF/docs/candidate/menucandidate.jsp";
    public static final String PAGE_SUBSCRIPTIONS = "/WEB-INF/docs/candidate/candidaturas.jsp";
    public static final String PAGE_LOGOUT = "index";
    public static final String PAGE_DISPLAY_CURRICULUM = "/WEB-INF/docs/candidate/display-curriculum.jsp";
    public static final String PAGE_STUDY="/WEB-INF/docs/candidate/candidatecurriculum.jsp?serv=candidate&action=curiculum2&user=candidate";
    public static final String PAGE_EXP="/WEB-INF/docs/candidate/candidatecurriculum.jsp?serv=candidate&action=curiculum3&user=candidate";
    public static final String PAGE_KNOW="/WEB-INF/docs/candidate/candidatecurriculum.jsp?serv=candidate&action=curiculum4&user=candidate";
    // Actions for this handler
    public static final String ACTION_SUBSCRIPTIONS = "subscriptions";
    public static final String ACTION_CURICULUM1 = "curiculum1";
    public static final String ACTION_CURICULUM2 = "curiculum2";
    public static final String ACTION_ADD_STUDY = "addStudy";
    public static final String ACTION_CURICULUM3 = "curiculum3";
    public static final String ACTION_ADD_EXPERIENCE = "addexperience";
    public static final String ACTION_CURICULUM4 = "curiculum4";
    public static final String ACTION_ADD_KNOWLEDGE = "addknowledge";
    public static final String ACTION_EDIT_EXPERIENCE = "editExperience";
    public static final String ACTION_EDIT_STUDY = "editStudy";
    public static final String ACTION_DELETE_EXP = "deleteExperience";
    public static final String ACTION_DELETE_STUDY = "deleteStudy";
    public static final String ACTION_EDIT_KNOWLEDGE = "editKnowledge";
    public static final String ACTION_DELETE_KNOWLEDGE = "deleteKnowledge";

    public static final String ACTION_MENU = "menu";
    public static final String ACTION_SHOW = "show";

    public static final String ADD = "add";
    public static final String EDIT = "edit";
    public static final String ATTRB_HASEXP="hasexperience";
    public static final String ATTRB_EXPERIENCES="experiences";
    public static final String ATTRB_HASSTUDY="hastudies";
    public static final String ATTRB_STUDIES="studies";
    public static final String ATTRB_KNOWLEDGES = "knowledges";
    public static final String ATTRB_HASKNOWLEDGE = "hasknowledge";
    public static final String ATTRB_FORM = "form";
    public static final String ATTRB_STEP = "step";
    public static final String ATTRB_ACT = "act";
    public static final String ATTRB_STUDYID="studyId";
    public static final String ATTRB_DGLEVEL="degree_level";

    public static final String PARAM_EXPERIENCEID = "experienceId";
    public static final String PARAM_KNOWLEDGEID = "knowledgeId";

    /** Form containg the personal data of a curriculum */
    protected PersonalDataForm personalDataForm;
    /** Form containg the study data of a curriculum */
    protected StudyForm studyForm;
    /** Form containg the experience data of a curriculum */
    protected ExperienceForm experienceForm;
    protected UserAccount user;
    /** Form containg the knowledge data of a curriculum */
    protected KnowledgeForm knowledgeForm;


    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {

        super.validateRequest(request, response);
        String step = request.getParameter(ATTRB_STEP);
        if (step == null){
            step="";
        }
        if(isUserInSession){
            user = (UserAccount) request.getSession().getAttribute(ATTRIBUTE_USER);
            user = FrontController.userCtl.findUserAccount(user.getId());
        }
        GregorianCalendar d = new GregorianCalendar();
        Integer year = d.get(GregorianCalendar.YEAR);
        request.setAttribute("currentyear", year);
        request.setAttribute("startyear", year - 100);

        if (action.equals(NOACTION)) {
            page = PAGE_MENU;
        }

        if (action.equals(ACTION_SUBSCRIPTIONS)) {
            request.setAttribute("offers", user.getStatus().iterator());
            request.setAttribute("hassoffers", !user.getOffers().isEmpty());
            page = PAGE_SUBSCRIPTIONS;
        } else if (action.equals(ACTION_SHOW)) {
            Long userId = Long.parseLong(request.getParameter("userId"));
            UserAccount userA = FrontController.userCtl.findUserAccount(userId);
            request.setAttribute("user_data", userA.getPersonalInfo());
            request.setAttribute(ATTRB_HASSTUDY, userA.getStudies().isEmpty());
            request.setAttribute(ATTRB_STUDIES, userA.getStudies().iterator());
            request.setAttribute(ATTRB_EXPERIENCES, userA.getExperiences().iterator());
            request.setAttribute(ATTRB_HASEXP, userA.getExperiences().isEmpty());
            request.setAttribute(ATTRB_HASKNOWLEDGE, userA.getKnowledges().isEmpty());
            request.setAttribute(ATTRB_KNOWLEDGES, userA.getKnowledges().iterator());

            page = PAGE_DISPLAY_CURRICULUM;
        } else if (action.equals(ACTION_CURICULUM1)) {
            personalDataForm = new PersonalDataForm(user);
            if (isPost) { //if is post validate data
                personalDataForm.map(request, response);
                personalDataForm.validate(errorlist);
            }
            request.setAttribute(ATTRB_FORM, personalDataForm);
            request.setAttribute(ATTRB_STEP, 1);
            page = PAGE_CURICULUM;
            
        } else if (action.equals(ACTION_CURICULUM2)|| step.equals("2")){
            validateStudy(request,response);

        }
         else if (action.equals(ACTION_CURICULUM3)|| step.equals("3")) {
             validateExperience(request,response);

        } else if (action.equals(ACTION_CURICULUM4)|| step.equals("4")) {
             validateKnowledge(request,response);
        }        
        return errorlist.isEmpty();

    }



    private void validateKnowledge(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(ATTRB_ACT, ADD);
        if (action.equals(ACTION_DELETE_KNOWLEDGE)) {
            validateDeleteKnowledge(request,response);
        }
        if (action.equals(ACTION_ADD_KNOWLEDGE)) {
            validateAddKnowledge(request,response);
        }
        if (action.equals(ACTION_EDIT_KNOWLEDGE)) {
            validateEditKnowledge(request,response);
        }
        request.setAttribute(ATTRB_HASKNOWLEDGE, user.getKnowledges().isEmpty());
        request.setAttribute(ATTRB_KNOWLEDGES, user.getKnowledges());
        request.setAttribute(ATTRB_STEP, 4);
        page = PAGE_CURICULUM;
    }
    private void validateAddKnowledge(HttpServletRequest request, HttpServletResponse response) {

        knowledgeForm = new KnowledgeForm();
        if (isPost) { //if is post validate data
            knowledgeForm.map(request, response);
            knowledgeForm.validate(errorlist);
        }
        request.setAttribute(ATTRB_FORM, knowledgeForm);
    }

    private void validateEditKnowledge(HttpServletRequest request, HttpServletResponse response) {
        Long id = Long.parseLong(request.getParameter(PARAM_KNOWLEDGEID));
        Knowledge knowledge = FrontController.knowledgeCtl.findKnowledge(id);
        knowledgeForm = new KnowledgeForm(knowledge);

        if (isPost) { //if is post validate data
            knowledgeForm.map(request, response);
            knowledgeForm.validate(errorlist);
        }
        request.setAttribute(ATTRB_FORM, knowledgeForm);
        request.setAttribute(ATTRB_ACT, EDIT);
        request.setAttribute(PARAM_KNOWLEDGEID, id);
        page = PAGE_CURICULUM;
    }

    private void validateDeleteKnowledge(HttpServletRequest request, HttpServletResponse response) {
        try {
            Long id = Long.parseLong(request.getParameter(PARAM_KNOWLEDGEID));
            Knowledge knowledge = FrontController.knowledgeCtl.findKnowledge(id);
            user.getKnowledges().remove(knowledge);
            FrontController.userCtl.edit(user);
            FrontController.knowledgeCtl.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void validateExperience(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(ATTRB_ACT, ADD);
        if (action.equals(ACTION_DELETE_EXP)){
            validateDeleteExperience(request,response);
        }
        if (action.equals(ACTION_ADD_EXPERIENCE)) {
            validateAddExperience(request,response);
        }
        if (action.equals(ACTION_EDIT_EXPERIENCE)) {
            validateEditExperience(request,response);
        }
        request.setAttribute(ATTRB_EXPERIENCES, user.getExperiences().iterator());
        request.setAttribute(ATTRB_HASEXP, user.getExperiences().isEmpty());
        request.setAttribute("areasProf", FrontController.areaCtl.findAreaEntities().iterator());
        request.setAttribute(ATTRB_STEP, 3);
        page = PAGE_CURICULUM;
    }

    private void validateAddExperience(HttpServletRequest request, HttpServletResponse response) {

        if (isPost) { //if is post validate data
            experienceForm = new ExperienceForm(user);
            experienceForm.map(request, response);
            experienceForm.validate(errorlist);
        }
        if (!errorlist.isEmpty()) {
            request.setAttribute(ATTRB_FORM, experienceForm);
        }

    }

    private void validateEditExperience(HttpServletRequest request, HttpServletResponse response) {

        request.setAttribute(ATTRB_ACT, EDIT);
        Long id = Long.parseLong(request.getParameter(PARAM_EXPERIENCEID));
        Experience exp = FrontController.experienceCtl.findExperience(id);
        experienceForm = new ExperienceForm(exp);

        if (isPost) { //if is post validate data
            experienceForm = new ExperienceForm(user);
            experienceForm.map(request, response);
            experienceForm.validate(errorlist);
        }
        request.setAttribute(PARAM_EXPERIENCEID, id);
        request.setAttribute(ATTRB_FORM, experienceForm);

    }

    
    private void validateDeleteExperience(HttpServletRequest request, HttpServletResponse response) {
        try {
            Long id = Long.parseLong(request.getParameter(PARAM_EXPERIENCEID));
            Experience exp = FrontController.experienceCtl.findExperience(id);
            user.getExperiences().remove(exp);
            FrontController.userCtl.edit(user);
            FrontController.experienceCtl.destroy(id);            
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void validateStudy(HttpServletRequest request, HttpServletResponse response){
        request.setAttribute(ATTRB_ACT, ADD);
        if (action.equals(ACTION_DELETE_STUDY)) {
            validateDeleteStudy(request, response);
        }
        if (action.equals(ACTION_ADD_STUDY)) {
            validateAddStudy(request,response);
        }
        if (action.equals(ACTION_EDIT_STUDY)) {
            validateEditStudy(request,response);
        }

        request.setAttribute(ATTRB_HASSTUDY, user.getStudies().isEmpty());
        request.setAttribute(ATTRB_STUDIES, user.getStudies().iterator());
        request.setAttribute(ATTRB_DGLEVEL, FrontController.degreeLevelCtl.findDegreeLevelEntities().iterator());
        request.setAttribute(ATTRB_STEP, 2);
        page = PAGE_CURICULUM;

    }
    private void validateEditStudy(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(ATTRB_ACT, EDIT);
        Long studyId = Long.parseLong(request.getParameter(ATTRB_STUDYID));
        //logger.info(""+studyId);
        Study study = FrontController.studyCtl.findStudy(studyId);
        studyForm = new StudyForm(study);

        if (isPost) { //if is post validate data
            studyForm.map(request, response);
            studyForm.validate(errorlist);
        }
        request.setAttribute(ATTRB_STUDYID,studyId);
        request.setAttribute(ATTRB_ACT, EDIT);
        request.setAttribute(ATTRB_FORM, studyForm);


    }

    private void validateAddStudy(HttpServletRequest request, HttpServletResponse response) {
        studyForm = new StudyForm();
        if (isPost) {
            studyForm.map(request, response);
            studyForm.validate(errorlist);
        }
        request.setAttribute(ATTRB_ACT, ADD);
        request.setAttribute(ATTRB_FORM, studyForm);

    }
    private void validateDeleteStudy(HttpServletRequest request, HttpServletResponse response) {
        try {
            Study s = FrontController.studyCtl.findStudy(Long.parseLong(request.getParameter(ATTRB_STUDYID)));
            user.getStudies().remove(s);
            FrontController.userCtl.edit(user);
            FrontController.studyCtl.destroy(Long.parseLong(request.getParameter(ATTRB_STUDYID)));
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {

        user = (UserAccount) request.getSession().getAttribute(ATTRIBUTE_USER);
        user = FrontController.userCtl.findUserAccount(user.getId());

        GregorianCalendar d = new GregorianCalendar();
        Integer year = d.get(GregorianCalendar.YEAR);
        request.setAttribute("currentyear", year);
        request.setAttribute("startyear", year - 50);
        Integer step = (request.getAttribute(ATTRB_STEP) == null?99:(Integer)request.getAttribute(ATTRB_STEP));
        if (action.equals(ACTION_CURICULUM1)) {
            try {
                personalDataForm.mapData(user);
                FrontController.userCtl.edit(user);
                request.setAttribute(ATTRB_STEP, 1);
                page = PAGE_CURICULUM;
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (action.equals(ACTION_CURICULUM2)|| step == 2) {
            handleStudy(request,response);

        } else if (action.equals(ACTION_CURICULUM3)|| step == 3) {
            handleExperience(request,response);
        }
         else if (action.equals(ACTION_CURICULUM4) || step == 4) {
            handleKnowledge(request, response);
        }



    }
    private void handleAddExperience(HttpServletRequest request, HttpServletResponse response) {
        try {
            experienceForm.mapData(user);
            FrontController.userCtl.edit(user);
            reloadUser(user, request);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void handleEditExperience(HttpServletRequest request, HttpServletResponse response) {
        try {
            Long experienceId = Long.parseLong(request.getParameter(PARAM_EXPERIENCEID));
            Experience experience = FrontController.experienceCtl.findExperience(experienceId);
            experienceForm.mapData(experience);
            FrontController.experienceCtl.edit(experience);
            FrontController.userCtl.edit(user);
            reloadUser(user, request);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void handleExperience(HttpServletRequest request, HttpServletResponse response){
        try {
            if (action.equals(ACTION_ADD_EXPERIENCE)) {
                handleAddExperience(request, response);
            }

            if (action.equals(ACTION_EDIT_EXPERIENCE)) {
                handleEditExperience(request, response);
            }
            request.setAttribute(ATTRB_EXPERIENCES, user.getExperiences().iterator());
            request.setAttribute(ATTRB_HASEXP, user.getExperiences().isEmpty());
            request.setAttribute(ATTRB_STEP, 3);
            page = PAGE_EXP;
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, "CandidateHandler::handleAddKnowledge(HttpServletRequest request, HttpServletResponse response)", ex);
        }
    }

    /**
     * 
     * @param request
     * @param response
     */
    private void handleKnowledge(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (action.equals(ACTION_ADD_KNOWLEDGE)) {
                handleAddKnowledge(request, response);
            }

            if (action.equals(ACTION_EDIT_KNOWLEDGE)) {
                handleEditKnowledge(request, response);
            }
            request.setAttribute(ATTRB_HASKNOWLEDGE, user.getKnowledges().isEmpty());
            request.setAttribute(ATTRB_KNOWLEDGES, user.getKnowledges());
            request.setAttribute(ATTRB_STEP, 4);
            page = PAGE_KNOW;
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, "CandidateHandler::handleAddKnowledge(HttpServletRequest request, HttpServletResponse response)", ex);
        }
    }
    /**
     * 
     * @param request
     * @param response
     */
    private void handleStudy(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (action.equals(ACTION_ADD_STUDY)) {
                handleAddStudy(request, response);
            }
            if (action.equals(ACTION_EDIT_STUDY)) {
                handleEditStudy(request, response);
            }
            //user.setCurriculumDate(new Date());
            //user = FrontController.userCtl.findUserAccount(user.getId());
            FrontController.userCtl.edit(user);
            reloadUser(user,request);
            request.setAttribute(ATTRB_HASSTUDY, user.getStudies().isEmpty());
            request.setAttribute(ATTRB_STUDIES, user.getStudies().iterator());
            request.setAttribute(ATTRB_DGLEVEL, FrontController.degreeLevelCtl.findDegreeLevelEntities().iterator());
            request.setAttribute(ATTRB_STEP, 2);
            page = PAGE_STUDY;
        }  catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    private void handleEditStudy(HttpServletRequest request, HttpServletResponse response) {
            try {
                Long studyId = Long.parseLong(request.getParameter(ATTRB_STUDYID));
                Study study = FrontController.studyCtl.findStudy(studyId);
                studyForm.mapData(study);
                FrontController.studyCtl.edit(study);                
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


    private void handleAddStudy(HttpServletRequest request, HttpServletResponse response) {
            try {
                studyForm.mapData(user);
            } catch (Exception ex) {
                Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    private void handleAddKnowledge(HttpServletRequest request, HttpServletResponse response) {
        try {

            //user = FrontController.userCtl.findUserAccount(user.getId());
            knowledgeForm.mapData(user);
            //user.setCurriculumDate(new Date());
            //FrontController.userCtl.edit(user);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, "CandidateHandler::handleAddKnowledge(HttpServletRequest request, HttpServletResponse response)", ex);
        }
    }

    private void handleEditKnowledge(HttpServletRequest request, HttpServletResponse response) {
        try {
            Long id = Long.parseLong(request.getParameter(PARAM_KNOWLEDGEID));
            Knowledge knowledge = FrontController.knowledgeCtl.findKnowledge(id);
            knowledgeForm.mapData(knowledge);
            FrontController.knowledgeCtl.edit(knowledge);
            reloadUser(user,request);
        } catch (Exception ex) {
            Logger.getLogger(CandidateHandler.class.getName()).log(Level.SEVERE, "CandidateHandler::handleEditKnowledge(HttpServletRequest request, HttpServletResponse response)", ex);
        }
    }
    private void reloadUser(UserAccount user,HttpServletRequest request) {
       user = FrontController.userCtl.findUserAccount(user.getId());
       request.getSession().setAttribute(ATTRIBUTE_USER, user);
    }
    @Override
    public String getJSP() {
        return page;
    }


}
