/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.servlets;


import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.controllers.OfferJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public class SiteMapServlet extends HttpServlet {


    /**
    * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
    * @param request servlet request
    * @param response servlet response
    */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            List<Offer> offers = new OfferJpaController().findOfferEntities();
            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.println("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
            out.println("<url>");
            out.println("<loc>http://www.castellonempleo.es/docs/</loc>");
            out.println("<lastmod>2009-03-01</lastmod>");
            out.println("<changefreq>weekly</changefreq>");
            out.println("<priority>1</priority>");
            out.println("</url>");
            for(Offer o : offers){
                out.println("<url>");
                out.println("<loc>http://www.castellonempleo.es/docs/?serv=offer&amp;action=show&amp;offerId="+o.getId()+"</loc>");
                out.println("<lastmod>2009-03-01</lastmod>");
                out.println("<changefreq>weekly</changefreq>");
                out.println("<priority>0.9</priority>");
                out.println("</url>");
            }
            out.println(" </urlset>");

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
    * Handles the HTTP <code>GET</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
    * Handles the HTTP <code>POST</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
    * Returns a short description of the servlet.
    */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
