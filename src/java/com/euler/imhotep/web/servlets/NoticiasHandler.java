/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.info.Noticia;
import com.euler.web.handler.HandlerException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 */
public class NoticiasHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "newsdisplay";
    public static final String PARAM_NEWS = "noticias";
    public static final String ACTION_SHOW_NEW = "shownew";
    public static int NUMNEWS=10;

    @Override
    protected void initRequest(HttpServletRequest request) {
        try {
            super.initRequest(request);
            request.setAttribute(PARAM_NEWS, FrontController.noticiaCtl.findNoticiaEntities().subList(0, NUMNEWS));
        } catch (Exception ex) {
            Logger.getLogger(NoticiasHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        try {
            super.validateRequest(request, response);
            if (action.equals(NOACTION)) {
                page = PAGE_DISPLAYNEWS;
            }

            if (action.equals(ACTION_SHOW_NEW)) {
                long newId = Long.parseLong(request.getParameter("id"));
                Noticia noticia = FrontController.noticiaCtl.findNoticia(newId);
                request.setAttribute("entry", noticia);
                page = PAGE_SHOW_NEW;
            }
        } catch (Exception ex) {
            Logger.getLogger(NoticiasHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return true;
        }
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
    }

    @Override
    protected String getSiteBar() {
        return PAGE_ADS;
    }

    @Override
    public String getJSP() {
        return page;
    }
}
