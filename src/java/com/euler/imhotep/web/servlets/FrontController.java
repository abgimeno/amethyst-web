/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Area;
import com.euler.imhotep.persistence.Contract;
import com.euler.imhotep.persistence.Timetable;
import com.euler.imhotep.persistence.controllers.*;
import com.euler.web.handler.HandlerServlet;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */

public class FrontController extends HandlerServlet {

    private String ATTRIBUTE_AREAS = "areas";
    private String PAGE_INDEX = "/WEB-INF/docs/index.jsp";
    private String DEFAULT_HANDLER = "search";


    public static AreaJpaController areaCtl;
    public static ConcernJpaController concernCtl;
    public static DegreeLevelJpaController degreeLevelCtl;
    public static ExperienceJpaController experienceCtl;
    public static ExperienceTimeJpaController expTimeCtl;
    public static KnowledgeJpaController knowledgeCtl;
    public static KnowledgeLevelJpaController knowLevelCtl;
    public static OfferJpaController offerCtl;
    public static ProvinceJpaController provinceCtl;
    public static SearchJpaController searchCtl;
    public static StatusJpaController statusCtl;
    public static StudyJpaController studyCtl;
    public static TownJpaController townCtl;
    public static UserAccountJpaController userCtl;
    public static SubAreaJpaController subAreaCtl;
    public static ContractJpaController contractCtl;
    public static TimetableJpaController timetableCtl;
    public static OfferStatusJpaController offerStsCtl;
    public static TagJpaController tagCtl;
    public static DispatchJpaController dispathcCtl;
    public static NoticiaJpaController noticiaCtl;

    public FrontController(){
        super();
        initJpaControllers();
    }



    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if(request.getSession().getAttribute(ATTRIBUTE_AREAS) == null){
                List<Area> categories = areaCtl.findAreaEntities();
                request.getSession().setAttribute(ATTRIBUTE_AREAS, categories);
             }
            
            super.processRequest(request, response);
        }catch (Exception exc){
            Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, "FrontController:processRequest()", exc);
        }
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void initHandler() {
        addHandler(UserHandler.HANDLERNAME, UserHandler.class);
        addHandler(CompanyHandler.HANDLERNAME, CompanyHandler.class);
        addHandler(LoginHandler.HANDLERNAME, LoginHandler.class);
        addHandler(RegisterHandler.HANDLERNAME, RegisterHandler.class);
        addHandler(CandidateHandler.HANDLERNAME, CandidateHandler.class);
        addHandler(SearchHandler.HANDLERNAME, SearchHandler.class);
        addHandler(OfferHandler.HANDLERNAME, OfferHandler.class);
        addHandler(AdminHandler.HANDLERNAME,AdminHandler.class);
        addHandler(NoticiasAdminHandler.HANDLERNAME,NoticiasAdminHandler.class);
        addHandler(NoticiasHandler.HANDLERNAME,NoticiasHandler.class);
        defaultHandler = DEFAULT_HANDLER;
    }

    @Override
    protected void initJpaControllers() {
        areaCtl = new AreaJpaController();
        concernCtl = new ConcernJpaController();
        degreeLevelCtl = new DegreeLevelJpaController();
        experienceCtl = new ExperienceJpaController();
        expTimeCtl = new ExperienceTimeJpaController();
        knowledgeCtl = new KnowledgeJpaController();
        knowLevelCtl = new KnowledgeLevelJpaController();
        offerCtl = new OfferJpaController();
        provinceCtl = new ProvinceJpaController();
        searchCtl = new SearchJpaController();
        statusCtl = new StatusJpaController();
        studyCtl = new StudyJpaController();
        townCtl = new TownJpaController();
        userCtl = new UserAccountJpaController();
        subAreaCtl = new SubAreaJpaController();
        contractCtl = new ContractJpaController();
        timetableCtl = new TimetableJpaController();
        offerStsCtl = new OfferStatusJpaController();
        tagCtl= new TagJpaController();
        dispathcCtl = new DispatchJpaController();
        noticiaCtl = new NoticiaJpaController();
    }
}
