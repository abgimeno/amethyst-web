package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.ConcernJpaController;
import com.euler.imhotep.persistence.controllers.UserAccountJpaController;
import com.euler.imhotep.report.iCurriculum;
import com.euler.imhotep.web.forms.LoginForm;
import com.euler.web.handler.HandlerException;
import com.euler.web.handler.RedirectException;
import com.euler.web.handler.SessionInfo;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *
 * @author abraham
 */
public class LoginHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "login";

    public static final String USERTYPE_CANDIDATE = "candidate";
    public static final String USERTYPE_COMPANY = "company";

    public static final String PAGE_LOGINCANDIDATE = "/WEB-INF/docs/candidate/logincandidate.jsp";
    public static final String PAGE_LOGINCOMPANY = "/WEB-INF/docs/company/logincompany.jsp";
    public static final String PAGE_USERHOME = "/WEB-INF/docs/candidate/candidatecurriculum.jsp";
    public static final String PAGE_COMPANYHOME = "/WEB-INF/docs/candidate/companyaccount.jsp";
    public static final String PAGE_MENU_CANDIDATE = "/WEB-INF/docs/candidate/menucandidate.jsp";
    public static final String PAGE_MENU_COMPANY = "/WEB-INF/docs/company/menucompany.jsp";
    public static final String PAGE_DISABLED = "login";
    public static final String PAGE_LOGOUT = "index.jsp";
    public static final String PAGE_RESULTS ="/WEB-INF/docs/search/results.jsp?serv=offer";

    // Actions for this handler
    public static final String ACTION_LOGIN = "login";
    public static final String ACTION_LOGOUT = "logout";
    public static final String ACTION_USER = "user";
    public static final String ACTION_COMPANY = "company";
    public static final String ACTION_MYACCOUNT="myaccount";
    protected boolean isAdmin;
    /** Form handling the login-data */
    protected LoginForm loginForm;
    protected UserAccountJpaController userCtl;
    protected ConcernJpaController concernCtl;
    
    private String userType="";

    private final int adminId=3;

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            super.validateRequest(request, response);
            userType = request.getParameter(PARAM_USERTYPE);

            if(request.getParameter("reg") != null && request.getParameter("reg").equals("true")){
                errorlist.add("register_completed");
            }
            if (action.equals(NOACTION) && userType == null) {
                List<Offer> offers = FrontController.offerCtl.findOfferEntities();
                request.setAttribute("offers", offers.iterator());
                page = PAGE_RESULTS;
            } else {
                if (action.equals(ACTION_LOGIN)) {
                    loginForm = new LoginForm();
                    loginForm.map(request, response);
                    if (userType.equals(USERTYPE_COMPANY)) {
                        description = "CastellonEmpleo pone a disposición de las empresas una plataforma donde publicar sus ofertas de empleo.";
                        page = PAGE_LOGINCOMPANY;
                    }
                    if (userType.equals(USERTYPE_CANDIDATE)) {
                        description = "Idéntificate en nuestro sistema y beneficiáte de el servicio de curriculum que te ofrece CastellonEmpleo.";
                        page = PAGE_LOGINCANDIDATE;
                    }
                    if (isPost) {
                        loginForm.validate(errorlist);
                    }
                    request.setAttribute("loginForm", loginForm);
                } else if (action.equals(ACTION_LOGOUT)) {
                    List<Offer> offers = FrontController.offerCtl.findOfferEntities();
                    request.setAttribute("offers", offers.iterator());
                    request.getSession().setAttribute(ATTRIBUTE_USER,null);
                    isUserInSession = false;
                    request.setAttribute("main_page", PAGE_MAINLOGOUT);
                    if (userType.equals(USERTYPE_CANDIDATE)) {
                        Logger.getLogger(LoginHandler.class.getName()).info("user logout");
                    }
                    if (userType.equals(USERTYPE_COMPANY)) {
                        Logger.getLogger(LoginHandler.class.getName()).info("Company logout");
                    }
                    page = PAGE_RESULTS;
                } else if (action.equals(ACTION_MYACCOUNT)) {
                    if (userType.equals(USERTYPE_CANDIDATE)) {
                        page = PAGE_MENU_CANDIDATE;
                    }
                    if (userType.equals(USERTYPE_COMPANY)) {
                        page = PAGE_MENU_COMPANY;
                    }
                }
            }
            return errorlist.size() == 0;
        } catch (HandlerException ex) {
            Logger.getLogger(LoginHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
      
            if (!action.equals(ACTION_LOGIN)) {
                return;
            }
            if (userType.equals(USERTYPE_CANDIDATE) && isPost) {
                loginUser(request,response);
            }
            if (userType.equals(USERTYPE_COMPANY) && isPost) {
                loginCompany(request,response);
            }

        } catch (NullPointerException ex) {
            Logger.getLogger(LoginHandler.class.getName()).log(Level.SEVERE, "LoginHandler()::handleRequest", ex);
        }
    }

    @Override
    public String getJSP() {
        return page;
    }

    private void addSessionListenerData(HttpServletRequest request, HttpServletResponse response, UserAccount userAccount) {

        ServletContext ctx = request.getSession().getServletContext();
        // find the session info for the actual session
        List<SessionInfo> list = (List<SessionInfo>) ctx.getAttribute("sessionList");
        for (SessionInfo info : list) {
            if (info.getSessionId().equals(request.getSession().getId())) {
                info.setEmail(userAccount.getEmailAccount());
            }
        }
        ctx.setAttribute("sessionList", list);
    }

    private void loginCompany(HttpServletRequest request, HttpServletResponse response) {
        try {
            
            Concern concern = FrontController.concernCtl.login(loginForm.getLogin(), loginForm.getPassword());

            // check if the user is enabled
            if (true) {
                request.getSession().setAttribute(ATTRIBUTE_USER, concern);
                Logger.getLogger(LoginHandler.class.getName()).info("account success, showing index.jsp");
                request.getSession().setAttribute("userType", "company");
                request.setAttribute("main_page", PAGE_MAINLOGIN);
                if (concern.getId() == adminId) {
                    isAdmin = Boolean.TRUE;
                }
                //It's false by default
                request.getSession().setAttribute("isAdmin", isAdmin);
                page = PAGE_MENU_COMPANY;

            } else {
                Logger.getLogger(LoginHandler.class.getName()).info("account disabled, showing disabled.jsp");
                Long userId = concern.getId();
                request.setAttribute("uid", userId);
                page = PAGE_DISABLED;

            }
        } catch (NoResultException noResExc) {
            errorlist.add("login_failed");
            page = PAGE_LOGINCOMPANY;
        }
    }

    private void loginUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            UserAccount userAccount = FrontController.userCtl.login(loginForm.getLogin(), loginForm.getPassword());
            // check if the user is enabled
            if (userAccount.isEnabled()) {
                request.getSession().setAttribute(ATTRIBUTE_USER, userAccount);
                new iCurriculum().createResume(userAccount);
                //Adding the information for the SessionListener
                //addSessionListenerData(request, response, userAccount);
                Logger.getLogger(LoginHandler.class.getName()).info("account success, showing index.jsp");
                //check if there are articles in the cart for redirect to the shipping page
                request.getSession().setAttribute("userType", "candidate");
                request.setAttribute("main_page", PAGE_MAINLOGIN);
                page = PAGE_MENU_CANDIDATE;

            } else {
                errorlist.add("enable_account");
                Logger.getLogger(LoginHandler.class.getName()).info("account disabled, showing disabled.jsp");
                Long userId = userAccount.getId();
                request.setAttribute("uid", userId);
                page = PAGE_LOGINCANDIDATE;
            }

        } catch (NoResultException ex) {
            errorlist.add("login_failed");
            page = PAGE_LOGINCANDIDATE;
        }
    }

    @Override
    protected String getDescription() {
        return description;
    }

    
}
