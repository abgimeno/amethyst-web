/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.configuration.Configuration;
import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.UserHash;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.web.forms.candidate.RegisterUserForm;
import com.euler.imhotep.web.forms.company.RegisterCompanyForm;
import com.euler.info.EmailMessageInfo;
import com.euler.io.SendMail;
import com.euler.web.handler.HandlerException;
import com.euler.web.handler.RedirectException;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public class RegisterHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "register";
    
    public static final String USERTYPE_CANDIDATE = "candidate";
    public static final String USERTYPE_COMPANY = "company";
    
    
    public static final String ACTION_REGISTER = "do_register";
    public static final String ACTION_LOAD = "load";
    public static final String ACTION_ENABLE = "enable";

    public static final String PARAM_HASH = "hash";
    
    public static final String PAGE_INDEX = "index";
    public static final String PAGE_REGISTERCANDIDATE = "/WEB-INF/docs/candidate/registercandidate.jsp";
    public static final String PAGE_REGISTERCOMPANY = "/WEB-INF/docs/company/registercompany.jsp";
    public static final String PAGE_ENABLEDACC = "/WEB-INF/docs/showmessage.jsp";
    public static final String PAGE_LOGINCANDIDATE = "/docs/?serv=login&action=login&user=candidate&reg=true";
    public static final String PAGE_ERROR = "error";

    
    private RegisterUserForm createAForm;
    private RegisterCompanyForm createCompForm;
    private String userType;

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        GregorianCalendar d = new GregorianCalendar();
        Integer year = d.get(GregorianCalendar.YEAR);
        request.setAttribute("currentyear", year);
        request.setAttribute("startyear", year - 50);
        try {
            super.validateRequest(request, response);            
            userType = request.getParameter(PARAM_USERTYPE);
            //enableAccountPetition(FrontController.userCtl.findUserAccount((long)10));
            if (action.equals(NOACTION) || userType == null) {
                page = PAGE_INDEX;
                return true;
            }
            if (action.equals(ACTION_REGISTER)) {

                if (userType.equals(USERTYPE_CANDIDATE)) {
                    createAForm = new RegisterUserForm();
                    createAForm.map(request, response);
                    if (isPost) {
                        createAForm.validate(errorlist);
                    }
                    page=PAGE_REGISTERCANDIDATE;
                }

                if (userType.equals(USERTYPE_COMPANY)) {
                    createCompForm = new RegisterCompanyForm();
                    createCompForm.map(request, response);
                    if (isPost) {
                        createCompForm.validate(errorlist);
                    }
                    page=PAGE_REGISTERCOMPANY;
                }

            }
            if(action.equals(ACTION_ENABLE)){
                if (userType.equals(USERTYPE_CANDIDATE)) {
                    UserAccount u = FrontController.userCtl.returnHashedUser(request.getParameter(PARAM_HASH));
                    if (u == null) {
                        errorlist.add("hash_notfound");
                        return errorlist.size() == 0;
                    }
                    u.setEnabled(true);
                    FrontController.userCtl.edit(u);
                    FrontController.userCtl.removeOlderHash(u);
                    request.setAttribute("message","activated");
                    page = PAGE_ENABLEDACC;
                }
                if (userType.equals(USERTYPE_COMPANY)) {

                }
            }
            return errorlist.size() == 0;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RegisterHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(RegisterHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        try {
            if (action.equals(ACTION_REGISTER)) {
                if (isPost && userType.equals(USERTYPE_CANDIDATE)) {
                   
                    UserAccount user = new UserAccount();
                    createAForm.mapData(user);
                    if (!FrontController.userCtl.existsUser(user.getEmailAccount())) {
                        user.setEnabled(false);
                        FrontController.userCtl.create(user);
                        enableAccountPetition(user);
                        throw new RedirectException(PAGE_LOGINCANDIDATE);
                    } else {
                        page = PAGE_LOGINCAND;
                        String errorString = "El usuario ya existe en nuestra base de datos.";
                        errorlist.add(errorString);
                    }
                }
                if(isPost && userType.equals(USERTYPE_COMPANY)){

                    Concern company = new Concern();
                    createCompForm.mapData(company);
                    if(!FrontController.concernCtl.existsCompany(company.getEmailAccount())){
                        FrontController.concernCtl.create(company);
                        throw new RedirectException(PAGE_LOGINCOMP);
                    }else{
                        page = PAGE_LOGINCOMP;
                        String errorString = "La compañia ya existe en nuestra base de datos.";
                        errorlist.add(errorString);
                    }
                }
            }
        } catch (NullPointerException nullExc) {
            Logger.getLogger(RegisterHandler.class.getName()).log(Level.SEVERE, "RegisterHandler()::handleRequest", nullExc);
        }
    }

        /**
     * The object setted in emailMessage should be
     * an UserAccount
     * @param emailMessage
     *
     */
    public void enableAccountPetition(UserAccount user) {
        try {
            FrontController.userCtl.removeOlderHash(user);
            EmailMessageInfo emailMessage = new EmailMessageInfo();
            UserHash userHash = FrontController.userCtl.createHash(user);
            emailMessage.setFrom("castellonempleo.es <administracion@castellonempleo.es>");
            emailMessage.setTo(user.getEmailAccount());
            emailMessage.setBody("Su cuenta en castellonempleo.es se ha creado correctamente. Para activar su cuenta haga clic en enlace.<br>" +
                    "<a href = \"http://" + Configuration.getProperty("mail.source")/*"localhost:8080"*/
                    + "/docs/?serv=register&action=enable&"+PARAM_USERTYPE+"="+USERTYPE_CANDIDATE+"&hash=" + userHash.getHashUrlEncoded()+"\"> LINK DE ACTIVACIÓN </a>");
            emailMessage.setSubject("Correo de activación para su cuenta en castellonempleo.es.");
            SendMail.sendMail(emailMessage, Configuration.getInstance().getProperties());
        } catch (Exception exc) {
            Logger.getLogger(RegisterHandler.class.getName()).log(Level.SEVERE, "RegisterHandler()::enableAccountPetition", exc);
        }

    }

    @Override
    public String getJSP() {
        return page;
    }
}
