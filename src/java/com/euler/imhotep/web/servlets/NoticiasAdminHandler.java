/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.persistence.info.Noticia;
import com.euler.imhotep.web.forms.NoticiaForm;
import com.euler.imhotep.web.forms.candidate.DateForm;
import com.euler.web.handler.HandlerException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 */

public class NoticiasAdminHandler extends ImhotepHandler{

    public static final String HANDLERNAME="news";

    public static final String ACTION_GET="getnew";
    public static final String ACTION_ADD="addnew";
    public static final String ACTION_EDIT="editnew";
    public static final String ACTION_DELETE="deletenew";
    public static final String ACTION_LIST="listnew";    

    public static final String PARAM_NEWS="noticias";
    public static final String PARAM_DATE="date";

    protected NoticiaForm entry;
    protected DateForm dateForm;

    @Override
    protected void initRequest(HttpServletRequest request) {
        try {
            super.initRequest(request);
            request.setAttribute(PARAM_NEWS, FrontController.noticiaCtl.findNoticiaEntities().iterator());
        } catch (Exception ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
         Noticia noticia; // Podemos ponerlo como variable global de la clase y pasar un boolean a la función mapAndValidate
        try {
            super.validateRequest(request, response);
            Boolean admin = (Boolean) request.getSession().getAttribute(PARAM_ISADMIN);
            if (!admin) {
                return false;
            }
            if (action.equals(ACTION_ADD)) {
                page = PAGE_NEW;
                return mapAndValidate(null, request, response);
            }
            if (action.equals(ACTION_EDIT)) {
                page = PAGE_NEW;
                noticia= FrontController.noticiaCtl.findNoticia(Long.parseLong(request.getParameter(PARAM_ID)));
                return mapAndValidate(noticia, request, response);
            }
            if (action.equals(ACTION_DELETE)) {
                noticia = FrontController.noticiaCtl.findNoticia(Long.parseLong(request.getParameter(PARAM_ID)));
                noticia.setEnabled(false);
                FrontController.noticiaCtl.edit(noticia);
                return true;
            }
            if (action.equals(ACTION_GET)) {
                return validateGetNoticia(request, response);
            }
            page = PAGE_NEWS;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
             return errorlist.isEmpty();
        }
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        Noticia noticia;
        Date date;
        try {
            
            if (action.equals(ACTION_ADD)) {
                noticia = new Noticia();
                date = dateForm.mapData();
                entry.mapData(noticia);
                noticia.setDate(date);
                SimpleDateFormat df =  new SimpleDateFormat("dd-MM-yy",Locale.getDefault());
                FrontController.noticiaCtl.create(noticia);
            }
            if (action.equals(ACTION_EDIT)) {
                noticia = FrontController.noticiaCtl.findNoticia(Long.parseLong(entry.getId()));
                date = dateForm.mapData();
                entry.mapData(noticia);
                noticia.setDate(date);
                FrontController.noticiaCtl.edit(noticia);
            }
            request.setAttribute(PARAM_NEWS, FrontController.noticiaCtl.findNoticiaEntities().iterator());   //give again the noticias in the request before change pa
            page = PAGE_NEWS;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean validateGetNoticia(HttpServletRequest request, HttpServletResponse response) {
        try {
            Noticia n = null;
            n = FrontController.noticiaCtl.findNoticia(Long.parseLong(request.getParameter("id")));
            entry = new NoticiaForm(n);
            request.setAttribute(PARAM_ENTRY, entry);

            return errorlist.isEmpty();
        } catch (Exception ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean mapAndValidate(Noticia noticia, HttpServletRequest request, HttpServletResponse response) {
        try {
            dateForm = new DateForm();
            if (noticia == null) {
                entry = new NoticiaForm();
                entry.map(request, response);
                dateForm.map(request, response);
            } else {
                entry = new NoticiaForm(noticia);
            }
            if (isPost) {
                entry.map(request, response);
                entry.validate(errorlist);
                dateForm.map(request, response);
                dateForm.validate(errorlist);
            }
            request.setAttribute(PARAM_ENTRY, entry);
            request.setAttribute(PARAM_DATE, dateForm);
        } catch (Exception ex) {
            Logger.getLogger(NoticiasAdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return errorlist.isEmpty();
        }
    }

    @Override
    public String getJSP() {
        return page;
    }


}
