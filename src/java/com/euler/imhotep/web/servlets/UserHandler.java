/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.servlets;

import com.euler.imhotep.web.forms.candidate.ExperienceForm;
import com.euler.imhotep.web.forms.candidate.PersonalDataForm;
import com.euler.imhotep.web.forms.candidate.StudyForm;
import com.euler.web.handler.HandlerException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 * 
 */
public class UserHandler extends ImhotepHandler{
    
    public static final String HANDLERNAME = "user";
    public static final String PARAM_SECTION = "section";
    
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_ADD = "add";

    public static final String SECTION_EXP = "experience";
    public static final String SECTION_STUDIES = "studies";
    public static final String SECTION_PERSONAL = "personal";

    public static final String PAGE_ERROR = "error.jsp";

    private String section;
    private ExperienceForm expForm;
    private PersonalDataForm personalInfoForm;
    private StudyForm studyForm;

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {

        super.validateRequest(request, response);
        section = request.getParameter(PARAM_SECTION);
        //No section shows error page
        if(section == null){
            page=PAGE_ERROR;
            return false;
        }


        if(action.equals(ACTION_UPDATE) || action.equals(ACTION_ADD)){
            if(section.equals(SECTION_PERSONAL)){
                personalInfoForm.map(request, response);
                if(isPost){
                    personalInfoForm.validate(errorlist);
                }
            }
            if(section.equals(SECTION_EXP)){
                expForm.map(request, response);
                if(isPost){
                    studyForm.validate(errorlist);
                }
            }
            if(section.equals(SECTION_STUDIES)){
                studyForm.map(request, response);
                if(isPost){
                    studyForm.validate(errorlist);
                }
            }
        }
        
        return errorlist.size() == 0;

    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
