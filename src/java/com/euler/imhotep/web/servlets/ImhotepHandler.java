/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.*;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.web.forms.OfferForm;
import com.euler.web.handler.Handler;
import com.euler.web.handler.HandlerException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public class ImhotepHandler extends Handler {

    //TODO: put here all the pages, not on every handler
    public static final String PAGE_LOGINCAND = "/docs/?serv=login&action=login&user=candidate";
    public static final String PAGE_LOGINCOMP= "/docs/?serv=login&action=login&user=company";
    public static final String PAGE_DISPLAYUSER="/docs/?serv=admin&action=show&userId=";
    public static final String PAGE_CURRICULUM="/WEB-INF/docs/candidate/display-curriculum.jsp";
    public static final String PAGE_OFFERS2CAN = "/WEB-INF/docs/admin/offers2candidates.jsp";
    public static final String PAGE_CANDIDATES= "/WEB-INF/docs/admin/candidatelist.jsp";
    public static final String PAGE_OFFERS="/WEB-INF/docs/company/companyoffers.jsp";
    public static final String PAGE_CREATETAG="/WEB-INF/docs/admin/createtag.jsp";
    public static final String PAGE_DISPATCHES="/WEB-INF/docs/admin/dispatches.jsp";
    public static final String PAGE_DISPATCH="/WEB-INF/docs/admin/display-dispatch.jsp";
    public static final String PAGE_SENDED="/WEB-INF/docs/admin/email_sended.jsp";
    public static final String PAGE_COMPANYDATA="/WEB-INF/docs/company/display-companydata.jsp" ;
    public static final String PAGE_COMPANYLIST="/WEB-INF/docs/admin/companylist.jsp" ;
    public static final String PAGE_DISPLAYNEWS="/WEB-INF/docs/news/displaynews.jsp";
    public static final String PAGE_SHOW_NEW= "/WEB-INF/docs/news/shownew.jsp";
    public static final String PAGE_NEWS="/WEB-INF/docs/admin/listnoticias.jsp";
    public static final String PAGE_NEW="/WEB-INF/docs/admin/noticia.jsp";

    public static final String PAGE_INDEX = "/WEB-INF/docs/index.jsp";
    public static final String PAGE_CATS = "/WEB-INF/docs/categories.jsp";
    public static final String PAGE_ADS= "/WEB-INF/docs/adsbar.jsp";

    public static final String PAGE_MAINLOGIN = "/WEB-INF/docs/index.jsp";
    public static final String PAGE_MAINLOGOUT = "/WEB-INF/docs/main.jsp";

    public static final String PARAM_USERTYPE = "user";
    public static final String PARAM_ID="id";
    public static final String PARAM_ISADMIN="isAdmin";
    public static final String PARAM_ENTRY="entry";

    public static final String NOACTION="noaction";

    protected static final String VALUE_CANDIDATE = "candidate";
    protected static final String VALUE_COMPANY = "company";

    protected static String DESCRIPTION="Castellon Empleo - Tu página de empleo en Castellón. Busca y encuentra trabajo en Castellón. " +
                                            "En Castellón Empleo centramos nuestra bolsa de trabajo en la provincia de Castellón";
    
    protected static String TITLE="| Tu pagina de empleo en Castellon";

    public static final Integer NRESULTS = 18;

    protected void initRequest(HttpServletRequest request) {
        if (action == null) {
            action = NOACTION;
        }
        request.setAttribute("action", action);
        request.setAttribute("sitebar",getSiteBar());
        if (request.getSession().getAttribute(ATTRIBUTE_USER) == null) {
            isUserInSession = false;
            request.setAttribute("main_page", PAGE_MAINLOGOUT);
        } else {
            isUserInSession = true;
            request.setAttribute("main_page", PAGE_MAINLOGIN);
        }
    }

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        initRequest(request);
        return true;
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
    }
    

    public Set<Area> getAreas(List<SubArea> subareas) {
        Set<Area> areas = new HashSet<Area>();
        for (SubArea sa : subareas) {
            areas.add(sa.getParent());
        }
        return areas;
    }

    /**
     * 
     * @param admin
     * @param companyId
     * @param request
     * @param response
     * @return
     */
    protected boolean validateEditOffer(boolean admin, Long companyId, HttpServletRequest request, HttpServletResponse response) {
        Long id = Long.parseLong(request.getParameter(CompanyHandler.PARAM_OFFERID));
        Offer offer = FrontController.offerCtl.findOffer(id);
        if (!admin) {
            if (offer.getCompany().getId() != companyId) {
                errorlist.add("trying to edit others offer");
                return errorlist.size() == 0;
            }
        }
        request.setAttribute(CompanyHandler.ATTRIBUTE_CONTRACTS, FrontController.contractCtl.findContractEntities().iterator());
        request.setAttribute(CompanyHandler.ATTRIBUTE_TIMETABLES, FrontController.timetableCtl.findTimetableEntities().iterator());
        request.setAttribute(CompanyHandler.ATTRIBUTE_TOWNS, FrontController.townCtl.getCastellonsTowns().iterator());
        List areas = FrontController.areaCtl.findAreaEntities();
        areas.removeAll(getAreas(offer.getAreas()));
        request.setAttribute(CompanyHandler.ATTRIBUTE_SUBAREAS, areas.iterator());
        request.setAttribute(CompanyHandler.ATTRIBUTE_SELECTED_SUBAREAS, getAreas(offer.getAreas()).iterator());
        OfferForm offerForm = new OfferForm(offer);
        if (isPost) {
            offerForm.map(request, response);
            offerForm.validate(errorlist);
        }
        request.setAttribute(CompanyHandler.PARAM_OFFERID, id);
        offerForm.map(request, response);
        if (isPost) {
            offerForm.validate(errorlist);

        }
        request.setAttribute(CompanyHandler.ATTRB_FORM, offerForm);
        page = CompanyHandler.PAGE_INSERT;
        return errorlist.size() == 0;
    }

    /**
     * 
     * @param admin
     * @param companyId
     * @param request
     * @param response
     */
    protected void handleEditOffer(boolean admin, Long companyId, HttpServletRequest request, HttpServletResponse response) {
        try {
            Long id = Long.parseLong(request.getParameter(CompanyHandler.PARAM_OFFERID));
            Offer offer = FrontController.offerCtl.findOffer(id);
            if (!admin) {
                if (offer.getCompany().getId() != companyId) {
                    errorlist.add("trying to edit others offer");
                    return;
                }
            }
            OfferForm offerForm = new OfferForm();
            /* We map the data again because the OfferForm is created in this function
            the data should be validated in validateEditOffer, so if it's arrives to this poing
             *  it has been properly validated */
            offerForm.map(request, response);
            offerForm.mapData(offer);
            List areas = FrontController.areaCtl.findAreaEntities();
            areas.removeAll(getAreas(offer.getAreas()));
            FrontController.offerCtl.edit(offer);
            request.setAttribute(CompanyHandler.PARAM_OFFERID, id);
            request.setAttribute(CompanyHandler.ATTRB_FORM, offerForm);
            page = CompanyHandler.PAGE_INSERT;
            
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void validateDeleteOffer(boolean isAdmin, Long companyId, HttpServletRequest request, HttpServletResponse response) {
        try {
            Long id = Long.parseLong(request.getParameter(CompanyHandler.PARAM_OFFERID));
            Offer offer = FrontController.offerCtl.findOffer(id);
            if (!isAdmin) {
                if (offer.getCompany().getId() != companyId) {
                    errorlist.add("trying to delete others offer");
                    return;
                }
            }
            offer.setEnabled(false);
            FrontController.offerCtl.edit(offer);
        } catch (Exception ex) {
            Logger.getLogger(ImhotepHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected String getSiteBar() {
        return PAGE_CATS;
    }
    protected String getDescription() {
        return DESCRIPTION;
    }

    protected String getTitle() {
        return TITLE;
    }


}
