/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.imhotep.web.servlets;

import com.euler.configuration.Configuration;
import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.Tag;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.company.Dispatch;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.report.iCurriculum;
import com.euler.imhotep.web.forms.OfferForm;
import com.euler.imhotep.web.forms.company.DispatchForm;
import com.euler.imhotep.web.forms.company.TagForm;
import com.euler.imhotep.web.forms.company.UserIdsForm;
import com.euler.imhotep.web.forms.company.UserTagsForm;
import com.euler.info.EmailMessageInfo;
import com.euler.io.SendMail;
import com.euler.web.handler.HandlerException;
import com.euler.web.paging.PageInfo;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 */

public class AdminHandler extends ImhotepHandler{

    public static final String HANDLERNAME="admin";

    public static final String ACTION_SENDCV="sendcv";
    public static final String ACTION_INSCRIPTIONS="inscriptions";
    public static final String ACTION_SHOW="show";
    public static final String ACTION_COMPANY="showcompany";
    public static final String ACTION_COMPANIES="showcompanies";
    public static final String ACTION_CANDIDATES="showcurr";
    public static final String ACTION_GETOFFERS="showoffers";
    public static final String ACTION_CREATETAG="create_tag";
    public static final String ACTION_DELETETAG="delete_tag";
    public static final String ACTION_EDITTAG="edit_tag";    
    public static final String ACTION_TAGUSER="tag_user";
    public static final String ACTION_DISPATCHES="dispatches";
    public static final String ACTION_DELETEUSER="delete_user";
    public static final String ACTION_SHOWDISPATCH="showdispatch";
    

    public static final String PARAM_USERID="userId";
    public static final String PARAM_HASSUBS="hassubscriptions";
    public static final String PARAM_SUBSCRIPTION="subscriptions";
    public static final String PARAM_NPAGE="page";
    public static final String PARAM_DISPATCHES="dispatches";
    public static final String PARAM_COMPANYID="companyId";
    public static final String PARAM_DISPATCHID="dispatchId";

    public static final String ATTRB_COMPANY="company";
    public static final String ATTRB_CANDIDATES="candidates";
    
    protected OfferForm offerForm;
    protected TagForm tagForm;
    protected UserTagsForm userTagsForm;
    protected UserIdsForm userIdsForm;
    protected DispatchForm dispatchForm;

    private final int adminId=3;
    private int npage;

 
    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {

        super.validateRequest(request, response);
        Concern concern = (Concern) request.getSession().getAttribute(ATTRIBUTE_USER);
        Boolean admin = (Boolean) request.getSession().getAttribute("isAdmin");
        PageInfo pg = null;
        npage = (request.getParameter(PARAM_NPAGE) == null ? 0 : Integer.parseInt(request.getParameter(PARAM_NPAGE)));
        
        if(concern.getId() != adminId || !admin){
            return false;
        }
        if(action.equals(ACTION_INSCRIPTIONS)){
            request.setAttribute(PARAM_HASSUBS, 0);
            request.setAttribute(PARAM_SUBSCRIPTION,FrontController.offerStsCtl.findOfferStatusEntities().iterator());
            page =PAGE_OFFERS2CAN;
            }
        if(action.equals(ACTION_SHOW)){
            Long id = Long.parseLong(request.getParameter(PARAM_USERID));
            UserAccount user = FrontController.userCtl.findUserAccount(id);
            if(isPost){
                userTagsForm= new UserTagsForm();
                userTagsForm.map(request, response);
                userTagsForm.validate(errorlist);
            }
            setShowAttributes(request,response,user);
            page = PAGE_CURRICULUM;
        }
        if(action.equals(ACTION_CANDIDATES)|| (action.equals(ACTION_DELETEUSER))){
	    if(action.equals(ACTION_DELETEUSER)){
		disableUser(request,response);
		}

            setCandidatesAttributes(request);
            page=PAGE_CANDIDATES;
        }
        if(action.equals(ACTION_GETOFFERS)){
            request.setAttribute("hassoffers", true);
            request.setAttribute("offers", FrontController.offerCtl.findOfferEntities().iterator());
            page = PAGE_OFFERS;
        }

        if (action.equals(CompanyHandler.ACTION_EDIT)) {
            return validateEditOffer(admin,Long.valueOf(-1),request, response);
        }
        if(action.equals(CompanyHandler.ACTION_DELETE)){
            validateDeleteOffer(admin,Long.valueOf(-1),request,response);
            request.setAttribute("hassoffers", true);
            request.setAttribute("offers", FrontController.offerCtl.findOfferEntities().iterator());
            page = CompanyHandler.PAGE_OFFERS;
        }
        /*******NEW FUNTIONALITIES ******/
        if(action.equals(ACTION_CREATETAG) || action.equals(ACTION_EDITTAG)){
            
            request.setAttribute("tags", FrontController.tagCtl.findTagEntities().iterator());
            if(action.equals(ACTION_EDITTAG)){
                Long id = Long.parseLong(request.getParameter("tagId"));
                request.setAttribute("tag", FrontController.tagCtl.findTag(id));
            }
            tagForm = new TagForm();
            tagForm.map(request, response);
            if(isPost){
                tagForm.validate(errorlist);               
            }
             page = PAGE_CREATETAG;
        }
        
        // TODO - Remove - this, it is only for test purposes, here we have to validate and in handle to post**/
        if(action.equals(ACTION_SENDCV)){
            userIdsForm = new UserIdsForm();
            userIdsForm.map(request, response);

        }
        if(action.equals(ACTION_DISPATCHES)){
            request.setAttribute(PARAM_DISPATCHES, FrontController.dispathcCtl.findDispatchEntities().iterator());
            dispatchForm = new DispatchForm();
            dispatchForm.map(request, response);
            page =PAGE_DISPATCHES;
        }
        if (action.equals(ACTION_COMPANY)) {
            Concern company = FrontController.concernCtl.findConcern(Long.parseLong(request.getParameter(PARAM_COMPANYID)));
            request.setAttribute(ATTRB_COMPANY, company);
            page=PAGE_COMPANYDATA;
        }
        if (action.equals(ACTION_COMPANIES)) {
            request.setAttribute("companies", FrontController.concernCtl.findConcernEntities().iterator());
            page=PAGE_COMPANYLIST;
        }
        if(action.equals(ACTION_SHOWDISPATCH)){
            Dispatch dispatch = FrontController.dispathcCtl.findDispatch(Long.parseLong(request.getParameter(PARAM_DISPATCHID)));
            request.setAttribute(ATTRB_COMPANY, dispatch.getCompany());
            request.setAttribute(ATTRB_CANDIDATES, dispatch.getUsers().iterator());
            page=PAGE_DISPATCH;
        }
        return errorlist.isEmpty();
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        try {
            super.handleRequest(request, response);
            Concern concern = (Concern) request.getSession().getAttribute(ATTRIBUTE_USER);
            Boolean admin = (Boolean) request.getSession().getAttribute("isAdmin");
            if (concern.getId() != adminId || !admin) {
                return;
            }
            if (action.equals(CompanyHandler.ACTION_EDIT)) {
                handleEditOffer(admin, Long.valueOf(-1), request, response);
                page = CompanyHandler.PAGE_INSERT;
            }
            if (action.equals(ACTION_CREATETAG)||action.equals(ACTION_EDITTAG)||action.equals(ACTION_DELETETAG)) {
                handleTag(request,response);
            }
            /*******NEW FUNTIONALITIES ******/
            if (action.equals(ACTION_SHOW)||action.equals(ACTION_DELETEUSER)){
                handleUser(request,response);
            }
            if(action.equals(ACTION_DISPATCHES)){
                List<Long> ids = new ArrayList<Long>();
                dispatchForm.mapData(ids);
                for(Long id : ids){
                    Dispatch d = FrontController.dispathcCtl.findDispatch(id);
                    d.setPayed(true);
                    FrontController.dispathcCtl.edit(d);
                }
                request.setAttribute(PARAM_DISPATCHES, FrontController.dispathcCtl.findDispatchEntities().iterator());
            }
            if (action.equals(ACTION_SENDCV)) {
                List<Long> ids = new ArrayList<Long>();
                userIdsForm.mapData(ids);
                handleSendCV(ids,request);

            }

        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    public String getJSP() {
        return page;
    }


    // TODO - We have to redirect to a correct page after sending
    private void handleSendCV(List<Long> ids,HttpServletRequest request) {
        try {
          
            List<File> files = new ArrayList<File>();
            Concern company = FrontController.concernCtl.findConcern(Long.parseLong(request.getParameter(PARAM_COMPANYID)));
            List<UserAccount> users = new ArrayList<UserAccount>();
            for (Long id : ids) {
                UserAccount u = FrontController.userCtl.findUserAccount(id);
                iCurriculum cv = new iCurriculum();
                files.add(cv.createResume(u));
                users.add(u);
                sendNotification(u);
            }
            EmailMessageInfo emailMessage = new EmailMessageInfo();
            emailMessage.setFrom("castellonempleo.es <administracion@castellonempleo.es>");
            emailMessage.setTo(company.getEmailAccount());
            emailMessage.getBcc().add("administracion@imhotepservicios.com");
            emailMessage.getBcc().add("admin@castellonempleo.es");
            emailMessage.setBody("Le adjuntamos junto con el correo nuestra seleccion de curriculums");
            emailMessage.setSubject("Curriculums castellonempleo.es");
            SendMail.sendMail(emailMessage, Configuration.getInstance().getProperties(), files);

            Dispatch dispatch = new Dispatch(company,false,new Date());
            dispatch.setUsers(users);
            FrontController.dispathcCtl.create(dispatch);

            request.setAttribute("name", company.getCompany().getCompanyName());
            request.setAttribute("ncvs", ids.size());
            page=PAGE_SENDED;

        } catch (Exception exc) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, exc);
        }
    }

    private void handleTag(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (action.equals(ACTION_CREATETAG)) {
                Tag tag = new Tag();
                tagForm.mapData(tag);
                FrontController.tagCtl.create(tag);
            }
            if (action.equals(ACTION_EDITTAG)) {
                Long id = Long.parseLong(request.getParameter("tagId"));
                Tag tag = FrontController.tagCtl.findTag(id);
                tagForm.mapData(tag);
                FrontController.tagCtl.edit(tag);
                request.setAttribute("tag", FrontController.tagCtl.findTag(id));
            }
            if (action.equals(ACTION_DELETETAG)) {
                Long id = Long.parseLong(request.getParameter("tagId"));
                FrontController.tagCtl.destroy(id);

            }
            request.setAttribute("tags", FrontController.tagCtl.findTagEntities().iterator());
        } catch (Exception ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
	private void disableUser(HttpServletRequest request, HttpServletResponse response){
		try{
			Long id = Long.parseLong(request.getParameter(PARAM_USERID));
			Logger.getLogger(AdminHandler.class.getName()).info("Disabling user " +id);
               	 	UserAccount user = FrontController.userCtl.findUserAccount(id);
                	user.setEnabled(false);
                	FrontController.userCtl.edit(user);
		}catch(Exception exc){
			Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE,"AdminHandler::disableUser()",exc);
		}
	}
    private void handleUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (action.equals(ACTION_SHOW)) {
                Long id = Long.parseLong(request.getParameter(PARAM_USERID));
                UserAccount user = FrontController.userCtl.findUserAccount(id);
                userTagsForm.mapData(user);
                FrontController.userCtl.edit(user);
                user = FrontController.userCtl.findUserAccount(id);
                setShowAttributes(request, response, user);

            }
            if (action.equals(ACTION_DELETEUSER)) {
                Long id = Long.parseLong(request.getParameter(PARAM_USERID));
		Logger.getLogger(AdminHandler.class.getName()).info("Disabling user " +id);
                UserAccount user = FrontController.userCtl.findUserAccount(id);
                user.setEnabled(false);
                FrontController.userCtl.edit(user);
            }
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE,"AdminHandler::handleUser()",ex);
        } catch (Exception ex) {
            Logger.getLogger(AdminHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void sendNotification(UserAccount u) {
        // TODO - Notify premium users that their curriculum has been sent to a company
    }

    private void setCandidatesAttributes(HttpServletRequest request) {

        if (request.getParameter("tagId1") != null && Integer.parseInt(request.getParameter("tagId1")) != 0) {
            Long tagId1 = Long.parseLong(request.getParameter("tagId1"));
            if (request.getParameter("tagId2") != null && Integer.parseInt(request.getParameter("tagId2")) != 0) {
                Long tagId2 = Long.parseLong(request.getParameter("tagId2"));
                Integer conj = Integer.parseInt(request.getParameter("conjunction"));
                request.setAttribute("candidates", FrontController.userCtl.getByTags(conj, tagId1, tagId2).iterator());

            } else {
                request.setAttribute("candidates", FrontController.userCtl.getByTags(-1, tagId1, -1).iterator());
            }
        } else {
            request.setAttribute("candidates", FrontController.userCtl.findUserAccountEntities().iterator());
        }
        request.setAttribute("tags", FrontController.tagCtl.findTagEntities());
        request.setAttribute("companies", FrontController.concernCtl.findConcernEntities());
    }

    private void setShowAttributes(HttpServletRequest request, HttpServletResponse response,UserAccount user) {
        request.setAttribute("user_data", user);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy", Locale.getDefault());
        request.setAttribute("birthdate", df.format(user.getBirthdate()));
        request.setAttribute("hasknowledge", !user.getKnowledges().isEmpty());
        request.setAttribute("hasstudies", !user.getStudies().isEmpty());
        request.setAttribute("hasexperiences", !user.getExperiences().isEmpty());
        request.setAttribute("companies", FrontController.concernCtl.findConcernEntities());
        if(user.getTags().isEmpty()){
            request.setAttribute("tags", FrontController.tagCtl.findTagEntities().iterator());
            }
        else{
            request.setAttribute("selectedtags",user.getTags().iterator());
            List<Tag> selected = FrontController.tagCtl.findTagEntities();
            selected.removeAll(user.getTags());
            request.setAttribute("tags", selected.iterator());
        }
    }


}
