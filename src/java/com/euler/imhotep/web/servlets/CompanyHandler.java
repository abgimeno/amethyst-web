/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Concern;
import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.persistence.controllers.exceptions.NonexistentEntityException;
import com.euler.imhotep.web.forms.OfferForm;
import com.euler.imhotep.web.forms.company.ConcernForm;
import com.euler.web.handler.HandlerException;
import com.euler.web.handler.RedirectException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public class CompanyHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "company";
    public static final String ATTRB_FORM = "form";

    public static final String ATTRIBUTE_COMPANY = "userAccount";
    public static final String ATTRIBUTE_SUBAREAS = "subareas";
    public static final String ATTRIBUTE_SELECTED_SUBAREAS = "selectedsubareas";
    public static final String ATTRIBUTE_TOWNS = "towns";
    public static final String ATTRIBUTE_CONTRACTS = "contracts";
    public static final String ATTRIBUTE_TIMETABLES = "timetables";

    public static final String PARAM_OFFERID = "offerId";

    public static final String PAGE_INSERT = "/WEB-INF/docs/company/insertoffer.jsp";
    public static final String PAGE_OFFERS = "/WEB-INF/docs/company/companyoffers.jsp";
    public static final String PAGE_DATA = "/WEB-INF/docs/company/companydata.jsp";
    public static final String ACTION_EDIT = "edit";

    public static final String ACTION_INSERT = "add";
    public static final String ACTION_OFFERS = "offers";
    public static final String ACTION_MYDATA = "mydata";
    public static final String ACTION_DELETE="delete";
    
    protected OfferForm offerForm;
    protected UserAccount company;
    protected ConcernForm concernForm;
    protected Concern concern;
   

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        super.validateRequest(request, response);
        concern = (Concern) request.getSession().getAttribute(ATTRIBUTE_USER);
        if(concern == null){
            throw new RedirectException(PAGE_LOGINCOMP);
        }
        request.setAttribute(ATTRIBUTE_CONTRACTS, FrontController.contractCtl.findContractEntities().iterator());
        request.setAttribute(ATTRIBUTE_TIMETABLES, FrontController.timetableCtl.findTimetableEntities().iterator());
        request.setAttribute(ATTRIBUTE_TOWNS, FrontController.townCtl.getCastellonsTowns().iterator());        
        if (action.equals(ACTION_INSERT)) {
            request.setAttribute(ATTRIBUTE_SUBAREAS, FrontController.areaCtl.findAreaEntities().iterator());
            offerForm = new OfferForm();
            offerForm.map(request, response);
            if (isPost) {
                offerForm.validate(errorlist);

            }
            request.setAttribute("form", offerForm);
            page = PAGE_INSERT;
        }
        if (action.equals(ACTION_OFFERS)) {
            request.setAttribute("offers", concern.getOffers().iterator());
            request.setAttribute("hassoffers", !(concern.getOffers().isEmpty()));

            page = PAGE_OFFERS;
        }
        if (action.equals(ACTION_MYDATA)) {
            validateMyData(request, response);
            page = PAGE_DATA;
        }
        if (action.equals(ACTION_EDIT)) {
            validateEditOffer(false,concern.getId(),request,response);
        }
        if(action.equals(CompanyHandler.ACTION_DELETE)){
             validateDeleteOffer(false,concern.getId(),request,response);
             concern = FrontController.concernCtl.findConcern(concern.getId());
             request.setAttribute("offers", concern.getOffers().iterator());
             request.setAttribute("hassoffers", !(concern.getOffers().isEmpty()));
        }
        return errorlist.size() == 0;
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        try {
            concern = (Concern) request.getSession().getAttribute(ATTRIBUTE_COMPANY);
            if (action.equals(ACTION_INSERT)) {
                Offer offer = new Offer();
                offer.setCompany(concern);
                offerForm.mapData(offer);
                FrontController.offerCtl.create(offer);

            }
           if (action.equals(ACTION_MYDATA)) {
                concernForm.mapData(concern);
                FrontController.concernCtl.edit(concern);
                page = PAGE_DATA;
            }
            if (action.equals(ACTION_EDIT)) {
                handleEditOffer(false,concern.getId(),request, response);
            }
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CompanyHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CompanyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void validateMyData(HttpServletRequest request, HttpServletResponse response) {

        concernForm = new ConcernForm(concern);
        if (isPost) {
            concernForm.map(request, response);
            concernForm.validate(errorlist);
        }
        request.setAttribute("form", concernForm);
        page = PAGE_DATA;

    }

    @Override
    public String getJSP() {
        return page;
    }
}
