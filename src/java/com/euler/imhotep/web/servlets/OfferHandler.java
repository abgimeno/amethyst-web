/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Offer;
import com.euler.imhotep.persistence.UserAccount;
import com.euler.imhotep.web.forms.OfferForm;
import com.euler.web.handler.HandlerException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 */
public class OfferHandler extends ImhotepHandler {

    protected static final String HANDLERNAME = "offer";
    protected static final String ACTION_SHOW = "show";
    protected static final String ACTION_SUSBCRIBE = "subs";
    protected static final String ACTION_EDIT = "edit";

    protected static final String PARAM_OFFER = "offer";
    protected static final String PARAM_OFFERID = "offerId";
    
    protected static final String ATTRB_FORM = "form";

    // Pages for this handler
    protected static final String PAGE_INSERT = "/WEB-INF/docs/company/insertoffer.jsp";
    protected static final String PAGE_DISPLAY_OFFER = "/WEB-INF/docs/offers/display-offer.jsp";
    protected static final String PAGE_RESULTS = "/WEB-INF/docs/search/results.jsp";
    protected static final String PAGE_LOGINCANDIDATE = "/WEB-INF/docs/candidate/logincandidate.jsp";

    protected OfferForm offerForm;
    protected UserAccount user;
    
    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        super.validateRequest(request, response);
        if (action.equals(NOACTION)) {
            List<Offer> offers = FrontController.offerCtl.findOfferEntities(NRESULTS, 0);
            request.setAttribute("offers", offers.iterator());
            page = PAGE_RESULTS;

        } else if (action.equals(ACTION_SHOW)) {
            Long id = Long.parseLong(request.getParameter(PARAM_OFFERID));
            Offer offer = FrontController.offerCtl.findOffer(id);
            request.setAttribute(PARAM_OFFER, offer);
            setShowDescription(offer);
            setTitle(offer);
            page = PAGE_DISPLAY_OFFER;

        }
        return errorlist.isEmpty();
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        Offer offer = null;
        Long id = Long.parseLong(request.getParameter(PARAM_OFFERID));
        try {
            if (action.equals(ACTION_SUSBCRIBE)) {                
                offer = FrontController.offerCtl.findOffer(id);
                user = (UserAccount) request.getSession().getAttribute(ATTRIBUTE_USER);
                if (user == null) {
                    page = PAGE_LOGINCANDIDATE;
                } else if (!offer.getUsers().contains(user)) {
                    FrontController.userCtl.addUserToOffer(user, id);
                    request.setAttribute("ok", 1);
                    request.setAttribute(PARAM_OFFERID, id);
                    page = PAGE_DISPLAY_OFFER;
                } else {
                    request.setAttribute("ok", 0);
                    request.setAttribute("subcribed", 1);
                    page = PAGE_DISPLAY_OFFER;
                }
                user= FrontController.userCtl.findUserAccount(id);                
                request.setAttribute(PARAM_OFFER, offer);
            }
        } catch (Exception ex) {
            Logger.getLogger(OfferHandler.class.getName()).log(Level.SEVERE, null, ex);
             request.setAttribute(PARAM_OFFERID, id);

        }

    }

    @Override
    protected String getDescription() {
        return description;
    }

    @Override
    public String getJSP() {
        return page;
    }

    @Override
    protected String getTitle() {
        return title;
    }

    private void setShowDescription(Offer offer) {
        StringBuffer buffer = new StringBuffer("Oferta de trabajo en ");
        buffer.append(offer.getTown().getTown());
        buffer.append("| ");
        buffer.append("Empleo en ");
        buffer.append(offer.getTown().getTown());
        buffer.append("| ");
        buffer.append(offer.getJobTitle());
        description = buffer.toString();
    }

    private void setTitle(Offer offer) {
        StringBuffer buffer = new StringBuffer("| ");
        buffer.append(offer.getTown().getTown());
        buffer.append(" | ");
        buffer.append(offer.getJobTitle());
        title = buffer.toString();
    }
}
