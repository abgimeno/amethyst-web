/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.imhotep.web.servlets;

import com.euler.imhotep.persistence.Area;
import com.euler.imhotep.persistence.Offer;
import com.euler.web.handler.HandlerException;
import com.euler.web.paging.PageInfo;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abrahamgimeno
 */
public class SearchHandler extends ImhotepHandler {

    public static final String HANDLERNAME = "search";
    public static final String ACTION_KEYWORD = "key";
    public static final String ACTION_AREA = "area";
    public static final String ACTION_DEGREE = "degree";
    public static final String ACTION_BYCAT = "bycat";
    public static final String ACTION_MIX = "mix";
    public static final String ATTRB_RESULTS = "offers";
    public static final String ATTRB_AREAS = "areas";
    public static final String PARAM_AREAID = "areaId";
    public static final String PARAM_KEYWORD = "keyword";
    public static final String PARAM_DEGREELEVELID = "degreeLevelId";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NPAGE = "page";
    
    public static final String PAGE_RESULTS = "/WEB-INF/docs/search/results.jsp";
    public static final String ATTRIBUTE_PAGEINFO = "pageinfo";
    protected List<Offer> offers = new ArrayList<Offer>();
    protected Integer npage = 0;

    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {

        super.validateRequest(request, response);
        PageInfo pg = null;
        npage = (request.getParameter(PARAM_NPAGE) == null ? 0 : Integer.parseInt(request.getParameter(PARAM_NPAGE)));
        if (action.equals(NOACTION)) {
            offers = FrontController.offerCtl.findOfferEntities(NRESULTS, npage);

        } else {
            if (action.equals(ACTION_BYCAT)) {
                Integer areaId = Integer.parseInt(request.getParameter(PARAM_ID));
                Area area = FrontController.areaCtl.findArea((long) areaId);
                setShowDescription(area);
                setTitle(area);
                if (areaId != null) {
                    //npage = Integer.parseInt(request.getParameter(PARAM_NPAGE));
                    offers = FrontController.searchCtl.getOffersByArea(areaId, NRESULTS, NRESULTS * npage);
                    pg = new PageInfo("?serv=search&action=" + action + "&" + PARAM_ID + "=" + areaId + "&", NRESULTS);
                    pg.setPagesCount((offers.size() / NRESULTS));
                    pg.setActPage(npage);
                }
            }
            if (action.equals(ACTION_KEYWORD)) {
                String keyword = request.getParameter(PARAM_KEYWORD);
                offers = FrontController.searchCtl.getOffersByKeyword(keyword);
                pg = new PageInfo("?serv=search&action=" + action + "&keyword=&" + keyword + "&", NRESULTS);
                pg.setPagesCount((offers.size() / NRESULTS));
                pg.setActPage(npage);

            }

            if (action.equals(ACTION_AREA)) {
                Integer areaId = Integer.parseInt(request.getParameter(PARAM_AREAID));
                Area area = FrontController.areaCtl.findArea((long) areaId);
                setShowDescription(area);
                setTitle(area);
                offers = FrontController.searchCtl.getOffersByArea(areaId);//?serv=search&action=bycat&id=7
                pg = new PageInfo("?serv=search&action=" + action + "&id=" + areaId + "&", NRESULTS);
                pg.setPagesCount((offers.size() / NRESULTS));
            }

            if (action.equals(ACTION_DEGREE)) {
                Integer degreeLevelId = Integer.parseInt(request.getParameter(PARAM_DEGREELEVELID));
                offers = FrontController.searchCtl.getOffersByDegreeLevel(degreeLevelId);
                pg.setPagesCount((offers.size() / NRESULTS));
            }

            if (action.equals(ACTION_MIX)) {
                String keyword = request.getParameter(PARAM_KEYWORD);
                if (keyword == null) {
                    keyword = "";
                }
                Integer areaId = Integer.parseInt(request.getParameter(PARAM_AREAID));
                switch (Integer.parseInt(request.getParameter(PARAM_AREAID))) {
                    case 0:
                        setShowDescription(keyword);
                        setTitle(keyword);
                        if (keyword.length() > 0) {
                            offers = FrontController.searchCtl.getOffersByKeyword(keyword, NRESULTS, NRESULTS * npage);
                            pg = new PageInfo("?serv=search&action=" + action + "&keyword=" + keyword + "&" + PARAM_AREAID + "=" + areaId + "&", NRESULTS);
                            pg.setPagesCount(offers.size() / NRESULTS);

                        } else {
                            offers = FrontController.offerCtl.findOfferEntities(NRESULTS, NRESULTS * npage);
                            pg = new PageInfo("?serv=search&action=" + action + "&areaId=0&", NRESULTS);
                            pg.setPagesCount(offers.size() / NRESULTS);

                        }
                        //offers = ((keyword.length() > 0 )?FrontController.searchCtl.getOffersByKeyword(keyword):FrontController.offerCtl.findOfferEntities(NRESULTS,npage));
                        //pg = ((keyword.length() > 0 )?new PageInfo("?serv=search&action="+action+"&keyword=&"+keyword+"&areaId=0&",NRESULTS):new PageInfo("?serv=search&action="+action+"&areaId=0&",NRESULTS));
                        break;
                    default:
                        Area area = FrontController.areaCtl.findArea((long) areaId);
                        setShowDescription(area);
                        setTitle(area);
                        if (keyword.length() > 0) {
                            offers = FrontController.searchCtl.getOffersByArea(keyword, areaId, NRESULTS, NRESULTS * npage);
                            pg = new PageInfo("?serv=search&action=" + action + "&keyword=&" + keyword + PARAM_AREAID + "=" + areaId + "&", NRESULTS);
                            pg.setPagesCount(offers.size() / NRESULTS);

                        } else {
                            offers = FrontController.searchCtl.getOffersByArea(areaId, NRESULTS, NRESULTS * npage);
                            pg = new PageInfo("?serv=search&action=" + action + "&" + PARAM_AREAID + "=" + areaId + "&", NRESULTS);
                            pg.setPagesCount(offers.size() / NRESULTS);

                        }
                        //offers = ((keyword.length() > 0 )?FrontController.searchCtl.getOffersByArea(keyword, areaId):FrontController.searchCtl.getOffersByArea(areaId,NRESULTS,npage));
                        //pg = ((keyword.length() > 0 )?new PageInfo("?serv=search&action="+action+"&keyword=&"+keyword+PARAM_AREAID+"="+areaId+"&",NRESULTS):new PageInfo("?serv=search&action="+action+"&"+PARAM_AREAID+"="+areaId+"&",NRESULTS));
                        break;
                }

                pg.setActPage(npage);

            }
        }
        // TODO - The pagination tag is disabled because we don't want to deploy
        if (pg != null) {
            request.setAttribute(ATTRIBUTE_PAGEINFO, pg);
        }
        request.setAttribute(ATTRB_RESULTS, offers.iterator());
        page = PAGE_RESULTS;
        return true;
    }

    @Override
    protected String getDescription() {
        return description;
    }

    @Override
    protected String getTitle() {
        return title;
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        super.validateRequest(request, response);
        page = PAGE_RESULTS;
    }

    @Override
    public String getJSP() {
        return page;
    }

    private void setShowDescription(Area area) {
        StringBuffer buffer = new StringBuffer(" Ofertas de trabajo en el area profesional de ");
        buffer.append(area.getArea());
        buffer.append("| ");
        buffer.append(" Ofertas de empleo en el area profesional de ");
        buffer.append(area.getArea());
        description = buffer.toString();
    }

    private void setTitle(Area area) {
        StringBuffer buffer = new StringBuffer("| Empleo para ");
        buffer.append(area.getArea());
        title = buffer.toString();
    }

    private void setShowDescription(String keyword) {
        StringBuffer buffer = new StringBuffer(" Ofertas de trabajo en ");
        buffer.append(keyword);
        buffer.append("| ");
        buffer.append(" Ofertas de empleo para ");
        buffer.append(keyword);
        description = buffer.toString();
    }

    private void setTitle(String keyword) {
        StringBuffer buffer = new StringBuffer("| Empleo para ");
        buffer.append(keyword);
        title = buffer.toString();
    }
}
