<%-- 
    Document   : enabledaccount
    Created on : Apr 5, 2009, 10:18:20 PM
    Author     : abrahamgimeno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<html>
        <p class="error"><fmt:message key="${message}" /></p>
</html>
