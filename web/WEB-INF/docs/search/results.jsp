<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="web" uri="http://euler.com/web"%>

<fmt:setBundle basename="resource_user" />

<div id="main">
    <div class="top">Ofertas ${pages}</div>
    <div class="content">
        <div id="search">
            <div class="network"></div>
            <form action="/docs/?serv=search&action=mix" id="searchform" method="post">
              

                <input name="keyword" type="text" id="searchText" class="search">
                <select name="areaId" id="areaId" class="combo">
                        <option value="0">(Todas las categorías)</option>
                    <web:forList items="${areas}" var="item">
                        <option value="${item.id}" >${item.area}</option>
                    </web:forList>
                </select>

                <input class="searchButton" type="submit" id="doSearch" value="Buscar"/>
            </form>
        </div>
        <div id="results">
            <table>
                <tr class="underline">                   
                    <th class="borderright">Puesto</th>
                    <th class="borderright">Poblacion</th>                    
                    <th>Fecha</th>
                </tr>
                <web:forEach items="${offers}" var="offer" >
                    <tr class="${class}">
                        <td><a href="?serv=offer&action=show&offerId=${offer.id}">${offer.jobTitle}</a></td>
                        <td>${offer.town.town}</td>
                        <td>${offer.customOfferDate}</td>
                    </tr>
                </web:forEach>
            </table>
        </div>  
                <%--web:paging pageInfo="${pageinfo}" basePicurl="../img/img/">
                    <div class="pagination"> ${pages}</div>
                </web:paging--%>
    </div>
    <div class="bottom">

    </div>
</div> 
</div>     