<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>

<div id="categories-wrap">
    <div class="top"></div>
    <div class="content">
        <div id="cat-title">Categorías</div>

        <ul id="categories">
            <web:forList items="${areas}" var="entry">
                <li class=""><a href="/docs/?serv=search&action=bycat&id=${entry.id}">${entry.area}</a></li>
            </web:forList>
        </ul>

    </div>

    <div class="bottom"></div>
</div>