<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div id="main">
    <div class="top">Oferta de trabajo en ${offer.town.town}</div>
    <div class="content">
        <div class="displayOffer">
            <fieldset>
                <legend>${offer.jobTitle}</legend>
                <div class="space"></div>
                <div class="row"><span class="left">Fecha:</span><span class="right">${offer.customOfferDate}</span></div>
                <div class="row"><span class="left">Descripcion de la oferta:</span><span class="right">${offer.description}</span></div>
                <br>
                <div class="row"><span class="left">Localidad:</span><span class="right">${offer.town.town}</span></div>
                <br>
                <div class="row"><span class="left">Estudios mínimos:</span><span class="right">${offer.requiredDegreeName} - ${offer.requiredDegree.levelName}</span></div>
                <div class="row"><span class="left">Estudios deseados:</span><span class="right">${offer.desiredDegreeName} - ${offer.desiredDegree.levelName}</span></div>
                <div class="row"><span class="left">Experiencia mínima:</span><span class="right">${offer.experienceTime.name}</span></div>
                <br>
                <div class="row"><span class="left">Requisitos mínimos:</span><span class="right">${offer.requirements}</span></div>
                <div class="row"><span class="left">Requisitos deseados:</span><span class="right">${offer.desired}</span></div>
                <br>
                <br>
                <div class="row"><span class="left">Vacantes:</span><span class="right">${offer.positions}</span></div>
                <div class="row"><span class="left">Horario:</span><span class="right">${offer.timetable.name}</span></div>
                <div class="row"><span class="left">Tipo contrato:</span><span class="right">${offer.contract.type}</span></div>
                </br>
                <br>
                <div class="row"><span class="left">Salario:</span><span class="right">${offer.sallary}</span></div>
                </br>
            </fieldset>
            <div class="subscribe_button">
                <form action="?serv=offer&action=subs&offerId=${offer.id}" method="post">
                    <input type="submit" value="Inscribete">
                    <!--  <a class="button">Inscribete</a> -->
                </form>
                <br>
                    <%--c:if test="${ok eq 1}">
                        <div class="okmsg">Te has inscrito con éxito</div>
                    </c:if>
                    <c:if test="${subcribed eq 1}">
                        <div class="subcribed">Ya estás inscrito en esta oferta</div>
                    </c:if--%>
            </div>

        </div>


    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>
