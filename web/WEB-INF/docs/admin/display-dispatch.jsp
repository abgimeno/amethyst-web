<%-- 
    Document   : display-dispatch
    Created on : 12-may-2009, 18:04:06
    Author     : abraham
--%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
        $("#tabs > ul").tabs();

    });
</script>
<div id="main">
    <div class="top">Datos del envío</div>
    <div class="content">
        <div class="contactform">
            <fieldset>
                <legend>Datos de la persona de contacto</legend>
                <p><label for="firstname" class="left">Nombre:</label>
                <input type="text" name="firstname" id="firstname" class="field" disabled='true' value="${company.firstName}"></p>

                <p><label for="lastname1" class="left">Apellidos:</label>
                <input type="text" name="lastname1" id="lastname1" class="field" disabled='true' value="${company.surname1}"></p>

                <p><label for="email" class="left">Email:</label>
                <input name="email" id="email" tabindex="1" class="field" disabled='true' value="${company.emailAccount}" >

                <p><label for="timetable" class="left">Horario contacto:</label>
                <input name="timetable" id="timetable" tabindex="1" class="field" disabled='true' value="${company.timetable}" >
            </fieldset>
            <!--COMPANY DATA -->
            <fieldset>
                <legend>Datos de la empresa</legend>
                <p><label for="name" class="left">Nombre:</label>
                <input type="text" name="company_name" id="company_name" class="field" disabled='true' value="${company.company.companyName}" ></p>

                <p><label for="company_phone" class="left">Teléfono:</label>
                <input name="company_phone" id="company_phone" tabindex="9" class="field" disabled='true' value="${company.company.phoneNumber}" type="text"></p>
                
                <p><label for="company_cif" class="left">CIF:</label>
                <input type="text" name="company_cif" id="company_cif" class="field" disabled='true' value="${company.company.nif}"></p>

                <p><label for="address" class="left">Dirección<span></span></label>
                <input type="text" name="address" id="address" class="field" disabled='true' value="${company.company.address.address}"></p>

                <p><label for="city" class="left">Población<span></span></label>
                <input type="text" name="city" id="city" class="field" disabled='true' value="${company.company.address.city}"></p>

            </fieldset>

        </div>
        <div id="legend">Curriculums enviados:</div>
            <table id="candidates_table">
                <tbody>
                    <tr>
                        <th class="topp" scope="col">Nombre</th>
                        <th class="topp" scope="col">Email</th>
                        <th class="topp options" scope="col">Opciones</th>
                    </tr>
                    <web:forEach items="${candidates}" var="entry">
                        <tr>
                            <td>${entry.personalInfo.name} ${entry.personalInfo.surname1}</td>
                            <td>${entry.emailAccount}</td>
                            <td class="options"><a href="?serv=admin&action=show&userId=${entry.id}">Ver  </a>  <%--a id="borrar" href="?serv=admin&action=delete_user&userId=${entry.id}"> Borrar</a--%>
                            </td>
                        </tr>
                    </web:forEach>
                </tbody>
            </table>
      
    </div>
</div>