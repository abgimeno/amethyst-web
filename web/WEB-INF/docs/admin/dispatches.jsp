<%-- 
    Document   : dispatches
    Created on : 12-may-2009, 18:04:17
    Author     : abraham
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
        $("#tabs > ul").tabs();

    });
</script>

<div id="main">
    <div class="top">Env�os</div>

    <div class="content">
        <form action="/docs/?serv=admin&action=dispatches" method="post">
            <table id="envios_table">
                <tbody>
                    <tr>
                        <th class="topp" scope="col">C�digo</th>
                        <th class="topp" scope="col">Compa�ia</th>
                        <th class="topp" scope="col">Fecha</th>
                        <th class="topp small" scope="col">Pagado</th>
                    </tr>
                    <web:forEach items="${dispatches}" var="entry">
                        <tr>
                            <td><a href="?serv=admin&action=showdispatch&dispatchId=${entry.id}">${entry.id}</a></td>                            
                            <td>${entry.company.company.companyName}</td>
                            <td>${entry.customDispatchDate}</td>
                            <td class="small"><input type="checkbox" name="payedIds" value="${entry.id}" <c:if test="${entry.payed}"> CHECKED disabled="true"</c:if> /></td>
                        </tr>
                    </web:forEach>
                </tbody>
            </table>
            <th class="topp" scope="col"><input class="apply" type="submit" id="apply" value="Aplicar"/></th>
        </form>
    </div>
    <div class="bottom"></div>
</div>