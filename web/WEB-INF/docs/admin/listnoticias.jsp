<%-- 
    Document   : listnews
    Created on : Sep 30, 2009, 7:17:24 PM
    Author     : abrahamgimeno
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />


<div id="main">
    <div class="top">Administrador de Noticias</div>
    <div class="content">
        <div class="contactform">
            <a href="?serv=news&action=addnew">Crear una noticia nueva</a>
                <table id="companies_table">
                    <tbody>
                        <tr>
                            <th class="topp" scope="col">Título</th>
                            <th id="newdatecol" class="topp" scope="col">Fecha</th>
                            <th id ="newoptcol" class="topp" scope="col">Opciones</th>
                        </tr>
                        <web:forEach items="${noticias}" var="item">
                            <tr>
                                <td>${item.title} </td>
                                <td id="newdatecoldata">${item.customDate} </td>
                                <td id="newoptcoldata"><a href="?serv=news&action=editnew&id=${item.id}">Editar</a>
                                <a href="?serv=news&action=deletenew&id=${item.id}">Borrar </a></td>
                            </tr>
                        </web:forEach>
                    </tbody>
                </table>
        </div>
    </div>
    <div class="bottom"></div>
</div>


