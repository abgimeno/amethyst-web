<%-- 
    Document   : candidatelist
    Created on : 31-mar-2009, 12:41:27
    Author     : abraham
--%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
        $("#tabs > ul").tabs();

    });
</script>

<div id="main">
    <div class="top">Candidatos</div>
        
    <div class="content">
        <form id="filter_form" action="/docs/?serv=admin&action=showcurr" method="post">
            <table id="filters_table">
                <tr>
                    <th class="topp" scope="col">Filtro</th>
                    <th class="topp" scope="col">
                        <select name="tagId1" id="tagId1" class="combo">
                            <option value="0">(Ninguno)</option>
                            <web:forList items="${tags}" var="item">
                                <option value="${item.id}" >${item.name}</option>
                            </web:forList>
                    </select></th>
                    <th class="topp" scope="col">
                        <select name="conjunction" id="conjunction" class="combo">
                            <option value="1"> Y </option>
                            <option value="2"> O </option>
                    </select></th>
                    <th class="topp" scope="col">
                        <select name="tagId2" id="tagId2" class="combo">
                            <option value="0">(Ninguno)</option>
                            <web:forList items="${tags}" var="item">
                                <option value="${item.id}" >${item.name}</option>
                            </web:forList>
                    </select></th>
                    <th class="topp" scope="col"><input class="searchButton" type="submit" id="doSearch" value="Aplicar"/></th>
                </tr>
            </table>
        </form>
        <form action="/docs/?serv=admin&action=sendcv" method="post">
            <table id="candidates_table">
                <tbody>
                    <tr>
                        <th class="topp" scope="col">Nombre</th>
                        <th class="topp" scope="col">Email</th>
                        <th class="topp options" scope="col">Fecha de Alta</th>
                        <th class="topp options" scope="col">Opciones</th>
                        <th class="topp small" scope="col">Enviar</th>
                    </tr>
                    <web:forEach items="${candidates}" var="entry">
                        <tr>
                            <td>${entry.personalInfo.name} ${entry.personalInfo.surname1}</td>
                            <td>${entry.emailAccount}</td>
                            <td>${entry.customDate}</td>
                            <td class="options"><a href="?serv=admin&action=show&userId=${entry.id}">Ver  </a>
                                <a id="borrar" href="?serv=admin&action=delete_user&userId=${entry.id}"> Borrar</a>
                            </td>
                            <td class="small"><input type="checkbox" name="user_id" value="${entry.id}" /></td>
                        </tr>
                    </web:forEach>
                </tbody>
            </table>
           <div class="sendto">
                <select name="companyId" id="companyId" class="combo">
                    <web:forList items="${companies}" var="item">
                        <option value="${item.id}" >${item.company.companyName}</option>
                    </web:forList>
                </select>
            
            <input class="searchButton" type="submit" id="doSearch" value="Enviar"/>
            </div>
        </form>
    </div>
    <div class="bottom"></div>
</div>
