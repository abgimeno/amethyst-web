<%-- 
    Document   : companieslist
    Created on : 19-may-2009, 9:55:17
    Author     : abraham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>


<div id="main">
    <div class="top">Empresas</div>
    <div class="content">
       <form >
        <table id="companies_table">
            <tbody>
                <tr>
                    <th class="topp" scope="col">Empresa</th>
                    <%--th class="" id="small" scope="col">&nbsp;</th--%>
                    <th class="topp" scope="col">Persona de Contacto</th>
                    <th class="topp empresa" scope="col">Horario</th>
                </tr>
                <web:forEach items="${companies}" var="item">
                    <tr>
                        <td><a href="?serv=admin&action=showcompany&companyId=${item.id}"> ${item.company.companyName}</a></td>
                        <td>${item.firstName} ${item.surname1} </td>
                        <td class="empresa">${item.timetable}</td>
                    </tr>
                </web:forEach>
            </tbody>
        </table>
        </form>
    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>