<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
        $("#tabs > ul").tabs();

    });
</script>
<div id="main">
    <div class="top">Inscripciones</div>
    <div class="content">

        <table id="inscriptions_table">
            <tbody>
                <tr>
                    <th class="topp" scope="col">Candidato</th>
                    <%--th class="" id="small" scope="col">&nbsp;</th--%>
                    <th class="topp" scope="col">Oferta</th>
                    <th class="topp empresa" scope="col">Empresa</th>
                </tr>
                <c:if test="${!(hassubscriptions eq 1)}">
                    <web:forEach items="${subscriptions}" var="offer">
                        <tr>
                            <td><a href="?serv=admin&action=show&userId=${offer.user.id}"> ${offer.user.emailAccount}</a></td>
                            <td><a href="?serv=offer&action=show&offerId=${offer.offer.id}">${offer.offer.jobTitle}</a></td>
                            <td class="empresa">${offer.offer.company.company.companyName}</td>
                        </tr>
                    </web:forEach>
                </c:if>
                <c:if test="${hassubscriptions eq 1}">
                    <tr>
                        <td colspan="3">Nadie se ha inscrito en ninguna oferta todavía</td>
                    </tr>
                </c:if>

            </tbody>
        </table>

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>
