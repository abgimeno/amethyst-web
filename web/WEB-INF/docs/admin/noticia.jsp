<%-- 
    Document   : noticia
    Created on : Sep 30, 2009, 7:17:38 PM
    Author     : abrahamgimeno
--%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />

<script type="text/javascript">
    window.onload = function()
    {
        var oFCKeditor1 = new FCKeditor( 'text' ) ;
        oFCKeditor1.BasePath = "../fckeditor/";
        oFCKeditor1.ToolbarSet = "MyToolbar" ;
        oFCKeditor1.ReplaceTextarea() ;
    }
</script>
<div id="main">
    <div class="top"><c:choose>
                <c:when test="${action eq 'editnew'}">
                    Editar Noticia
                </c:when>
                <c:otherwise>
                    Crear Noticia
                </c:otherwise>
            </c:choose></div>
    <div class="content">
        
        <div class="contactform">
            <div id="back_tolist">
                <a href="?serv=news">Volver al Listado </a>
            </div>
            <c:choose>
                <c:when test="${action eq 'editnew'}">
                    <form action="?serv=news&action=editnew&id=${entry.id}" method="post">
                </c:when>
                <c:otherwise>
                    <form action="/docs/?serv=news&action=addnew" method="post">
                </c:otherwise>
            </c:choose>
                <p><label class="left">Título:</label>
                    <input name="title" id="title" tabindex="1" class="field" value="${entry.title}" type="text">
                </p>
                <web:iferror errorList="${errorlist}" error="title_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <p><label class="left">Fecha de publicación:</label>
                    <web:DaySelect name="birthday" id="birthday" htmlClass="combo" value="${date.birthday}">
                    </web:DaySelect>
                    <web:MonthSelect name="birthmonth" id="birthmonth" htmlClass="combo" value="${date.birthmonth}">
                    </web:MonthSelect>
                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="birthyear" id="birthyear" htmlClass="combo" value="${date.birthyear}">
                    </web:NumberSelect>
                </p>
                <p><label class="left">Texto de la noticia:</label>
                    <textarea  id="text" name="text" tabindex="2" class="field" >${entry.text}</textarea>
                </p>
                <web:iferror errorList="${errorlist}" error="text_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <p><label for="submit" class="left"></label>
                    <input name="submit" value="Guardar" class="myButton" type="submit">
                </p>

            </form>
        </div>
    </div>
    <div class="bottom"></div>
</div>

