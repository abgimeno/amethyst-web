<%-- 
    Document   : createtag
    Created on : 12-may-2009, 17:42:41
    Author     : abraham
--%>

<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>


<script src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script type="text/javascript" src="../js/lib/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="../js/lib/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.autocomplete.js"></script>
<script type='text/javascript' src='../js/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/lib/thickbox-compressed.js'></script>
<script type="text/javascript" src="../js/jquery.multiselects.js"></script>
<script type="text/javascript" src="../js/swfobject.js" ></script>
<fmt:setBundle basename="resource" />
<script>

    $(document).ready(function(){
        $("#tabs > ul").tabs();
    });
</script>
<script>
    $(document).ready(function() {
        /////lanuages/////
        $("#select_left2").multiSelect("#select_right2", {trigger: "#options_right2"});
        $("#select_right2").multiSelect("#select_left2", {trigger: "#options_left2"});


    })
</script>
<div id="main">
    <div class="top">Etiquetas</div>
    <div class="content">
        <div class="contactform">
            <c:if test="${action eq 'edit_tag'}">
                <form action="?serv=admin&action=edit_tag&tagId=${tag.id}" method="post">
            </c:if>
            <c:if test="${action eq 'create_tag'}">
                <form action="/docs/?serv=admin&action=create_tag" method="post">
            </c:if>
                <table id="companies_table">
                    <tbody>
                        <tr>
                            <th class="topp" scope="col">Etiqueta</th>
                            <th class="topp" scope="col">Opciones</th>
                        </tr>
                        <web:forEach items="${tags}" var="item">
                            <tr>
                                <td>${item.name} </td>
                                <td><a href="?serv=admin&action=edit_tag&tagId=${item.id}">Editar</a>
                                <a href="?serv=admin&action=delete_tag&tagId=${item.id}">Borrar </a></td>
                            </tr>
                        </web:forEach>
                    </tbody>
                </table>
                <p><label for="title" class="left">Nombre:</label>
                    <input name="name" id="name" tabindex="1" class="field" value="${entry.text}" type="text">
                </p>
                <p><label for="submit" class="left"></label>
                    <input name="submit" id="create_tag" class="myButton" type="submit" value="<fmt:message key="${action}" />">
                </p>
            </form>
        </div>
    </div>
    <div class="bottom"></div>
</div>

