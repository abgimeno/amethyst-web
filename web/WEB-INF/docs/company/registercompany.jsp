<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<div id="main">
    <div class="top">Alta de Empresa</div>
    <div class="content">
        <h2></h2>
        <div class="contactform">
            <form action="/docs/?serv=register&action=do_register&user=company" method="post">

                <p><label for="email" class="left">E-mail:<span>*</span></label>
                <input name="email" id="email" tabindex="1" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="email_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="email_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="email_email">
                        <p class="error"><fmt:message key="user_error_email_format" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="create_password" class="left">Contraseña:<span>*</span></label>
                <input name="create_password" id="create_password" tabindex="2" class="field" value="" type="password"></p>
                <web:iferror errorList="${errorlist}" error="create_password_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="create_password_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="create_password_length" >
                        <p class="error"><fmt:message key="password_error_length" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="create_password2" class="left">Contraseña otra vez:<span>*</span></label>
                <input name="create_password2" id="create_password2" tabindex="3" class="field" value="" type="password"></p>
                <web:iferror errorList="${errorlist}" error="create_password2_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="create_password2_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="create_password2_length" >
                        <p class="error"><fmt:message key="password_error_length" /></p>
                    </web:iferror>
                </web:iferror>

                <web:iferror errorList="${errorlist}" error="create_password_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="create_password_length" option="not">
                        <web:iferror errorList="${errorlist}" error="create_password2_notnull" option="not">
                            <web:iferror errorList="${errorlist}" error="create_password2_length" option="not">
                                <web:iferror errorList="${errorlist}" error="password_notequals">
                                    <p class="error"><fmt:message key="passwords_notequals" /></p>
                                </web:iferror>
                            </web:iferror>
                        </web:iferror>
                    </web:iferror>
                </web:iferror>


                <p><label for="firstname" class="left">Nombre:<span>*</span></label>
                <input name="firstname" id="firstname" tabindex="4" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="firstname_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="firstname_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="firstname_length" >
                        <p class="error"><fmt:message key="error_length" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="lastname1" class="left">Apellidos:<span>*</span></label>
                    <input name="lastname1" id="lastname1" tabindex="5" class="field" value="" type="text">
                <input name="lastname2" id="lastname2" tabindex="6" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="lastname1_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="lastname1_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="lastname1_length" >
                        <p class="error"><fmt:message key="error_length" /></p>
                    </web:iferror>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="lastname2_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
                <p><label for="company_name" class="left">Nombre de la empresa:<span>*</span></label>
                <input name="company_name" id="company_name" tabindex="6" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="company_name_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="company_name_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="company_name_length" >
                        <p class="error"><fmt:message key="error_length64" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="company_phone" class="left">Teléfono de la empresa:<span>*</span></label>
                <input name="company_phone" id="company_phone" tabindex="9" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="company_phone_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="company_phone_notnull" option="not" >
                    <web:iferror errorList="${errorlist}" error="company_phone_length" >
                        <p class="error"><fmt:message key="error_length64" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="timetable" class="left">Horario de Contacto: </label>
                    <select name="timetable" id="timetable" class="combo">
                        <option value="Mañana">Mañana</option>
                        <option value="Tarde">Tarde</option>
                        <option value="Oficina">Oficina</option>
                        <option value="Fines de Semana" >Fines de Semana</option>
                        <option value="Indiferente"  >Indiferente</option>
                    </select>
                </p>
                <p><label for="description" class="left">Nota Legal:</label>
                    <textarea name="description" id="description_legal" tabindex="4" class="field" value="" type="textarea" readonly>En cumplimiento de lo dispuesto en el artículo 5 de la Ley Orgánica 15/1999, de 13 de diciembre, de Proteción de Datos de Carácter Personal y art. 21 de la Ley 34/2002 SSI de correo electrónico, le informamos que los datos personales que haya facilitado en esta comunicación o facilite en el futuro están incorporados a un fichero creado por Imhotep Asesores SL, con la finalidad de poder gestionar la relación negocial que nos vincula e informarle de nuestros servicios y si usted no desea recibir más información sobre nuestros servicios, puede darse de baja en la siguiente dirección de correo electrónico  info@imhotepservicios.com.


Este mensaje, su contenido y cualquier fichero transmitido con él, contienen información de carácter confidencial, estando exclusivamente dirigido a su destinatario o destinatarios y quedando prohibida su divulgación, reproducción o distribución.


En virtud de lo dispuesto en el artículo 15 y siguientes de la LOPD y en los términos que indica su Reglamento de desarrollo aprobado por Real Decreto 1720/2007, de 21 de diciembre, en cualquier momento usted podrá ejercer sus derechos de acceso, rectificación, cancelación  y oposición, dirigiéndose por escrito a Imhotep Asesores SL, CL. Prim 12 3 D, Castellón, 12003.
IMHOTEP ASESORES, S.L.
CIF: B-12750410
C/PRIM, 12 3º D
12003-CASTELLON
INSCRITA EN EL REGISTRO MERCANTIL DE CASTELLON:
Tomo: 1408
Libro: 970
Folio: 44
Hoja: CS-28005
Inscrip: 1
</textarea>
                </p>
                <p><label for="description" class="left">Acepto las condiciones legales</label>
                <input type="checkbox" name="accept" value="true"></p>
                <web:iferror errorList="${errorlist}" error="accept_terms" >
                    <p class="error"><fmt:message key="accept_terms" /></p>
                </web:iferror>

                <p><label for="cmdwebregister" class="left"></label><input name="cmdwebregister" class="myButton" value="Registrar" type="submit"></p>


            </form>
        </div>


    </div>

    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>          
