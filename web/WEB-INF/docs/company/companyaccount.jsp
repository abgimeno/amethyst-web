<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<script src="../js/jquery-1.2.3.min.js"></script> 
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
    	 $("#tabs > ul").tabs();

    });
</script>
                <div id="main">
                    <div class="top">Menu Privado</div>
                    <div class="content">
                    <div id="tabs">
                    <ul class="tab_format">
            			<li><a href="#fragment-1"><span>Datos de Empresa</span></a></li>
     
        			</ul>  
        			 <div id="fragment-1" class="ui-tabs-panel contactform">
        			 			<div class="contactform">
       							 <form action="#" method="post">

					                    <p><label for="firstname" class="left">Nombre<span>*</span></label>
						                    <input type="text" name="firstname" id="firstname" class="field" value="${form.firstname}" ></p>
						                    <web:iferror errorList="${errorlist}" error="firstname_notnull">
						                        <p class="error"><fmt:message key="error_null" /></p>
						                    </web:iferror>
					                    </p>   
					                    					                
						                <p><label for="lastname1" class="left">Apellidos<span>*</span></label>
						                    <input type="text" name="lastname1" id="lastname1" class="field" value="${form.lastName1}">
						                	<input type="text" name="lastname2" id="lastname2" class="field" value="${form.lastName2}"></p>
							                <web:iferror errorList="${errorlist}" error="lastname1_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="lastname1_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="lastname1_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
							            </p>
						               
						                <p><label for="birthday" class="left">Fecha de nacimiento<span>*</span></label>
						                    <web:DaySelect name="birthday" id="birthday" htmlClass="combo" value="${form.birthday}">
						                    </web:DaySelect>
						                    <web:iferror errorList="${errorlist}" error="birthday_notnull">
						                        <p class="error"><fmt:message key="error_null" /></p>
						                    </web:iferror>
						                    <web:MonthSelect name="birthmonth" id="birthmonth" htmlClass="combo" value="${form.birthmonth}">
						                    </web:MonthSelect>
						                    <web:iferror errorList="${errorlist}" error="birthmonth_notnull">
						                        <p class="error"><fmt:message key="error_null" /></p>
						                    </web:iferror>
						                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="birthyear" id="birthyear" htmlClass="combo" value="${form.birthyear}">       
						                    </web:NumberSelect>
						                    <web:iferror errorList="${errorlist}" error="birthyear_notnull">
						                        <p class="error"><fmt:message key="error_null" /></p>
						                    </web:iferror>
						                </p>
						                
						                <p><label for="address" class="left"><fmt:message key="address" /><span>*</span></label>
							                <input type="text" name="address" id="address" class="field" value="${form.address}"></p>
							                <web:iferror errorList="${errorlist}" error="address_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="address_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="address_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
							            </p>
						                
						                <p><label for="city" class="left"><fmt:message key="city" /><span>*</span></label>
							                <input type="text" name="city" id="city" class="field" value="${form.city}"></p>
							                <web:iferror errorList="${errorlist}" error="city_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="city_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="city_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
						                </p>
						                
						                <p><label for="province" class="left"><fmt:message key="province" /><span>*</span></label>
							                <input type="text" name="province" id="province" class="field" value="${form.province}"></p>
							                <web:iferror errorList="${errorlist}" error="province_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="province_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="province_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
						                </p>
						                <p><label for="country" class="left"><fmt:message key="country" /><span>*</span></label>
							                <input type="text" name="country" id="country" class="field" value="${form.country}"></p>
							                <web:iferror errorList="${errorlist}" error="country_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="country_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="country_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
						                </p>
						                <p><label for="mobile" class="left"><fmt:message key="mobile" /></label>
							                <input type="text" name="mobile" id="mobile" class="field" value="${form.mobile}"></p>
							                <web:iferror errorList="${errorlist}" error="mobile_length" >
							                    <p class="error"><fmt:message key="error_length" /></p>
							                </web:iferror>
						                </p>
						                <p><label for="zip" class="left"><fmt:message key="zip" /><span>*</span></label>
							                <input type="text" name="zip" id="zip" class="field" value="${form.ZIP}"></p>
							                <web:iferror errorList="${errorlist}" error="zip_notnull">
							                    <p class="error"><fmt:message key="error_null" /></p>
							                </web:iferror>
							                <web:iferror errorList="${errorlist}" error="zip_notnull" option="not">
							                    <web:iferror errorList="${errorlist}" error="zip_length" >
							                        <p class="error"><fmt:message key="error_length" /></p>
							                    </web:iferror>
							                </web:iferror>
						                </p>
						                
						                <p colspan="2"><input class="button" type="submit" value="Guardar y continuar"></p>
						        
						        </form>
						        <span>*<fmt:message key="requiredfieldsmessage" /></span>
						    </div>
        			 </div> 
        			                    
                    </div>
                     </div>
                    <div class="bottom">
                        <div id="pagination">
                            
                        </div>
                    </div>
                </div> 
                </div> 