
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<div id="main">
    <div class="top">Menu Privado</div>
    <div class="content">
        <div class="offersLink">
            <a href="?serv=company&action=offers" id="Linktitle" >Tus Ofertas</a>
        </div>
        <div class="insertOffersLink">
            <a href="?serv=company&action=add"  id="Linktitle">Insertar Oferta</a>
        </div>
        <div class="insertOffersLink">
            <a href="?serv=company&action=mydata"  id="Linktitle">Datos de la Empresa</a>
        </div>
        <c:if test="${isAdmin}">
            <jsp:include page="../admin/menuadmin.jsp" flush="true"/>
        </c:if>
    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>