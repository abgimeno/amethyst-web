<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />

<div id="main">
    <div class="top">Acceso empresas</div>
    <div class="content">
        <h2></h2>
        <div class="logincompany">
            <h3>¿Tienes cuenta? Entrar</h3>
            <form action="/docs/?serv=login&action=login&user=company" method="post">

                <p><label for="usernameCompany" class="topp">Nombre de Usuario:</label><br>
                <input name="userlogin" id="userlogin" tabindex="1" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="userlogin_notnull">
                    <p class="error"><fmt:message key="username_error_null"/></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="userlogin_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="userlogin_email">
                        <p class="error"><fmt:message key="user_error_email_format" /></p>
                    </web:iferror>
                </web:iferror>

                <p><label for="passwordCompany" class="topp">Contraseña:</label><br>
                <input name="password" id="password" tabindex="2" class="field" value="" type="password"></p>
                <web:iferror errorList="${errorlist}" error="password_notnull">
                    <p class="error"><fmt:message key="password_error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="password_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="password_length" >
                        <p class="error"><fmt:message key="password_error_length" /></p>
                    </web:iferror>
                </web:iferror>

                <web:iferror errorList="${errorlist}" error="login_failed">
                    <p class="error"><fmt:message key="failed_login" /></p>
                </web:iferror>


                <p><input name="cmdweblogin" class="myButton" value="Entrar" type="submit"></p>

                <p><a href="#" id="forgotpsswd_1">¿Has olvidado tu contraseña?</a></p>

            </form>
        </div>
        <div class="registercompany">
            <h3>¿Todavía no tienes cuenta?<br>Aprovecha nuestras ventajas</h3>
            <ul id="characteristics">
                <li>bolsa de trabajo centrada en la provincia de Castellón</li>
                <li>te ayudamos a encontrar la persona con el perfil adecuado a tu empresa</li>
                <li>preselección de curriculums recibidos</li>
                <li>solo 2 euros por curriculum</li>
                <li>atención personalizada</li>

            </ul>

            <a href="/docs/?serv=register&action=do_register&user=company">Regístrate</a>

        </div>

        

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>  
</div>     