<%--
    Document   : main
    Created on : 30-ene-2009, 17:09:41
    Author     : abraham
--%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page buffer="none"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="web" uri="http://euler.com/web"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Castellon Empleo ${title} </title><%--<title>$Revision: 1.1.1.1 $ / ${store.storeTitle}</title>--%>
        <link rel="stylesheet" href="../css/estilo.css">
        <link rel="stylesheet" href="../css/lightbox.css" type="text/css" media="screen" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="description" content="${description}">
        <meta name="keywords" content="castellon, trabajar, empleo, oferta, trabajo, ofertas de trabajo, ofertas de empleo, buscar trabajo, busco empleo, trabajar en  castellon, busco trabajo en castellon, busco trabajo, ofertas de trabajo en castellon, ofertas de empleo en castellon">
        <script type="text/javascript" src="../js/jquery.js" ></script>
        <script type="text/javascript" src="../js/swfobject.js" ></script>
        <script type="text/javascript" src="../js/interface.js" ></script>
        <script type="text/javascript" src="../js/prototype.js"></script>
        <script type="text/javascript" src="../js/scriptaculous.js?load=effects,builder"></script>
        <script type="text/javascript" src="../js/lightbox.js"></script>
        <script type="text/javascript">
            swfobject.embedSWF("../flash/banner2.swf","box","350","150","9.0.0", "expressInstall.swf");
        </script>
        <script type="text/javascript" src="../fckeditor/fckeditor.js"></script>
    </head>
    <body>


        <div id="header">
            <div id="header-wrap">
                <a href="/docs/"><div id="logo"></div></a>
                <div id="box">
                    <div id="box1">Encuentra trabajo en Castellón</div>
                    <div id="box2">En CastellonEmpleo tenemos una bolsa de empleo centrada en la provincia de Castellón.</div>
                    <div id="box3">Da de alta tu curriculum y empieza a buscar trabajo. </div>

                </div>
                <div id="banner" class="FatSquare">
                    <a href="/files/bonificaciones_sordos.pdf">BONIFICACIONES PARA DISCAPACITADOS A. </a><br><br>
                    <a href="/files/bonificaciones_down.pdf">  BONIFICACIONES PARA DISCAPACITADOS I.</a>
                </div>
            </div>
        </div>


        <div id="container">
            <div id="container-wrap">
                <div id="bar">

                    <ul class="login">
                        <li class="start"><a class="" href="/docs/">Inicio</a></li>
                        <li class="barnews"><a class="" href="/docs/?serv=newsdisplay">Noticias</a></li>
                        <li class="register"><a class="" href="/docs/?serv=login&action=login&user=candidate">Acceso Candidatos</a></li>
                        <li class="login2"><a class="" href="/docs/?serv=login&action=login&user=company">Acceso Empresas</a></li>
                        

                    </ul>
                </div>
                <div id="sidebar">
                    <div class="top"></div>
                    <div class="content">
                        <%--script type="text/javascript"><!--
                            google_ad_client = "pub-5346560976035336";
                            /* 125x125, created 5/20/09 */
                            google_ad_slot = "9590074949";
                            google_ad_width = 125;
                            google_ad_height = 125;
                            //-->
                        </script>
                        <script type="text/javascript"
                                src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                        </script--%>
                        <jsp:include page="${sitebar}" flush="true"/>
                        <div class="bottom"></div>
                    </div>
                </div>
                <web:content/>
                
            </div>
            <div id="footer">
                <div id="footer-wrap">
                    <div class="top"></div>
                    <div class="content">
                        <ul class="info">
                            <li><a href="../img/sobre.jpg" rel="lightbox">Sobre castellonempleo.es</a></li>
                            <li><a href="../legal_info.html"  target="_blank">Condiciones legales</a></li>
                            <li><a href="mailto:contacto@castellonempleo.es">Contacto</a></li>
                        </ul>
                    </div>
                    <div class="bottom"></div>

                </div>
            </div>
            <div id="publicity">
                <div id="publicity-wrap">
                    <div class="top"></div>
                    <div class="content">
                        <script type="text/javascript"><!--
                            google_ad_client = "pub-9433246160245072";
                            /* BannerBigBottom */
                            google_ad_slot = "5530018458";
                            google_ad_width = 728;
                            google_ad_height = 90;
                            //-->
                        </script>
                        <script type="text/javascript"
                                src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                        </script>


                    </div>
                    <div class="bottom"></div>
                </div>
            </div>
            <script type="text/javascript">
                var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
            <script type="text/javascript">
                try {
                    var pageTracker = _gat._getTracker("UA-7938750-2");
                    pageTracker._trackPageview();
                } catch(err) {}</script>
        </div>
    </body>

    <HEAD>
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    </HEAD>

</html>