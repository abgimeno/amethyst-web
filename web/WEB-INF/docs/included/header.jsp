<%-- 
    Document   : header
    Created on : 03-abr-2009, 9:46:07
    Author     : abraham
--%>
<html>
    <head>
        <meta equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CastellonEmpleo | Tu pagina de empleo en Castellon</title><%--<title>$Revision: 1.1.1.1 $ / ${store.storeTitle}</title>--%>
        <link rel="stylesheet" href="../css/estilo.css">
        <link rel="stylesheet" href="../css/lightbox.css" type="text/css" media="screen" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="description" content="${description}">
        <%--meta name="keywords" content="castellon, trabajar, empleo, oferta, trabajo, ofertas de trabajo, ofertas de empleo, buscar trabajo, busco empleo, trabajar en  castellon, busco trabajo en castellon, busco trabajo, ofertas de trabajo en castellon, ofertas de empleo en castellon"--%>
        <script type="text/javascript" src="../../js/jquery-1.2.6.min.js"></script>
        <script type="text/javascript" src="../js/swfobject.js" ></script>
        <script type="text/javascript" src="../js/interface.js" ></script>
        <script type="text/javascript" src="../js/prototype.js"></script>
        <script type="text/javascript" src="../js/scriptaculous.js?load=effects,builder"></script>
        <script type="text/javascript" src="../js/lightbox.js"></script>
        <div id="header">
            <div id="header-wrap">
                <a href=""><div id="logo"></div></a>
                <div id="box">
                    <object
                        codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0"
                        width="350"
                        height="150">
                        <param name="quality" value="high" />
                        <param name="wmode" value="transparent">
                        <embed src="/flash/banner2.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="350" height="150" wmode="transparent"></embed>
                    </object>
                    <div id="box1">Encuentra trabajo en Castellón</div>
                    <div id="box2">En CastellonEmpleo tenemos una bolsa de empleo centrada en la provincia de Castellón.</div>
                    <div id="box3">Da de alta tu curriculum y empieza a buscar trabajo. </div>
                </div>
                <div id="banner" class="FatSquare">
                </div>
            </div>
        </div>
    </head>
    <body>
