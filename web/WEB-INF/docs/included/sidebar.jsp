<%-- 
    Document   : sidebar
    Created on : 03-abr-2009, 9:58:37
    Author     : abraham
--%>
<div id="sidebar">
    <div class="top"></div>
    <div class="content">
        <div id="categories-wrap">
            <div class="top"></div>
            <div class="content">
                <div id="cat-title">Categorķas</div>

                <ul id="categories">
                    <web:forList items="${areas}" var="entry">
                        <li class=""><a href="/docs/?serv=search&action=bycat&id=${entry.id}">${entry.area}</a></li>
                    </web:forList>
                </ul>

            </div>

            <div class="bottom"></div>
        </div>
        <div class="bottom"></div>
    </div>
</div>
