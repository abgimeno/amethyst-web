<%-- 
    Document   : footer
    Created on : 03-abr-2009, 9:57:12
    Author     : abraham
--%>

<div id="footer">
    <div id="footer-wrap">
        <div class="top"></div>
        <div class="content">
            <ul class="info">
                <li><a href="../img/sobre.jpg" rel="lightbox">Sobre castellonempleo.es</a></li>
                <li><a href="../legal_info.html"  target="_blank">Condiciones legales</a></li>
                <li><a href="mailto:contacto@castellonempleo.es">Contacto</a></li>
            </ul>
        </div>
        <div class="bottom"></div>
    </div>
</div>
<div id="publicity">
    <div id="publicity-wrap">
        <div class="top"></div>
        <div class="content">
        </div>
        <div class="bottom"></div>
    </div>
</div>
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-7938750-2");
        pageTracker._trackPageview();
    } catch(err) {}</script>

</body>
<HEAD>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>

</html>
