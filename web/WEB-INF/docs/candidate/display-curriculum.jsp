<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script type="text/javascript" src="../js/lib/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="../js/lib/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.autocomplete.js"></script>
<script type='text/javascript' src='../js/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/lib/thickbox-compressed.js'></script>
<script type="text/javascript" src="../js/jquery.multiselects.js"></script>
<script type="text/javascript" src="../js/swfobject.js" ></script>
<script>

    $(document).ready(function(){
        $("#tabs > ul").tabs();
    });
</script>
<script>
    $(document).ready(function() {
        /////lanuages/////
        $("#select_left2").multiSelect("#select_right2", {trigger: "#options_right2"});
        $("#select_right2").multiSelect("#select_left2", {trigger: "#options_left2"});


    })
</script>
<div id="main">
    <div class="top">Curriculum de ${user_data.name} ${user_data.surname1}</div>
    <div class="content">
        <div class="displayOffer">
            <fieldset>
                <legend>Datos Personales</legend>
                <div class="space"></div>
                <div class="row"><span class="left">Nombre:</span><span class="right">${user_data.name}</span></div>
                <div class="row"><span class="left">Apellidos:</span><span class="right">${user_data.surname1}&nbsp;${user_data.surname2}</span></div>
                <br>
                <div class="row"><span class="left">Fecha de nacimiento:</span><span class="right">${birthdate}</span></div>
                <br>
                    <div class="row"><span class="left">Email:</span><span class="right">${user_data.email}</span></div>
                <div class="row"><span class="left">Dirección:</span><span class="right"><c:if test="${!(user_data.address.address eq null)}">${user_data.address.address},&nbsp;</c:if> <c:if test="${!(user_data.address.town eq null)}">${user_data.address.town},&nbsp;</c:if> <c:if test="${!(user_data.address.province eq null)}"> ${user_data.address.province},&nbsp;</c:if> <c:if test="${!(user_data.address.country eq null)}">${user_data.address.country},&nbsp;</c:if> ${user_data.address.zip} </span></div>
                <div class="row"><span class="left">Telefono:</span><span class="right"><c:if test="${!(user_data.mobilePhone eq null)}">${user_data.mobilePhone},&nbsp;</c:if> ${user_data.publicPhone}</span></div>
                </br>
            </fieldset>

            <fieldset>
                <legend>Estudios</legend>
                <c:if test="${(hasstudies eq 'true')}">
                <web:forList items="${user_data.studies}" var="study">
                     <br>
                     <div class="row"><span class="left">Título:</span><span class="right">${study.degreeName}</span></div>
                     <div class="row"><span class="left">Nivel:</span><span class="right">${study.degreeLevel.levelName}</span></div>
                     <div class="row"><span class="left">Periodo:</span><span class="right">${study.customStartDate} - <c:if test="${study.cursando eq 'true'}">Cursando</c:if> <c:if test="${!(study.cursando eq 'true')}">${study.customFinishDate}</c:if> </span></div>
                     <div class="row"><span class="left">Centro:</span><span class="right">${study.universityName}</span></div>
                     <br>
                     <hr class="separator">
                </web:forList>
            </c:if>
            </fieldset>
            <fieldset>
                <legend>Experiencia</legend>
            <c:if test="${(hasexperiences eq 'true')}">
                <web:forList items="${user_data.experiences}" var="exp">
                     <br>
                     <div class="row"><span class="left">Puesto:</span><span class="right">${exp.position}</span></div>
                     <div class="row"><span class="left">Periodo:</span><span class="right">${exp.customStartDate} - <c:if test="${exp.currently eq 'true'}">Cursando</c:if> <c:if test="${!(exp.currently eq 'true')}">${exp.customFinishDate}</c:if></span></div>
                     <div class="row"><span class="left">Empresa:</span><span class="right">${exp.company}</span></div>
                     <div class="row"><span class="left">Área Profesional:</span><span class="right">${exp.area.area}</span></div>
                     <div class="row"><span class="left">Descripción del puesto:</span><span class="right">${exp.description}</span></div>
                     <br>
                     <hr class="separator">
                </web:forList>
            </c:if>
            </fieldset>
            <fieldset>
                <legend>Conocimientos</legend>
           <c:if test="${(hasknowledge eq 'true')}">
                <web:forList items="${user_data.knowledges}" var="know">
                     <br>
                     <div class="row"><span class="left">Conocimiento:</span><span class="right">${know.knowledge}</span></div>
                     <div class="row"><span class="left">Nivel:</span><span class="right">${know.level.knowLevel}</span></div>
                     <div class="row"><span class="left">Experiencia:</span><span class="right">${know.experience.name}</span></div>
                     <div class="row"><span class="left">Descripción:</span><span class="right">${know.description}</span></div>
                     <br>
                     <hr class="separator">
                </web:forList>
            </c:if>
            </fieldset>
            <c:if test="${isAdmin}">
                <fieldset>
                    <legend>Etiquetas</legend>
                    <form action="/docs/?serv=admin&action=show&userId=${user_data.id}" method="post">
                        <div class="select_boxes">
                            <select name="subarea_name" id="select_left2" multiple="multiple" size=10>
                                <web:forEach items="${tags}" var="entry">
                                    <option value="${entry.id}" class="parent">${entry.name}</option>
                                </web:forEach>
                            </select>


                            <a id="options_right2" href="#">Derecha</a>
                            <a id="options_left2" href="#">Izquierda</a>
                            <select name="selected_tags" id="select_right2" multiple="multiple" size=10>
                                <web:forEach items="${selectedtags}" var="entry" >
                                    <option value="${entry.id}" class="child">${entry.name}</option>
                                </web:forEach>
                            </select>
                            <web:iferror errorList="${errorlist}" error="selected_subareas_notnull">
                                <p class="error"><fmt:message key="error_null" /></p>
                            </web:iferror>
                        </div>
                        <p><label for="submit" class="left"></label>
                            <input name="submit" id="tag_user" class="myButton" type="submit" value="Aplicar">
                        </p>
                    </form>
                </fieldset>
            </c:if>
        </div>

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>
