<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/ui.tabs.js"></script>
<link rel="stylesheet" href="../../css/estilo.css">
<fmt:setBundle basename="resource" />
<div id="fragment-1" class="ui-tabs-panel contactform">
    <div class="contactform">
        <form action="" method="post">

            <p><label for="firstname" class="left">Nombre<span>*</span></label>
            <input type="text" name="firstname" id="firstname" class="field" value="${form.firstName}" ></p>
            <web:iferror errorList="${errorlist}" error="firstname_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="firstname_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="firstname_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>


            <p><label for="lastname1" class="left">Apellidos<span>*</span></label>
                <input type="text" name="lastname1" id="lastname1" class="field" value="${form.lastName1}">
            <input type="text" name="lastname2" id="lastname2" class="field" value="${form.lastName2}"></p>
            <web:iferror errorList="${errorlist}" error="lastname1_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="lastname1_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="lastname1_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>


            <p><label for="birthday" class="left">Fecha de nacimiento<span>*</span></label>
            <web:DaySelect name="birthday" id="birthday" htmlClass="combo" value="${form.birthday}">
            </web:DaySelect>
            <web:iferror errorList="${errorlist}" error="birthday_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:MonthSelect name="birthmonth" id="birthmonth" htmlClass="combo" value="${form.birthmonth}">
            </web:MonthSelect>
            <web:NumberSelect from="${startyear}" to="${currentyear}" name="birthyear" id="birthyear" htmlClass="combo" value="${form.birthyear}">
            </web:NumberSelect>

            <p><label for="address" class="left">Dirección<span>*</span></label>
            <input type="text" name="address" id="address" class="field" value="${form.address}"></p>
            <web:iferror errorList="${errorlist}" error="address_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="address_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="address_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>

            <p><label for="city" class="left">Población<span>*</span></label>
            <input type="text" name="city" id="city" class="field" value="${form.city}"></p>
            <web:iferror errorList="${errorlist}" error="city_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="city_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="city_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>


            <p><label for="province" class="left">Provincia<span>*</span></label>
            <input type="text" name="province" id="province" class="field" value="${form.province}"></p>
            <web:iferror errorList="${errorlist}" error="province_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="province_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="province_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>

            <p><label for="country" class="left">Pais<span>*</span></label>
            <input type="text" name="country" id="country" class="field" value="${form.country}"></p>
            <web:iferror errorList="${errorlist}" error="country_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="country_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="country_length" >
                    <p class="error"><fmt:message key="error_length" /></p>
                </web:iferror>
            </web:iferror>

            <p><label for="mobile" class="left">Movil</label>
            <input type="text" name="mobile" id="mobile" class="field" value="${form.mobile}"></p>
            <web:iferror errorList="${errorlist}" error="mobile_length" >
                <p class="error"><fmt:message key="error_length64" /></p>
            </web:iferror>

            <p><label for="zip" class="left">Código Postal<span>*</span></label>
            <input type="text" name="zip" id="zip" class="field" value="${form.ZIP}"></p>
            <web:iferror errorList="${errorlist}" error="zip_notnull">
                <p class="error"><fmt:message key="error_null" /></p>
            </web:iferror>
            <web:iferror errorList="${errorlist}" error="zip_notnull" option="not">
                <web:iferror errorList="${errorlist}" error="zip_length" >
                    <p class="error"><fmt:message key="error_length" /> </p>
                </web:iferror>
            </web:iferror>
            
            <p><label for="saveData" class="left"></label><input class="myButton" type="submit"  name="saveData" value="Guardar"></p>

        </form>
       <div class="outer-continue"><div class="continuar"><a  href="?serv=candidate&action=curiculum2">Continuar</a></div><div class="arrow"><img src="../img/arrow-continue.png"/></div></div>
        <span>*Este campo es obligatorio</span>
    </div>
</div> 


