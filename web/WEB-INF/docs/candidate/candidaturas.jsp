<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../js/jquery-1.2.3.min.js"></script> 
<script type="text/javascript" src="../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../js/ui.tabs.js"></script>
<script>
    $(document).ready(function() {
        $("#tabs > ul").tabs();

    });
</script>
<div id="main">
    <div class="top">Tus Candidaturas</div>
    <div class="content">

        <table id="candidaturas_table">
            <tbody>
                <tr>
                    <th class="topp" scope="col">Oferta</th>
                    <th class="topp" scope="col">Estado</th>
                    <th class="topp" scope="col">Opciones</th>
                </tr>
                <c:if test="${hassoffers}">
                    <web:forEach items="${offers}" var="offer">
                        <tr>
                            <td>${offer.offer.jobTitle}</td>
                            <td>${offer.status.stateName}</td>
                            <td><a href="?serv=offer&action=show&offerId=${offer.offer.id}">Ver</a></td>
                        </tr>
                    </web:forEach>
                </c:if>
                <c:if test="${!(hassoffers)}">
                    <tr>
                        <td colspan="3">No te has inscrito en ninguna oferta todavía</td>
                    </tr>
                </c:if>

            </tbody>
        </table>

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div>
</div>
