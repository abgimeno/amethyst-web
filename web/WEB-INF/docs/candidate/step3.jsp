<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<script src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/ui.tabs.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ( ($("#currently").is(':checked')) ) {
            $("#finish_year").attr("disabled", "disabled");
            $("#finish_month").attr("disabled", "disabled");
            $("#finish_day").attr("disabled", "disabled");
        }
        $("#currently").click(function () {
            if ( $("#currently").is(':checked') ) {

                $("#finish_year").attr("disabled", "disabled");
                $("#finish_month").attr("disabled", "disabled");
                $("#finish_day").attr("disabled", "disabled");
            }else{

                $("#finish_year").removeAttr("disabled");
                $("#finish_month").removeAttr("disabled");
                $("#finish_day").removeAttr("disabled");
            }
        } )

    })

</script>
<link rel="stylesheet" href="../../css/estilo.css">

<div id="fragment-3" class="ui-tabs-panel contactform">

    <table id="experience_table">
        <tbody>
            <tr>
                <th class="topp" scope="col">Experiencia</th>
                <th class="topp" scope="col">Periodo</th>
                <th class="topp" scope="col">Puesto</th>
                <th class="topp" scope="col">Empresa</th>
                <th class="topp" scope="col">Opciones</th>
            </tr>

            <c:if test="${!(hasexperience eq 'true')}">
                <web:forEach items="${experiences}" var="exp">
                    <tr>
                        <td>${exp.description}</td>
                        <td>${exp.customExperiencePeriod}</td>
                        <td>${exp.position}</td>
                        <td>${exp.company}</td>
                        <td><a href="?serv=candidate&step=3&action=editExperience&experienceId=${exp.id}">Editar</a><a href="?serv=candidate&step=3&action=deleteExperience&experienceId=${exp.id}">Borrar</a></td>
                    </tr>
                </web:forEach>
            </c:if>
            <c:if test="${hasexperience eq 'true'}">
                <tr>
                    <td colspan="5">No ha sido añadida ninguna experiencia laboral todavia</td>
                </tr>
            </c:if>


        </tbody>
    </table>


    <div class="contactform">
        <c:if test="${act eq 'add'}">
            <form action="?serv=candidate&step=3&action=addexperience" method="post">
        </c:if>
        <c:if test="${act eq 'edit'}">
            <form action="?serv=candidate&step=3&action=editExperience&experienceId=${experienceId}" method="post">
            </c:if>
            <fieldset>
                <legend>Experiencia</legend>
                <p><label for="start_day" class="left">Fecha de Inicio<span>*</span></label>
                    <web:DaySelect name="start_day" id="start_day" htmlClass="combo" value="${form.startDay}">
                    </web:DaySelect>
                    <web:MonthSelect name="start_month" id="start_month" htmlClass="combo" value="${form.startMonth}">
                    </web:MonthSelect>
                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="start_year" id="start_year" htmlClass="combo" value="${form.startYear}">
                    </web:NumberSelect>
                </p>
                <p><label for="finish_day" class="left">Fecha de fin<span>*</span></label>
                    <web:DaySelect name="finish_day" id="finish_day" htmlClass="combo" value="${form.finishDay}">
                    </web:DaySelect>
                    <web:MonthSelect name="finish_month" id="finish_month" htmlClass="combo" value="${form.finishMonth}">
                    </web:MonthSelect>
                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="finish_year" id="finish_year" htmlClass="combo" value="${form.finishYear}">
                    </web:NumberSelect>
                    <input type="checkbox" name="currently" id="currently" htmlClass="checkbox" class="field" value="true" <c:if test="${form.cursando eq 'true'}"> checked="checked" </c:if> >Actualmente
                </p>


                <p><label for="position" class="left">Puesto<span>*</span></label>
                <input type="text" name="position" id="position" class="field" value="${form.position}" ></p>
                <web:iferror errorList="${errorlist}" error="position_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="position_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="position_length" >
                        <p class="error"><fmt:message key="error_length255" /> </p>
                    </web:iferror>
                </web:iferror>


                <p><label for="company" class="left">Empresa<span>*</span></label>
                <input type="text" name="company" id="company" class="field" value="${form.company}" ></p>
                <web:iferror errorList="${errorlist}" error="company_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="company_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="company_length" >
                        <p class="error"><fmt:message key="error_length64" /> </p>
                    </web:iferror>
                </web:iferror>
                <p><label for="area_id" class="left">Área Profesional<span>*</span></label>
                    <select name="area_id" id="area_id" class="combo">
                        <web:forEach items="${areasProf}" var="areaP">
                            <option value="${areaP.id}" <c:if test="${form.areaId eq areaP.id}"> selected</c:if> >${areaP.area}</option>

                        </web:forEach>
                        <%--
            <option value="2" <c:if test="${form.areaId eq '1'}"> selected</c:if> >2</option>
                        <option value="3" <c:if test="${form.areaId eq '2'}"> selected</c:if>  >3</option>--%>
                    </select>
                </p>
                <p><label for="description" class="left">Descripción del puesto <span>*</span></label>
                <textarea name="description" id="description" class="field">${form.description}</textarea></p>
                <web:iferror errorList="${errorlist}" error="description_length" >
                    <p class="error"><fmt:message key="error_length255" /> </p>
                </web:iferror>



                <p><label for="editButton" class="left"></label><input  name="editButton" class="myButton" type="submit" value="<fmt:message key="${act}" /> Experiencia"></p>
                <c:if test="${act eq 'edit'}">
                     <div class="allplus"> <div class="plus"><img src="../img/plus.png"/></div><div class="plusLink"><a class="button" href="?serv=candidate&action=curiculum3">Añadir nueva Experiencia</a></div></div>
                </c:if>
            </fieldset>
        </form>
        <div class="outer-continue"><div class="continuar"><a href="?serv=candidate&action=curiculum4">Continuar</a></div><div class="arrow"><img src="../img/arrow-continue.png"/></div></div>

        <span>*Este campo es obligatorio</span>
    </div>


</div>