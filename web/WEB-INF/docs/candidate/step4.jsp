<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<script src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/ui.tabs.js"></script>
<link rel="stylesheet" href="../../css/estilo.css">
    <fmt:setBundle basename="resource" />

<div id="fragment-4" class="ui-tabs-panel contactform">
    <table id="knowledge_table">
        <tbody>
            <tr>
                <th class="topp" scope="col">Conocimiento</th>
                <th class="topp" scope="col">Nivel</th>
                <th class="topp" scope="col">Experiencia</th>
                <th class="topp" scope="col">Opciones</th>
            </tr>
            <c:if test="${!(hasknowledge eq 'true')}">
                <web:forList items="${knowledges}" var="know">
                    <tr>
                    <td>${know.knowledge}</td>
                    <td>${know.level.knowLevel}</td>
                    <td>${know.experience.name}</td>
                    <td><a href="?serv=candidate&step=4&action=editKnowledge&knowledgeId=${know.id}">Editar</a>&nbsp;<a href="?serv=candidate&step=4&action=deleteKnowledge&knowledgeId=${know.id}">Borrar</a></td></td>
                    </tr>
                </web:forList>
            </c:if>
            <c:if test="${hasknowledge eq 'true'}">
                <tr>
                    <td colspan="4">Aquí puedes añadir tus conocimientos en otros campos. (Idiomas, uso de programas informáticos, habilidades personales, ... )</td>
                </tr>
            </c:if>


        </tbody>
    </table>
    <div class="contactform">
        <c:if test="${act eq 'add'}">
            <form action="?serv=candidate&step=4&action=addknowledge&step=4" method="post">
        </c:if>
        <c:if test="${act eq 'edit'}">
            <form action="?serv=candidate&step=4&action=editKnowledge&step=4&knowledgeId=${knowledgeId}" method="post">
            </c:if>

            <fieldset>
                <legend>Conocimiento</legend>
                <p><label for="knowledge" class="left">Conocimiento<span>*</span></label>
                <input type="text" name="knowledge" id="knowledge" class="field" value="${form.knowledge}" ></p>
                <web:iferror errorList="${errorlist}" error="knowledge_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                
                <p><label for="knowledge_level" class="left">Nivel<span>*</span></label>
                    <select name="knowledge_level" id="knowledge_level" class="combo">
                        <option value="1" <c:if test="${form.knowledgeLevel eq '1'}"> selected</c:if> >Básico</option>
                        <option value="2" <c:if test="${form.knowledgeLevel eq '2'}"> selected</c:if> >Medio</option>
                        <option value="3" <c:if test="${form.knowledgeLevel eq '3'}"> selected</c:if>  >Alto</option>
                        <option value="4" <c:if test="${form.knowledgeLevel eq '4'}"> selected</c:if>  >Experto</option>
                    </select>
                </p>
                <p><label for="knowledge_experience" class="left">Experiencia<span>*</span></label>
                    <select name="knowledge_experience" id="knowledge_experience" class="combo">
                        <option value="1" <c:if test="${form.knowledgeExperience eq '1'}"> selected</c:if> >Menos de 1 año</option>
                        <option value="2" <c:if test="${form.knowledgeExperience eq '2'}"> selected</c:if> >Entre 1 y 2 años</option>
                        <option value="3" <c:if test="${form.knowledgeExperience eq '3'}"> selected</c:if> >Entre 2 y 3 años</option>
                        <option value="4" <c:if test="${form.knowledgeExperience eq '4'}"> selected</c:if> >Entre 3 y 4 años</option>
                        <option value="5" <c:if test="${form.knowledgeExperience eq '5'}"> selected</c:if> >Más de 5 años</option>
                    </select>
                </p>
                <p><label for="description" class="left">Descripción<span>*</span></label>
                <textarea  name="description" id="description" class="field" >${form.description}</textarea></p>
                
                <p><label for="editknowledge" class="left"></label><input class="myButton" type="submit" name="editknowledge" value="<fmt:message key="${act}" /> Conocimiento"></p>
                <c:if test="${act eq 'edit'}">
                    <div class="allplus"> <div class="plus"><img src="../img/plus.png"/></div><div class="plusLink"><a class="button" href="?serv=candidate&action=curiculum4">Añadir nuevo Conocimiento</a></div></div>
                </c:if>
            </fieldset>
        </form>
        <div class="outer-continue"><div class="continuar"><a  href="?serv=candidate&action=curiculum1">Continuar</a></div><div class="arrow"><img src="../img/arrow-continue.png"/></div></div>

        <span>*Este campo es obligatorio</span>

    </div>




</div>