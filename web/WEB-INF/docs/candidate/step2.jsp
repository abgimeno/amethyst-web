<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<script src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-1.2.3.min.js"></script>
<script type="text/javascript" src="../../js/ui.tabs.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ( ($("#cursando").is(':checked')) ) {
            $("#finish_year").attr("disabled", "disabled");
            $("#finish_month").attr("disabled", "disabled");
            $("#finish_day").attr("disabled", "disabled");
        }
        $("#cursando").click(function () {
            if ( $("#cursando").is(':checked') ) {

                $("#finish_year").attr("disabled", "disabled");
                $("#finish_month").attr("disabled", "disabled");
                $("#finish_day").attr("disabled", "disabled");
            }else{

                $("#finish_year").removeAttr("disabled");
                $("#finish_month").removeAttr("disabled");
                $("#finish_day").removeAttr("disabled");
            }
        } )

    })

</script>
<link rel="stylesheet" href="../../css/estilo.css">

<div id="fragment-2" class="ui-tabs-panel contactform">

    <table id="studies_table">
        <tbody>
            <tr>
                <th class="topp" scope="col">Título</th>
                <th class="topp" scope="col">Centro</th>

                <th class="topp" scope="col">Opciones</th>
            </tr>
            <c:if test="${!(hastudies eq 'true')}">
                <web:forEach items="${studies}" var="study">
                    <tr>
                        <td>${study.degreeName}</td>
                        <td>${study.universityName}</td>
                        <td><a href="?serv=candidate&step=2&action=editStudy&studyId=${study.id}">Editar</a>&nbsp;<a href="?serv=candidate&step=2&action=deleteStudy&studyId=${study.id}">Borrar</a></td>
                    </tr>
                </web:forEach>
            </c:if>
            <c:if test="${hastudies eq 'true'}">
                <tr>
                    <td colspan="3">No ha sido añadido ningun estudio todavia</td>
                </tr>
            </c:if>

        </tbody>
    </table>

    <div class="contactform">
        <c:if test="${act eq 'edit'}">
            <form action="?serv=candidate&step=2&action=editStudy&studyId=${studyId}" method="post">
        </c:if>
        <c:if test="${act eq 'add'}">
            <form action="?serv=candidate&step=2&action=addStudy" method="post">
         </c:if>

            <fieldset>
                <legend>Estudio</legend>

                <p><label for="degree_name" class="left">Título<span>*</span></label>
                <input type="text" name="degree_name" id="degree_name" class="field" value="${form.degreeName}" ></p>
                <web:iferror errorList="${errorlist}" error="degree_name_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="degree_name_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="degree_name_length" >
                        <p class="error"><fmt:message key="error_length255" /> </p>
                    </web:iferror>
                </web:iferror>

                <p><label for="degreelevel_id" class="left">Nivel<span>*</span></label>
                    <select name="degreelevel_id" id="degreelevel_id" class="combo">
                        <web:forEach items="${degree_level}" var="level">
                            <option value="${level.id}" <c:if test="${form.degreeLevelId eq level.id}"> selected</c:if> >${level.levelName}</option>
                        </web:forEach>
                    </select>
                </p>
                <p><label for="start_day" class="left">Fecha de Inicio<span>*</span></label>
                    <web:DaySelect name="start_day" id="start_day" htmlClass="combo" value="${form.startDay}">
                    </web:DaySelect>
                    <web:MonthSelect name="start_month" id="start_month" htmlClass="combo" value="${form.startMonth}">
                    </web:MonthSelect>
                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="start_year" id="start_year" htmlClass="combo" value="${form.startYear}">
                    </web:NumberSelect>
                </p>
                <p><label for="finish_day" class="left">Fecha de fin<span>*</span></label>
                    <web:DaySelect name="finish_day" id="finish_day" htmlClass="combo" value="${form.finishDay}">
                    </web:DaySelect>
                    <web:MonthSelect name="finish_month" id="finish_month" htmlClass="combo" value="${form.finishMonth}">
                    </web:MonthSelect>
                    <web:NumberSelect from="${startyear}" to="${currentyear}" name="finish_year" id="finish_year" htmlClass="combo" value="${form.finishYear}">
                    </web:NumberSelect>
                    <input type="checkbox" name="cursando" id="cursando" htmlClass="checkbox" class="field" value="true" <c:if test="${form.cursando eq 'true'}"> checked="checked" </c:if> >Cursando

                </p>
                <p><label for="university" class="left">Centro<span>*</span></label>
                <input type="text" name="university" id="university" class="field" value="${form.university}" ></p>
                <web:iferror errorList="${errorlist}" error="university_notnull">
                    <p class="error"><fmt:message key="error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="university_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="university_length" >
                        <p class="error"><fmt:message key="error_length255" /> </p>
                    </web:iferror>
                </web:iferror>                

                <p><label for="editstudy" class="left"></label><input class="myButton" type="submit" name="editstudy" value="<fmt:message key="${act}" /> Estudio">
                    <c:if test="${act eq 'edit'}">
                      <div class="allplus"> <div class="plus"><img src="../img/plus.png"/></div><div class="plusLink"><a  href="?serv=candidate&action=curiculum2">Añadir nuevo Estudio</a></div></div>
                    </c:if>
                </p>
            </fieldset>

        </form>
        <div class="outer-continue"><div class="continuar"><a href="?serv=candidate&action=curiculum3">Continuar</a></div><div class="arrow"><img src="../img/arrow-continue.png"/></div></div>
        <span>*Este campo es obligatorio</span>
    </div>

</div> 


