<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div id="main">
    <div class="top">Menu Privado</div>
    <div class="content">
        <div class="curriculumLink">
            <a href="?serv=candidate&action=curiculum1" id="Linktitle" >Mi curiculum</a>
            <ul>
                <li><a href="?serv=candidate&action=curiculum1" >Datos Personales</a></li>
                <li><a href="?serv=candidate&action=curiculum2" >Estudios</a></li>
                <li><a href="?serv=candidate&action=curiculum3" >Experiencia</a></li>
                <li><a href="?serv=candidate&action=curiculum4" >Conocimientos</a></li>
            </ul>
        </div>

        <div class="candidanciesLink">
            <a href="?serv=candidate&action=subscriptions">Mis candidaturas</a>
        </div>

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div> 
</div>
