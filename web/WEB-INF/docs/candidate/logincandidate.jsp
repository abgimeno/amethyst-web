<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>
<fmt:setBundle basename="resource" />
<div id="main">
    <div class="top">Acceso candidatos</div>
    <div class="content">
        <h2></h2>
        <div class="logincandidate">
            <h3>Después de registrarte, inserta aquí tu curriculum</h3>
            <form action="/docs/?serv=login&action=login&user=candidate" method="post">
                <p><label for="usernameCandidate" class="topp">Email:</label><br>
                <input name="userlogin" id="userlogin" tabindex="1" class="field" value="" type="text"></p>
                <web:iferror errorList="${errorlist}" error="userlogin_notnull">
                    <p class="error"><fmt:message key="username_error_null"/></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="userlogin_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="userlogin_email">
                        <p class="error"><fmt:message key="user_error_email_format" /></p>
                    </web:iferror>
                </web:iferror>
                <p><label for="passwordCandidate" class="topp">Contraseña:</label><br>
                <input name="password" id="password" tabindex="2" class="field" value="" type="password"></p>
                <web:iferror errorList="${errorlist}" error="password_notnull">
                    <p class="error"><fmt:message key="password_error_null" /></p>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="password_notnull" option="not">
                    <web:iferror errorList="${errorlist}" error="password_length" >
                        <p class="error"><fmt:message key="password_error_length" /></p>
                    </web:iferror>
                </web:iferror>
                <web:iferror errorList="${errorlist}" error="login_failed">
                    <p class="error"><fmt:message key="failed_login" /></p>
                </web:iferror>


                <p><input name="cmdweblogin" class="myButton" value="Entrar" type="submit"></p>
                <p><a href="#" id="forgotpsswd_1">¿Has olvidado tu contraseña?</a></p>
               <web:iferror errorList="${errorlist}" error="enable_account">
                    <p class="error"><fmt:message key="enable_account" /></p>
                </web:iferror>
               <web:iferror errorList="${errorlist}" error="register_completed">
                    <p class="error"><fmt:message key="register_completed" /></p>
                </web:iferror>
            </form>
        </div>
        <div class="registercandidate">
            <h3>¿Todavia no tienes cuenta?</h3>
            <ul id="characteristics">
                <li>web dedicada exclusivamente a la oferta laboral de la provincia de Castellón</li>
                <li>date de alta totalmente gratis</li>
                <li>te ayudaremos a encontrar trabajo</li>
                <li>te informaremos de toda la oferta laboral de la provincia</li>
                <li>puedes elegir las categorías que se ajusten a tu perfil</li>
                <li>información de cursos, masters, etc.</li>
            </ul>
            <form>
                <p><a id="reg_1" href="/docs/?serv=register&action=do_register&user=candidate">Registrate</a></p>
            </form>
        </div>

    </div>
    <div class="bottom">
        <div id="pagination">

        </div>
    </div>
</div> 
</div> 