<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>

<div id="main">
    <div class="top"></div>
    <div class="content">
        <div class="shownew">
		<div class="shownewtitle">
                    ${entry.title}
		</div>
                <div class="new">
                    <div class="text">${entry.texto}</div>
                    <div class="newdate">${entry.customDate}</div>
                </div>
        </div>
    </div>
    <div class="bottom">
        <div id="pagination">
        </div>
    </div>
</div>
