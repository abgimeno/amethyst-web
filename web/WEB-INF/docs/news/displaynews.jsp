<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://euler.com/web" prefix="web" %>

<div id="main">
    <div class="top">Noticias</div>
    <div class="content">
        <div class="displayNews">
            <web:forList items="${noticias}" var="entry" cols="2">
                <div class="new${column}">
                    <a id="new" href="?serv=newsdisplay&action=shownew&id=${entry.id}">${entry.trimedTitle}</a>
                    <div id="textbody">${entry.trimedText}</div>
                    <div id="newdate">${entry.customDate}</div>
                </div>

            </web:forList>
        </div>
    </div>
    <div class="bottom">
        <div id="pagination">
        </div>
    </div>
</div>